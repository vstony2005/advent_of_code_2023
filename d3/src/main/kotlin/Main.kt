import java.io.File

fun main(args: Array<String>) {
    //part1()
    part2()
}

fun part1() {
    readfile1(".\\test1.txt")
    readfile1(".\\input1.txt")
}

fun part2() {
    readfile3(".\\test1.txt")
    readfile3(".\\input1.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    val lines = mutableListOf<String>()
    var res: Int = 0

    f.forEachLine { lines.add(it)}

    val linesRes = mutableListOf<String>()
    for (i in 0..<lines.size) {
        linesRes.add(".".repeat(lines[i].length))
    }

    //for (lin in lines) {
    //    for (ch in lin) {
    //        println(ch)
    //    }
    //}

    //for (i in 0..<lines.size) {
    //    val lin: String = lines[i]
    //    for (j in lin.indices) {
    //        val ch = lin[j]
    //        if (ch == '.') continue
    //        println("line $i : char $j = ${lin[j]}")
    //    }
    //}

    var i: Int = 0
    while (i < lines.size) {
        var n_found = false
        do {
            n_found = false
            var j: Int = 0
            while (j < lines[i].length) {
                val ch: Char = lines[i][j]
                if (ch in '0'..'9' && isCaseValid(i, j, lines)) {
                    n_found = true

                    linesRes[i] = linesRes[i].replaceRange(j, j+1, ch.toString())
                    lines[i] = lines[i].replaceRange(j, j+1, "*")
                }
                j++
            }
        } while (n_found)
        i++
    }

    for (line in linesRes) {
        val reg = Regex("[0-9]+")
        val matches = reg.findAll(line)
        matches.forEach {
            res += it.value.toInt()
        }
    }

    println(res)
}

fun isCaseValid(aLin: Int, aCol: Int, lines: List<String>): Boolean {
    if (aLin > 0) {
        if (isCharValid(lines[aLin-1][aCol]))  return true
        if (aCol > 0 && isCharValid(lines[aLin-1][aCol-1])) return true
        if (aCol < lines[aLin].length-1 &&  isCharValid(lines[aLin-1][aCol+1])) return true
    }

    if (aLin < lines.size-1) {
        if (isCharValid(lines[aLin+1][aCol])) return true
        if (aCol > 0 && isCharValid(lines[aLin+1][aCol-1])) return true
        if (aCol < lines[aLin].length-1 && isCharValid(lines[aLin+1][aCol+1])) return true
    }

    if (aCol > 0 && isCharValid(lines[aLin][aCol-1])) return true
    if (aCol < lines[aLin].length-1 && isCharValid(lines[aLin][aCol+1])) return true

    return false
}

fun isCharValid(aChar: Char): Boolean {
    val reg = Regex("[^.0-9]")
    return reg.containsMatchIn(aChar.toString())
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    val lines = mutableListOf<String>()
    var res: Int = 0

    f.forEachLine { lines.add(it)}

    val linesRes = mutableListOf<String>()
    for (i in 0..<lines.size) {
        linesRes.add(".".repeat(lines[i].length))
    }

    var i: Int = 0
    while (i < lines.size) {
        var n_found = false
        do {
            n_found = false
            var j: Int = 0
            while (j < lines[i].length) {
                val ch: Char = lines[i][j]
                if (ch == '*')
                    linesRes[i] = linesRes[i].replaceRange(j, j+1, "*")
                else if (ch in '0'..'9' && isCaseValid(i, j, lines)) {
                    n_found = true

                    linesRes[i] = linesRes[i].replaceRange(j, j+1, ch.toString())
                    lines[i] = lines[i].replaceRange(j, j+1, "+")
                }
                j++
            }
        } while (n_found)
        i++
    }

    lines.clear()
    for (line in linesRes) {
        lines.add(line)
    }
    linesRes.clear()
    for (l in 0..<lines.size) {
        linesRes.add(".".repeat(lines[l].length))
    }


    i = 0
    while (i < lines.size) {
        var n_found = false
        do {
            n_found = false
            var j: Int = 0
            while (j < lines[i].length) {
                val ch: Char = lines[i][j]
                if (ch == '*')
                    linesRes[i] = linesRes[i].replaceRange(j, j+1, "*")
                else if (ch in '0'..'9' && isCaseValid(i, j, lines)) {
                    n_found = true

                    linesRes[i] = linesRes[i].replaceRange(j, j+1, ch.toString())
                    lines[i] = lines[i].replaceRange(j, j+1, "+")
                }
                j++
            }
        } while (n_found)
        i++
    }

    for (line in linesRes) {
        println(line)
    }

    println(res)
}

fun readfile3(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    val lines = mutableListOf<String>()
    var res: Int = 0

    f.forEachLine { lines.add(it)}

    var linesRes = simplify(lines)
    linesRes = simplify(linesRes)

    for (line in linesRes) println(line)

    linesRes = stars2(linesRes)
    linesRes = simplify(linesRes)

    for (line in linesRes) println(line)

    res = calculRes(linesRes)

    println(res)
}

fun simplify(aLines: List<String>): MutableList<String> {
    val lines = mutableListOf<String>()
    val linesRes = mutableListOf<String>()
    for (line in aLines) {
        lines.add(line)
        linesRes.add(".".repeat(line.length))
    }

    var i: Int = 0
    while (i < lines.size) {
        var nFound = false
        do {
            nFound = false
            var j: Int = 0
            while (j < lines[i].length) {
                val ch: Char = lines[i][j]
                if (ch == '*')
                    linesRes[i] = linesRes[i].replaceRange(j, j+1, "*")
                else if (ch in '0'..'9' && isCaseValid(i, j, lines)) {
                    nFound = true

                    linesRes[i] = linesRes[i].replaceRange(j, j+1, ch.toString())
                    lines[i] = lines[i].replaceRange(j, j+1, "+")
                }
                j++
            }
        } while (nFound)
        i++
    }
    return linesRes
}

fun stars2(aLines: List<String>): MutableList<String> {
    val lines = mutableListOf<String>()
    val linesRes = mutableListOf<String>()
    for (line in aLines) {
        lines.add(line)
        linesRes.add(".".repeat(line.length))
    }

    var i: Int = 0
    while (i < lines.size) {
        var j: Int = 0
        while (j < lines[i].length) {
            val ch: Char = lines[i][j]
            if (ch == '*') {
                if (!isCaseWith2Num(i, j, lines))
                    linesRes[i] = linesRes[i].replaceRange(j, j + 1, ".")
                else
                    linesRes[i] = linesRes[i].replaceRange(j, j + 1, "*")
            }
            else
                linesRes[i] = linesRes[i].replaceRange(j, j+1, ch.toString());
            j++
        }
        i++
    }
    return linesRes
}

fun isCaseWith2Num(aLin: Int, aCol: Int, lines: List<String>): Boolean {
    var nb: Int = 0

    if (aLin > 0) {
        var n: Int = 0
        if (aCol > 0) {
            if (lines[aLin-1][aCol-1] in '0'..'9') {
                nb += 1
                n = 1
            }
        }

        if (lines[aLin-1][aCol] in '0'..'9') {
            if (n == 0) {
                nb += 1
                n = 1
            }
        } else {
            n = 0
        }

        if (lines[aLin-1][aCol+1] in '0'..'9') {
            if (n == 0) nb += 1
        }
    }

    if (aLin < lines.size-1)
    {
        var n: Int = 0
        if (aCol > 0) {
            if (lines[aLin+1][aCol-1] in '0'..'9') {
                nb += 1
                n = 1
            }
        }

        if (lines[aLin+1][aCol] in '0'..'9') {
            if (n == 0) {
                nb += 1
                n = 1
            }
        } else {
            n = 0
        }

        if (lines[aLin+1][aCol+1] in '0'..'9') {
            if (n == 0) nb += 1
        }
    }

    if (aCol > 0 && lines[aLin][aCol-1] in '0'..'9') {
        nb += 1
    }

    if (aCol < lines[aLin].length-1 && lines[aLin][aCol+1] in '0'..'9') {
        nb += 1
    }

    println(nb)
    return nb == 2
}

fun calculRes(aLines: List<String>): Int {
    var n: Int = 0

    var i: Int = 0
    while (i < aLines.size) {
        var j: Int = 0
        while (j < aLines[i].length) {
            val ch: Char = aLines[i][j]
            if (ch == '*') {
                n += isCalculStar(i, j, aLines)
            }
            j++
        }
        i++
    }
    return n
}

fun isCalculStar(aLin: Int, aCol: Int, aLines: List<String>): Int {
    var nb: Int = 1

    if (aLin > 0) {
        var n: Int = 0
        if (aCol > 0) {
            if (aLines[aLin-1][aCol-1] in '0'..'9') {
                nb *= getNumber(aLin-1, aCol-1, aLines)
                n = 1
            }
        }

        if (aLines[aLin-1][aCol] in '0'..'9') {
            if (n == 0) {
                nb *= getNumber(aLin-1, aCol, aLines)
                n = 1
            }
        } else {
            n = 0
        }

        if (aLines[aLin-1][aCol+1] in '0'..'9') {
            if (n == 0)
                nb *= getNumber(aLin-1, aCol+1, aLines)
        }
    }

    if (aLin < aLines.size-1)
    {
        var n: Int = 0
        if (aCol > 0) {
            if (aLines[aLin+1][aCol-1] in '0'..'9') {
                nb *= getNumber(aLin+1, aCol-1, aLines)
                n = 1
            }
        }

        if (aLines[aLin+1][aCol] in '0'..'9') {
            if (n == 0) {
                nb *= getNumber(aLin+1, aCol, aLines)
                n = 1
            }
        } else {
            n = 0
        }

        if (aLines[aLin+1][aCol+1] in '0'..'9') {
            if (n == 0)
                nb *= getNumber(aLin+1, aCol+1, aLines)
        }
    }

    if (aCol > 0 && aLines[aLin][aCol-1] in '0'..'9') {
        nb *= getNumber(aLin, aCol-1, aLines)
    }

    if (aCol < aLines[aLin].length-1 && aLines[aLin][aCol+1] in '0'..'9') {
        nb *= getNumber(aLin, aCol+1, aLines)
    }

    return nb
}

fun getNumber(aLin: Int, aCol: Int, lines: List<String>): Int {
    val n: Int = 0
    var n1: Int = aCol
    var n2: Int = aCol
    while (n1 > 0 && lines[aLin][n1-1] in '0'..'9') n1 -= 1
    while (n2 < lines[aCol].length-1 && lines[aLin][n2+1] in '0'..'9') n2 += 1

    val res: Int = lines[aLin].substring(n1, n2+1).toInt()
    return res
}
