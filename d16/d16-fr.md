# --- Jour 16 : Le sol sera de la lave ---

## --- Première partie ---

Avec le faisceau de lumière complètement focalisé *quelque part*, le renne vous conduit encore plus profondément dans l'usine de production de lave. movezsA un moment donné, vous réalisez que les murs d'acier de l'usine ont été remplacés par des grottes, que les portes ne sont que des grottes, que le sol est une grotte et que vous êtes presque sûr qu'il s'agit en fait d'une grotte géante.

Enfin, alors que vous approchez de ce qui doit être le cœur de la montagne, vous apercevez une lumière brillante dans une caverne située devant vous. Vous découvrez alors que le faisceau de lumière que vous avez si soigneusement focalisé émerge de la paroi de la caverne la plus proche de l'installation et déverse toute son énergie dans un engin situé de l'autre côté.

En y regardant de plus près, l'engin semble être une grille carrée plate en deux dimensions contenant *de l'espace vide* (`.`), *des miroirs* (`/` et `-`), et *des séparateurs* (`|` et `-`).

L'engin est aligné de façon à ce que la majeure partie du faisceau rebondisse autour de la grille, mais chaque tuile de la grille convertit une partie de la lumière du faisceau en *chaleur* pour faire fondre la roche dans la caverne.

Vous notez la disposition de l'engin (votre entrée dans l'énigme). Par exemple :

```
.|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....
```

Le faisceau entre dans le coin supérieur gauche depuis la gauche et se dirige vers la *droite*. Ensuite, son comportement dépend de ce qu'il rencontre au cours de son déplacement :

- Si le faisceau rencontre un *espace vide* (`.`), il continue dans la même direction.
- Si le faisceau rencontre un *miroir* (`/` ou `\`), il est *réfléchi* de 90 degrés en fonction de l'angle du miroir. Par exemple, un faisceau se déplaçant vers la droite et rencontrant un miroir `/` continuera *vers le haut* dans la colonne du miroir, tandis qu'un faisceau se déplaçant vers la droite et rencontrant un miroir `/` continuera *vers le bas* de la colonne du miroir.
- Si le faisceau rencontre l'extrémité *pointue d'un séparateur* (`|` ou `-`), il traverse le séparateur comme s'il s'agissait d'un *espace vide*. Par exemple, un faisceau se déplaçant vers la droite et rencontrant un séparateur `-` continuera dans la même direction.
- Si le faisceau rencontre le *côté plat d'un séparateur* (`|` ou `-`), le faisceau est *séparé en deux faisceaux* allant dans chacune des deux directions vers lesquelles pointent les extrémités du séparateur. Par exemple, un faisceau se déplaçant vers la droite qui rencontre un séparateur `|` se divise en deux faisceaux : un qui continue *vers le haut* à partir de la colonne du séparateur et un qui continue *vers le bas* à partir de la colonne du séparateur.

Les faisceaux n'interagissent pas avec d'autres faisceaux ; une tuile peut être traversée par plusieurs faisceaux en même temps. Une tuile est *énergisée* si au moins un rayon la traverse, s'y reflète ou s'y divise.

Dans l'exemple ci-dessus, voici comment le faisceau de lumière rebondit autour de l'engin :

```
>|<<<\....
|v-.\^....
.v...|->>>
.v...v^.|.
.v...v^...
.v...v^..\
.v../2\\..
<->-/vv|..
.|<<<2-|.\
.v//.|.v..
```

Les rayons ne sont affichés que sur les tuiles vides ; les flèches indiquent la direction des rayons. Si une tuile contient des rayons se déplaçant dans plusieurs directions, le nombre de directions distinctes est indiqué. Voici le même diagramme, mais en indiquant seulement si une tuile est *énergisée* (`#`) ou non (`.`) :

```
######....
.#...#....
.#...#####
.#...##...
.#...##...
.#...##...
.#..####..
########..
.#######..
.#...#.#..
```

En fin de compte, dans cet exemple, `*46*` tuiles sont *énergisées*.

La lumière n'alimente pas suffisamment de tuiles pour produire de la lave ; pour déboguer l'engin, vous devez commencer par analyser la situation actuelle. Si le faisceau commence en haut à gauche et se dirige vers la droite, *combien de tuiles finissent par être énergisées* ?

Pour commencer, [obtenez les données de votre puzzle](https://adventofcode.com/2023/day/16/input).

## --- Deuxième partie ---

Alors que vous essayez de comprendre ce qui ne va pas, le renne tire sur votre chemise et vous conduit vers un panneau de commande situé à proximité. Là, une série de boutons vous permet d'aligner l'engin de façon à ce que le faisceau entre par *n'importe quelle tuile du bord* et s'éloigne de ce bord. (Vous pouvez choisir l'une des deux directions pour le faisceau s'il commence dans un coin ; par exemple, si le faisceau commence dans le coin inférieur droit, il peut commencer à se diriger soit vers la gauche, soit vers le haut).

Ainsi, le faisceau peut commencer sur n'importe quelle tuile de la rangée supérieure (vers le bas), n'importe quelle tuile de la rangée inférieure (vers le haut), n'importe quelle tuile de la colonne la plus à gauche (vers la droite) ou n'importe quelle tuile de la colonne la plus à droite (vers la gauche). Pour produire de la lave, vous devez trouver la configuration qui *énergise autant de tuiles que possible*.

Dans l'exemple ci-dessus, cela peut être réalisé en commençant le faisceau dans la quatrième tuile en partant de la gauche dans la rangée du haut :

```
.|<2<\....
|v-v\^....
.v.v.|->>>
.v.v.v^.|.
.v.v.v^...
.v.v.v^..\
.v.v/2\\..
<-2-/vv|..
.|<<<2-|.\
.v//.|.v..
```

Avec cette configuration, les tuiles `*51*` sont alimentées :

```
.#####....
.#.#.#....
.#.#.#####
.#.#.##...
.#.#.##...
.#.#.##...
.#.#####..
########..
.#######..
.#...#.#..
```

Trouvez la configuration initiale du faisceau qui alimente le plus grand nombre de tuiles ; *combien de tuiles sont alimentées dans cette configuration?*