import java.util.concurrent.TimeUnit
import kotlin.Exception

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

operator fun Pair<Int,Int>.plus(other: Pair<Int, Int>): Pair<Int,Int> =
    Pair(this.first + other.first, this.second + other.second)
operator fun List<List<Char>>.get(index: Pair<Int, Int>): Char =
    this[index.first][index.second]

fun divers() {
    val map = MyMap(5,5)
    map[positions[DIRECTIONS.RIGHT]] = DIRECTIONS.RIGHT
    map.println()
    map[Pair(1,2) + Pair(1,1)].println("pos 2,3: ")
    map[Pair(1,1) + positions[DIRECTIONS.DOWN]!!].println()
    map[Pair(10,10)].println("out: ")
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    readfile2("input")
}

typealias ListChars = List<List<Char>>
typealias ListMoves = MutableList<Pair<Pair<Int,Int>, DIRECTIONS>>

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val grid: ListChars = lines.map { line -> line.toList() }
    val visited: MyMap = MyMap(grid.size, grid[0].size)
    goMove(grid, visited, mutableListOf(Pair(Pair(0,0), DIRECTIONS.RIGHT)))
    //visited.println("visited\n")

    var res: Int = visited.countVisited()
    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun goMove(grid: ListChars, visited: MyMap, todo: ListMoves) {
    while (todo.any()) {
        val aMove = todo.removeAt(0)
        move(grid, visited, aMove.first, aMove.second, todo)
    }
}

fun move(grid: ListChars, visited: MyMap, position: Pair<Int, Int>, direction: DIRECTIONS, todo: ListMoves) {
    if (visited[position] == null || visited.hasDirection(position, direction)!!)
        return
    visited[position] = direction

    if (grid[position] == '.') {
        move(grid, visited, position + positions[direction]!!, direction, todo)
    } else if (grid[position] == '/') {
        when (direction) {
            DIRECTIONS.UP -> {
                todo.add(Pair(position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT))
            }
            DIRECTIONS.RIGHT -> {
                todo.add(Pair(position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP))
            }
            DIRECTIONS.DOWN -> {
                todo.add(Pair(position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT))
            }
            DIRECTIONS.LEFT -> {
                todo.add(Pair(position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN))
            }
        }
    } else if (grid[position] == '\\') {
        when (direction) {
            DIRECTIONS.UP -> {
                todo.add(Pair(position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT))
            }
            DIRECTIONS.RIGHT -> {
                todo.add(Pair(position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN))
            }
            DIRECTIONS.DOWN -> {
                todo.add(Pair(position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT))
            }
            DIRECTIONS.LEFT -> {
                todo.add(Pair(position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP))
            }
        }
    } else if (grid[position] == '-') {
        when (direction) {
            DIRECTIONS.RIGHT,
            DIRECTIONS.LEFT -> {
                todo.add(Pair(position + positions[direction]!!, direction))
            }
            DIRECTIONS.DOWN,
            DIRECTIONS.UP -> {
                todo.add(Pair(position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT))
                todo.add(Pair(position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT))
            }
        }
    } else if (grid[position] == '|') {
        when (direction) {
            DIRECTIONS.UP,
            DIRECTIONS.DOWN -> {
                todo.add(Pair(position + positions[direction]!!, direction))
            }
            DIRECTIONS.RIGHT,
            DIRECTIONS.LEFT -> {
                todo.add(Pair(position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP))
                todo.add(Pair(position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN))
            }
        }
    }
}

fun moveRecurs(grid: ListChars, visited: MyMap, position: Pair<Int, Int>, direction: DIRECTIONS) {
    // does not work in recursive mode
    if (visited[position] == null || visited.hasDirection(position, direction)!!)
        return
    visited[position] = direction

    if (grid[position] == '.') {
        moveRecurs(grid, visited, position + positions[direction]!!, direction)
    } else if (grid[position] == '/') {
        when (direction) {
            DIRECTIONS.UP -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT)
            }
            DIRECTIONS.RIGHT -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP)
            }
            DIRECTIONS.DOWN -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT)
            }
            DIRECTIONS.LEFT -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN)
            }
        }
    } else if (grid[position] == '\\') {
        when (direction) {
            DIRECTIONS.UP -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT)
            }
            DIRECTIONS.RIGHT -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN)
            }
            DIRECTIONS.DOWN -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT)
            }
            DIRECTIONS.LEFT -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP)
            }
        }
    } else if (grid[position] == '-') {
        when (direction) {
            DIRECTIONS.RIGHT,
            DIRECTIONS.LEFT -> {
                moveRecurs(grid, visited, position + positions[direction]!!, direction)
            }
            DIRECTIONS.DOWN,
            DIRECTIONS.UP -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.LEFT]!!, DIRECTIONS.LEFT)
                moveRecurs(grid, visited, position + positions[DIRECTIONS.RIGHT]!!, DIRECTIONS.RIGHT)
            }
        }
    } else if (grid[position] == '|') {
        when (direction) {
            DIRECTIONS.UP,
            DIRECTIONS.DOWN -> {
                moveRecurs(grid, visited, position + positions[direction]!!, direction)
            }
            DIRECTIONS.RIGHT,
            DIRECTIONS.LEFT -> {
                moveRecurs(grid, visited, position + positions[DIRECTIONS.UP]!!, DIRECTIONS.UP)
                moveRecurs(grid, visited, position + positions[DIRECTIONS.DOWN]!!, DIRECTIONS.DOWN)
            }
        }
    }
}

class MyMap(lines: Int, cols: Int) {
    private val nbLines: Int = lines
    private val nbCols: Int = cols
    private val grid: Array<Array<MutableSet<DIRECTIONS>>> = Array(nbLines) { Array(nbCols) {
        mutableSetOf()
    } }

    fun hasDirection(index: Pair<Int,Int>, direction: DIRECTIONS): Boolean? =
        try {
            grid[index.first][index.second].contains(direction)
        } catch (e: Exception) {
            null
        }

    operator fun get(index: Pair<Int, Int>): Set<DIRECTIONS>? =
        try {
            grid[index.first][index.second]
        } catch (e: Exception) {
            null
        }

    operator fun set(index: Pair<Int, Int>?, value: DIRECTIONS) {
        if (index != null)
            grid[index.first][index.second].add(value)
    }

    override fun toString(): String {
        var str: String = ""
        grid.map { line ->
            line.map { str += if (it.any()) '#' else '.'  }
            str += "\n"
        }
        return str
    }

    fun countVisited(): Int {
        var res: Int = 0
        grid.map { line->
            res += line.count { col -> col.any() }
        }
        return res
    }
}

enum class DIRECTIONS { UP, RIGHT, DOWN, LEFT }
val positions: HashMap<DIRECTIONS, Pair<Int,Int>> = hashMapOf(
    DIRECTIONS.UP to Pair(-1,0),
    DIRECTIONS.RIGHT to Pair(0,1),
    DIRECTIONS.DOWN to Pair(1,0),
    DIRECTIONS.LEFT to Pair(0,-1)
)

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val grid: ListChars = lines.map { line -> line.toList() }
    var res: Int = goMoveAll(grid)
    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun goMoveAll(grid: List<List<Char>>): Int {
    val values: MutableList<Int> = mutableListOf()
    for (l in grid.indices) {
        values.add(getValueMove(grid, mutableListOf(Pair(Pair(l,0), DIRECTIONS.RIGHT))))
        values.add(getValueMove(grid, mutableListOf(Pair(Pair(l,grid.size-1), DIRECTIONS.LEFT))))

        if (l == 0) {
            for (c in grid[0].indices) {
                values.add(getValueMove(grid, mutableListOf(Pair(Pair(0,c), DIRECTIONS.DOWN))))
                values.add(getValueMove(grid, mutableListOf(Pair(Pair(grid.size-1,c), DIRECTIONS.UP))))
            }
        }
    }
    return values.max()
}



fun getValueMove(grid: ListChars, todo: ListMoves): Int {
    val visited: MyMap = MyMap(grid.size, grid[0].size)
    while (todo.any()) {
        val aMove = todo.removeAt(0)
        move(grid, visited, aMove.first, aMove.second, todo)
    }
    return visited.countVisited()
}
