import java.io.File

fun main(args: Array<String>) {
    //part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1(".\\test.txt")
    //readfile1(".\\input.txt")
}

fun part2() {
    readfile2(".\\test.txt")
    readfile2(".\\input.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }

    val grid: MutableList<MutableList<Char>> = mutableListOf()
    for (i in lines.indices) {
        val l: MutableList<Char> = mutableListOf()
        for (j in lines[i].indices) {
            l.add(lines[i][j])
        }
        grid.add(l)
    }

    //region empty lines
    val emptyLines: MutableList<Int> = mutableListOf()
    for (l in grid.indices) {
        var isEmpty: Boolean = true
        for (c in grid[l]) {
            isEmpty = (c == '.')
            if (!isEmpty) break
        }
        if (isEmpty) emptyLines.add(l)
    }
    for (l in emptyLines.reversed()) {
        addLin(grid, l)
    }
    //endregion

    //region empty columns
    val emptyCols: MutableList<Int> = mutableListOf()
    for (c in grid[0].indices) {
        var isEmpty: Boolean = true
        for (l in grid.indices) {
            isEmpty = (grid[l][c] == '.')
            if (!isEmpty) break
        }
        if (isEmpty) emptyCols.add(c)
    }
    for (c in emptyCols.reversed()) {
        addCol(grid, c)
    }
    //endregion

    //for (g in grid) {
    //    println(g)
    //}

    val pos: MutableList<List<Int>> = mutableListOf()
    for (l in grid.indices) {
        for (c in grid[l].indices) {
            if (grid[l][c] != '.')
                pos.add(listOf(l, c))
        }
    }

    var res: Int = 0
    for (i in pos.indices) {
        for (j in i..<pos.size) {
            if (i != j) {
                res += distancePoints(pos[i][0], pos[j][0])
                res += distancePoints(pos[i][1], pos[j][1])
            }
        }
    }

    println(res)
}

fun distancePoints(a: Int, b: Int): Int {
    return if (a > b) a - b else b - a
}

fun addLin(array: MutableList<MutableList<Char>>, pos: Int) {
    val line: MutableList<Char> = mutableListOf()
    for (i in array[pos].indices)
        line.add('.')
    array.add(pos, line)
}

fun addCol(array: MutableList<MutableList<Char>>, pos: Int) {
    for (i in array.indices)
        array[i].add(pos, '.')
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }

    val grid: MutableList<MutableList<Char>> = mutableListOf()
    for (i in lines.indices) {
        val l: MutableList<Char> = mutableListOf()
        for (j in lines[i].indices) {
            l.add(lines[i][j])
        }
        grid.add(l)
    }

    //region empty lines
    val emptyLines: MutableList<Int> = mutableListOf()
    for (l in grid.indices) {
        var isEmpty: Boolean = true
        for (c in grid[l]) {
            isEmpty = (c == '.')
            if (!isEmpty) break
        }
        if (isEmpty) emptyLines.add(l)
    }
    //endregion

    //region empty columns
    val emptyCols: MutableList<Int> = mutableListOf()
    for (c in grid[0].indices) {
        var isEmpty: Boolean = true
        for (l in grid.indices) {
            isEmpty = (grid[l][c] == '.')
            if (!isEmpty) break
        }
        if (isEmpty) emptyCols.add(c)
    }
    //endregion

    val pos: MutableList<List<Long>> = mutableListOf()
    for (l in grid.indices) {
        for (c in grid[l].indices) {
            if (grid[l][c] != '.')
                pos.add(listOf(l.toLong(), c.toLong()))
        }
    }

    var res: Long = 0
    val coef: Int = 1000000
    for (i in pos.indices) {
        for (j in i..<pos.size) {
            if (i != j) {
                res += distancePointsWithCoef(pos[i][0], pos[j][0], emptyLines, coef)
                res += distancePointsWithCoef(pos[i][1], pos[j][1], emptyCols, coef)
            }
        }
    }

    println(res)
}

fun distancePointsWithCoef(a: Long, b: Long, vals: List<Int>, coef: Int): Long {
    val vMax = if (a > b) a else b
    val vMin = a + b - vMax

    var nb: Int = 0
    for (v in vals) {
        if (v in (vMin + 1)..<vMax) nb += 1
    }

    return vMax + (nb * (coef - 1)) - vMin
}

