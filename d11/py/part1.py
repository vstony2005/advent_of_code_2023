file = []
# read file
filename = "test.txt"
filename = "input.txt"
print(filename)
with open(filename, 'r') as filin:
    file = filin.readlines()

lines = []
for l in file:
    cols = []
    for c in l.strip():
        cols.append(c)
    lines.append(cols)


def add_line(arr_2d, line):
    l = []
    for i in arr_2d[0]:
        l.append('.')
    arr_2d.insert(line, l)

def add_col(arr_2d, col):
    for l in arr_2d:
        l.insert(col, '.')


# empty lines
l_add = []
for i in range(0, len(lines)):
    is_empty = True
    for c in lines[i]:
        is_empty = is_empty and (c == '.')
    if (is_empty):
        l_add.append(i)

print(l_add)
for i in range(len(l_add)-1,-1,-1):
    add_line(lines, l_add[i])


# empty cols
c_add = []
for i in range(len(lines[0])):
    is_empty = True
    for l in range(len(lines)):
        is_empty = is_empty and (lines[l][i] == '.')
    if (is_empty):
        c_add.append(i)

print(c_add)
for i in range(len(c_add)-1,-1,-1):
    add_col(lines, c_add[i])


pos = []
for l in range(len(lines)):
    for c in range(len(lines[l])):
        if (lines[l][c] != '.'):
            pos.append((l, c))

def sub_a_b(a, b):
    r = 0
    if (a > b):
        r = a - b
    else:
        r = b - a
    return r

res = 0
for i in range(len(pos)):
    for j in range(i, len(pos)):
        if (i != j):
            res += sub_a_b(pos[i][0], pos[j][0])
            res += sub_a_b(pos[i][1], pos[j][1])

print(res)