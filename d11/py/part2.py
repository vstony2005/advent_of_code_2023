file = []
# read file
filename = "test.txt"
filename = "input.txt"
print(filename)
with open(filename, 'r') as filin:
    file = filin.readlines()

lines = []
for l in file:
    cols = []
    for c in l.strip():
        cols.append(c)
    lines.append(cols)


# empty lines
l_add = []
for i in range(0, len(lines)):
    is_empty = True
    for c in lines[i]:
        is_empty = is_empty and (c == '.')
    if (is_empty):
        l_add.append(i)


# empty cols
c_add = []
for i in range(len(lines[0])):
    is_empty = True
    for l in range(len(lines)):
        is_empty = is_empty and (lines[l][i] == '.')
    if (is_empty):
        c_add.append(i)


pos = []
for l in range(len(lines)):
    for c in range(len(lines[l])):
        if (lines[l][c] != '.'):
            pos.append((l, c))

def sub_a_b(a, b, vals):
    r = 0
    v_max = a if (a > b) else b
    v_min = a + b - v_max

    cols = 0
    for v in vals:
        if (v > v_min) and (v < v_max):
            cols += 1

    coef = 1000000
    # 10 -> 1030
    # 100 -> 8410
    # 1000000 -> ?
    r = v_max + (cols * (coef - 1)) - v_min

    return r

res = 0
for i in range(len(pos)):
    for j in range(i, len(pos)):
        if (i != j):
            res += sub_a_b(pos[i][0], pos[j][0], l_add)
            res += sub_a_b(pos[i][1], pos[j][1], c_add)

print(res)
