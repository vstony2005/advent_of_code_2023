import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    part1()
    //part2()
    println("end.")
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    //readfile2("input")
}

//region my types
operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, DIRECTIONS>>


enum class DIRECTIONS { UP, RIGHT, DOWN, LEFT }
val positions: HashMap<DIRECTIONS, PairInt> = hashMapOf(
    DIRECTIONS.UP to Pair(-1,0),
    DIRECTIONS.RIGHT to Pair(0,1),
    DIRECTIONS.DOWN to Pair(1,0),
    DIRECTIONS.LEFT to Pair(0,-1)
)
//endregion

fun divers() {
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val rules: HashMap<String,List<Operation>> = getRules(lines)
    val datas: List<HashMap<Char, Int>> = getDatas(lines)

    var res: Int = 0
    res = solveDatas(datas, rules)
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun solveDatas(datas: List<HashMap<Char, Int>>, rules: HashMap<String, List<Operation>>): Int {
    var res: Int = 0
    for (data in datas) {
        res += soveData(data, rules)
    }
    return res
}

fun soveData(data: HashMap<Char, Int>, rules: HashMap<String, List<Operation>>): Int {
    var code: String = "in"
    do {
        var found: Boolean = false
        val ops: List<Operation>? = rules[code]
        if (ops != null) {
            for (op in ops) {
                if (op.letter == ' ') {
                    code = op.toCode
                } else if (data.contains(op.letter)) {
                    when (op.operator) {
                        '<' -> {
                            if (data[op.letter]!! < op.value)
                                code = op.toCode
                            else
                                code = ""
                        }
                        '>' -> {
                            if (data[op.letter]!! > op.value)
                                code = op.toCode
                            else
                                code = ""
                        }
                        else -> "op ${op.operator} error".println()
                    }
                }

                when (code) {
                    "A" -> return data.values.sum()
                    "R" -> return 0
                }
                if (code.isNotEmpty())
                    break
            }
            found = code.isNotEmpty()
        }
    } while (found)
    return 0
}

fun getRules(lines: List<String>): HashMap<String, List<Operation>> {
    val rules: HashMap<String, List<Operation>> = hashMapOf()
    val reg: Regex = Regex("([a-z]+)\\{(.*)\\}")
    val reCode: Regex = Regex("([a-z])(.)([0-9]+):([A-Za-z]+)")
    lines.takeWhile { it.isNotEmpty() }.map { lin ->
        reg.findAll(lin).forEach { re ->
            val code: String = re.groups[1]!!.value
            rules[code] = re.groups[2]!!.value.split(",").map { str ->
                val match: MatchResult? = reCode.find(str)
                if (match != null) {
                    Operation(
                        code,
                        match.groups[1]!!.value.first(),
                        match.groups[2]!!.value.first(),
                        match.groups[3]!!.value.toInt(),
                        match.groups[4]!!.value
                    )
                } else {
                    Operation(
                        code,
                        ' ',
                        ' ',
                        0,
                        str
                    )
                }
            }
        }
    }
    return rules
}

fun getDatas(lines: List<String>): List<HashMap<Char, Int>> {
    val datas: List<HashMap<Char, Int>> = lines.takeLastWhile { it.isNotEmpty() }.map {
        it.drop(1).dropLast(1).split(",").map { vals ->
            vals[0] to vals.takeLastWhile { c -> c in '0'..'9' }.toInt()
        }.toMap() as HashMap<Char, Int>
    }
    return datas
}

data class Operation (
    val code: String,
    val letter: Char,
    val operator: Char,
    val value: Int,
    val toCode: String
)

fun readfile2(afile: String) {
}
