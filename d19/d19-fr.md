# ---  Jour 19 : L'abondance ---

## --- Première partie ---

Les Elfes de Gear Island vous remercient de votre aide et vous envoient sur le chemin. Ils ont même un deltaplane que quelqu'un a [volé](https://adventofcode.com/2023/day/9) sur l'île déserte ; comme vous allez déjà dans cette direction, cela les aiderait beaucoup si vous l'utilisiez pour descendre et leur rendre.

Alors que vous atteignez le bas de l'*avalanche incessante de pièces de machines*, vous découvrez qu'elles forment déjà un formidable amas. Mais ne vous inquiétez pas, un groupe d'Elfes est déjà là pour organiser les pièces, et ils ont un *système*.

Pour commencer, chaque pièce est classée dans quatre catégories :

- `x` : E*x*trêmement cool
- `m` : *M*usical (il fait du bruit quand on le frappe)
- `a` : *A*érodynamique
- `s` : Brillant(*S*)

Ensuite, chaque pièce est envoyée à travers une série de *flux de travail* qui vont finalement *accepter* ou *rejeter* la pièce. Chaque flux de travail porte un nom et contient une liste de *règles* ; chaque règle spécifie une condition et l'endroit où envoyer la pièce si la condition est vraie. La première règle qui correspond à la pièce considérée est appliquée immédiatement, et la pièce est acheminée vers la destination décrite par la règle. (La dernière règle de chaque flux de travail n'a pas de condition et s'applique toujours si elle est atteinte).

Considérons le flux de travail `ex{x>10:one,m<20:two,a>30:R,A}`. Ce flux de travail est nommé `ex` et contient quatre règles. Si le flux de travail `ex` considérait une pièce spécifique, il effectuerait les étapes suivantes dans l'ordre :

- Règle "`x>10:one`" : Si `x` de la pièce est supérieur à `10`, envoyer la pièce au workflow nommé `one`.
- Règle "`m<20:two`" : Sinon, si le `m` de la pièce est inférieur à `20`, envoyez la pièce au flux de travail nommé `two`.
- Règle "`a>30:R`" : Sinon, si le `a` de la pièce est supérieur à `30`, la pièce est immédiatement *rejetée* (`R`).
- Règle "`A`" : Sinon, comme aucune autre règle ne correspond à la pièce, celle-ci est immédiatement *acceptée* (`A`).

Si une pièce est envoyée à un autre flux de travail, elle passe immédiatement au début de ce flux et ne revient jamais. Si une pièce est *acceptée* (envoyée à `A`) ou *rejetée* (envoyée à `R`), le traitement de la pièce est immédiatement interrompu.

Le système fonctionne, mais il n'arrive pas à suivre le torrent de formes métalliques bizarres. Les Elfes vous demandent de les aider à trier quelques pièces et vous donnent la liste des flux de travail et quelques évaluations de pièces (votre entrée dans le puzzle). Par exemple :

```
px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}
```

Les flux de travail sont énumérés en premier, suivis d'une ligne vide, puis des cotes des pièces que les Elfes aimeraient que vous triiez. Toutes les parties commencent dans le flux de travail nommé `in`. Dans cet exemple, les cinq pièces listées passent par les flux de travail suivants :

- `{x=787,m=2655,a=1222,s=2876}`: `in` -> `qqz` -> `qs` -> `lnx` -> `*A*`
- `{x=1679,m=44,a=2067,s=496}`: `in` -> `px` -> `rfg` -> `gd` -> `*R*`
- `{x=2036,m=264,a=79,s=2244}`: `in` -> `qqz` -> `hdj` -> `pv` -> `*A*`
- `{x=2461,m=1339,a=466,s=291}`: `in` -> `px` -> `qkq` -> `crn` -> `*R*`
- `{x=2127,m=1623,a=2188,s=1013}`: `in` -> `px` -> `rfg` -> `*A*`

En fin de compte, trois parties sont *acceptées*. En additionnant les notes `x`, `m`, `a`, et `s` pour chacune des parties acceptées, on obtient `7540` pour la partie avec `x=787`, `4623` pour la partie avec `x=2036`, et `6951` pour la partie avec `x=2127`. L'addition de toutes les évaluations pour *toutes* les pièces acceptées donne une somme totale de `*19114*`.

Triez toutes les pièces qui vous ont été données ; *qu'est-ce que vous obtenez si vous additionnez toutes les notes de toutes les pièces qui ont été acceptées ?*

Pour commencer, [obtenez les données de votre puzzle] (https://adventofcode.com/2023/day/19/input).

## --- Deuxième partie ---

Même avec votre aide, le processus de tri n'est toujours pas assez rapide.

L'un des Elfes propose un nouveau plan : plutôt que de trier les pièces individuellement à travers tous ces flux de travail, peut-être pourriez-vous déterminer à l'avance quelles combinaisons d'évaluations seront acceptées ou rejetées.

Chacune des quatre évaluations (`x`, `m`, `a`, `s`) peut avoir une valeur entière allant d'un minimum de `1` à un maximum de `4000`. Parmi *toutes les combinaisons distinctes possibles* de notes, votre travail est de déterminer lesquelles seront *acceptées*.

Dans l'exemple ci-dessus, il y a `*167409079868000*` combinaisons distinctes de notes qui seront acceptées.

Considérez uniquement votre liste de flux de travail ; la liste des évaluations de pièces que les Elfes voulaient que vous triiez n'est plus pertinente. *Combien de combinaisons distinctes d'évaluations seront acceptées par les flux de travail des Elfes ?*