import java.io.File
import java.nio.file.Paths

fun main(args: Array<String>) {
    //part1()
    part2()
}

fun part1() {
    //readFile("E:\\datas\\src\\aoc\\d1\\test_1.txt")
    readFile1(".\\test_1.txt")
    readFile1(".\\input_1.txt")

    println(Paths.get("").toAbsolutePath().toString())
    println(System.getProperty("user.dir"))
}

fun readFile1(afile: String) {
    println(afile)

    var file = File(afile)
    if (!file.exists()) {
        println("file not exists")
        return
    }

    var res: Int = 0
    var i: Int

    file.forEachLine {
        println(it)
        val reg1 = Regex("[0-9]")
        i = (reg1.find(it)?.value?.toInt() ?: 0)
        res += i * 10
        println(i)

        val reg2 = Regex("([0-9])[^0-9]*$")
        i = (reg2.find(it)?.groupValues?.get(1)?.toInt() ?: 0)
        res += i
        println(i)
    }

    println(res)
}

fun part2() {
    readFile2(".\\test_2.txt")
    readFile2(".\\input_1.txt")
}

fun readFile2(afile: String) {
    println(afile)

    var file = File(afile)
    if (!file.exists()) {
        println("file not exists")
        return
    }

    var res: Int = 0
    var i: Int
    val dic = mapOf("one" to 1,"two" to 2,"three" to 3,"four" to 4,"five" to 5,"six" to 6,"seven" to 7,"eight" to 8,
                    "nine" to 9,"0" to 0,"1" to 1,"2" to 2,"3" to 3,"4" to 4,"5" to 5,"6" to 6,"7" to 7,"8" to 8,"9" to 9)

    file.forEachLine {
        println(it)
        val reg = Regex("one|two|three|four|five|six|seven|eight|nine|0|1|2|3|4|5|6|7|8|9")
        val vals = reg.findAll(it).map { it.value }.toList()

        if (vals.isNotEmpty()) {
            i = (dic[vals[0]]?.times(10) ?: 0) + dic[vals[vals.size-1]]!!
            println(i)
            res += i
            //println(vals)
        }
    }

    println(res)
}

