# --- Jour 24 : Ne me dites jamais les chances ---

## --- Première partie ---

On dirait que quelque chose ne va pas dans le processus de fabrication de la neige.  Au lieu de former de la neige, l'eau qui a été absorbée dans l'air semble former de la [grêle](https://en.wikipedia.org/wiki/Hail) !

Peut-être pouvez-vous faire quelque chose pour briser les grêlons ?

En raison de vents puissants, probablement magiques, les grêlons volent tous dans l'air selon des trajectoires parfaitement linéaires. Vous notez la *position* et la *vitesse* de chaque grêlon (votre entrée dans l'énigme). Voici un exemple :

```
19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3
```

Chaque ligne de texte correspond à la position et à la vitesse d'un seul grêlon. Les positions indiquent où se trouvent les grêlons *à l'instant présent* (au temps `0`). Les vitesses sont constantes et indiquent exactement la distance parcourue par chaque grêlon en *une nanoseconde*.

Chaque ligne de texte utilise le format `px py pz @ vx vy vz`. Par exemple, le grêlon spécifié par `20, 19, 15 @ 1, -5, -3` a une position initiale X `20`, Y `19`, Z `15`, une vitesse X `1`, Y `-5`, et Z `-3`. Après une nanoseconde, le grêlon serait à `21, 14, 12`.

Vous n'aurez peut-être rien à faire. Quelle est la probabilité que les grêlons entrent en collision les uns avec les autres et se transforment en minuscules cristaux de glace ?

Pour l'estimer, ne considérez que les axes X et Y ; *ignorez l'axe Z*. En regardant *dans le temps*, combien de *trajectoires* de grêlons se croiseront dans une zone d'essai ? (Les grêlons eux-mêmes n'ont pas besoin d'entrer en collision, il suffit de rechercher les intersections entre les trajectoires qu'ils traceront).

Dans cet exemple, recherchez les intersections qui se produisent avec une position X et Y d'au moins `7` et d'au plus `27` ; dans vos données réelles, vous devrez vérifier une zone de test beaucoup plus grande.  En comparant toutes les paires de trajectoires futures des grêlons, on obtient les résultats suivants :

```
Hailstone A: 19, 13, 30 @ -2, 1, -2
Hailstone B: 18, 19, 22 @ -1, -1, -2
Les trajectoires des grêlons se croisent à l'intérieur de la zone de test (at x=14.333, y=15.333).

Hailstone A: 19, 13, 30 @ -2, 1, -2
Hailstone B: 20, 25, 34 @ -2, -2, -4
Les trajectoires des grêlons se croisent à l'intérieur de la zone de test (at x=11.667, y=16.667).

Hailstone A: 19, 13, 30 @ -2, 1, -2
Hailstone B: 12, 31, 28 @ -1, -2, -1
Les trajectoires des grêlons se croisent en dehors de la zone de test (at x=6.2, y=19.4).

Hailstone A: 19, 13, 30 @ -2, 1, -2
Hailstone B: 20, 19, 15 @ 1, -5, -3
Les trajectoires des grêlons se sont croisées dans le passé pour le grêlon A.

Hailstone A: 18, 19, 22 @ -1, -1, -2
Hailstone B: 20, 25, 34 @ -2, -2, -4
Les trajectoires des pierres de grêle sont parallèles ; elles ne se croisent jamais.

Hailstone A: 18, 19, 22 @ -1, -1, -2
Hailstone B: 12, 31, 28 @ -1, -2, -1
Les trajectoires des grêlons se croisent en dehors de la zone de test (at x=-6, y=-5).

Hailstone A: 18, 19, 22 @ -1, -1, -2
Hailstone B: 20, 19, 15 @ 1, -5, -3
Les trajectoires des grêlons se sont croisées dans le passé pour les deux grêlons.

Hailstone A: 20, 25, 34 @ -2, -2, -4
Hailstone B: 12, 31, 28 @ -1, -2, -1
Les trajectoires des grêlons se croisent en dehors de la zone de test (at x=-2, y=3).

Hailstone A: 20, 25, 34 @ -2, -2, -4
Hailstone B: 20, 19, 15 @ 1, -5, -3
Les trajectoires des grêlons se sont croisées dans le passé pour le grêlon B.

Hailstone A: 12, 31, 28 @ -1, -2, -1
Hailstone B: 20, 19, 15 @ 1, -5, -3
Les trajectoires des grêlons se sont croisées dans le passé pour les deux grêlons.
```

Ainsi, dans cet exemple, les trajectoires futures de `*2*` grêlons se croisent à l'intérieur des limites de la zone de test.

Cependant, vous devrez rechercher une zone de test beaucoup plus grande si vous voulez voir si des grêlons risquent d'entrer en collision. Recherchez les intersections qui se produisent avec une position X et Y au moins égale à `200000000000000` et au plus égale à `400000000000000`. Ne tenez pas compte de l'axe Z.

En ne tenant compte que des axes X et Y, vérifiez si toutes les trajectoires futures des paires de grêlons se croisent. *Combien de ces intersections se produisent dans la zone de test ?*

Pour commencer, [obtenez les données de votre puzzle](https://adventofcode.com/2023/day/24/input).