# --- Jour 21 : Compteur d'étapes ---

## --- Première partie ---

Vous avez réussi à attraper le [Dirigeable](https://adventofcode.com/2023/day/7) au moment où il déposait quelqu'un d'autre pour un voyage tous frais payés sur l'Ile déserte ! Il vous dépose même près du [jardinier](https://adventofcode.com/2023/day/5) et de son immense ferme.

"Vous avez fait couler le sable à nouveau ! Beau travail ! Il ne nous reste plus qu'à attendre d'avoir assez de sable pour filtrer l'eau de Snow Island et nous aurons à nouveau de la neige en un rien de temps."

Pendant que vous attendez, l'un des Elfes qui travaille avec le jardinier a entendu dire que vous étiez doué pour résoudre les problèmes et aimerait que vous l'aidiez. Il a besoin de faire ses [pas] (https://en.wikipedia.org/wiki/Pedometer) pour la journée, et il aimerait savoir *quelles parcelles de jardin il peut atteindre avec exactement ses `64` pas restants*.

Il vous donne une carte à jour (votre entrée de puzzle) de sa position de départ (`S`), des parcelles de jardin (`.`), et des rochers (`#`). Par exemple :

```
...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........
```

L'Elfe commence à la position de départ (`S`) qui compte également comme une parcelle de jardin. Ensuite, il peut faire un pas vers le nord, le sud, l'est ou l'ouest, mais seulement sur des tuiles qui sont des parcelles de jardin. Cela lui permet d'atteindre n'importe laquelle des tuiles marquées `O` :

```
...........
.....###.#.
.###.##..#.
..#.#...#..
....#O#....
.##.OS####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........
```

Ensuite, il fait un deuxième pas. Puisqu'à ce stade il peut être sur *l'une ou l'autre* des tuiles marquées `O`, son second pas lui permettra d'atteindre n'importe quelle parcelle de jardin située à un pas au nord, au sud, à l'est ou à l'ouest de *n'importe quelle* tuile qu'il aurait pu atteindre après le premier pas :

```
...........
.....###.#.
.###.##..#.
..#.#O..#..
....#.#....
.##O.O####.
.##.O#...#.
.......##..
.##.#.####.
.##..##.##.
...........
```

Après deux pas, il peut se trouver sur n'importe laquelle des tuiles marquées "O" ci-dessus, y compris la position de départ (soit en allant du nord au sud, soit en allant de l'ouest à l'est).

Un troisième pas mène à encore plus de possibilités :

```
...........
.....###.#.
.###.##..#.
..#.#.O.#..
...O#O#....
.##.OS####.
.##O.#...#.
....O..##..
.##.#.####.
.##..##.##.
...........
```

Il continuera ainsi jusqu'à ce qu'il ait épuisé ses pas pour la journée. Après un total de `6` pas, il peut atteindre n'importe quelle parcelle de jardin marquée ``O` :

```
...........
.....###.#.
.###.##.O#.
.O#O#O.O#..
O.O.#.#.O..
.##O.O####.
.##.O#O..#.
.O.O.O.##..
.##.#.####.
.##O.##.##.
...........
```

Dans cet exemple, si l'objectif de l'Elfe était de faire exactement `6` pas de plus aujourd'hui, il pourrait les utiliser pour atteindre n'importe laquelle des `*16*` parcelles de jardin.

Cependant, l'Elfe *doit en fait faire `64` pas aujourd'hui*, et la carte qu'il vous a remise est beaucoup plus grande que la carte de l'exemple.

En partant de la parcelle de jardin marquée " S " sur votre carte, *combien de parcelles de jardin l'elfe pourrait-il atteindre en exactement `64` pas ?*

Pour commencer, [obtenez l'entrée de votre puzzle] (https://adventofcode.com/2023/day/21/input).

## --- Deuxième partie ---

L'Elfe semble confus par votre réponse jusqu'à ce qu'il se rende compte de son erreur : il lisait une liste de ses nombres préférés qui sont à la fois des carrés et des cubes parfaits, et non pas son compteur de pas.

Le nombre *réel* de pas qu'il doit faire aujourd'hui est exactement `*26501365*`.

Il souligne également que les parcelles de jardin et les rochers sont disposés de telle sorte que la carte *se répète à l'infini* dans toutes les directions.

Ainsi, si vous regardez une largeur ou une hauteur de carte supplémentaire à partir du bord de la carte ci-dessus, vous constaterez qu'elle se répète sans cesse :

```
.................................
.....###.#......###.#......###.#.
.###.##..#..###.##..#..###.##..#.
..#.#...#....#.#...#....#.#...#..
....#.#........#.#........#.#....
.##...####..##...####..##...####.
.##..#...#..##..#...#..##..#...#.
.......##.........##.........##..
.##.#.####..##.#.####..##.#.####.
.##..##.##..##..##.##..##..##.##.
.................................
.................................
.....###.#......###.#......###.#.
.###.##..#..###.##..#..###.##..#.
..#.#...#....#.#...#....#.#...#..
....#.#........#.#........#.#....
.##...####..##..S####..##...####.
.##..#...#..##..#...#..##..#...#.
.......##.........##.........##..
.##.#.####..##.#.####..##.#.####.
.##..##.##..##..##.##..##..##.##.
.................................
.................................
.....###.#......###.#......###.#.
.###.##..#..###.##..#..###.##..#.
..#.#...#....#.#...#....#.#...#..
....#.#........#.#........#.#....
.##...####..##...####..##...####.
.##..#...#..##..#...#..##..#...#.
.......##.........##.........##..
.##.#.####..##.#.####..##.#.####.
.##..##.##..##..##.##..##..##.##.
.................................
```

Ceci n'est qu'une minuscule tranche de trois cartes par trois cartes de la ferme inexplicablement infinie ; les parcelles de jardin et les rochers se répètent aussi loin que l'on puisse voir. L'Elfe commence toujours sur la tuile centrale marquée `S`, cependant - tous les autres `S` répétés sont remplacés par une parcelle de jardin normale (`.`).

Voici le nombre de parcelles de jardin atteignables dans cette nouvelle version infinie de la carte d'exemple pour différents nombres d'étapes :

- En exactement `6` pas, il peut encore atteindre `*16*` parcelles de jardin.
- En exactement `10` pas, il peut atteindre n'importe laquelle des `*50*` parcelles de jardin.
- En exactement `50` pas, il peut atteindre `*1594*` parcelles de jardin.
- En exactement `100` pas, il peut atteindre `*6536*` parcelles de jardin.
- En exactement `500` pas, il peut atteindre `*167004*` parcelles de jardin.
- En exactement `1000` pas, il peut atteindre `*668697*` parcelles de jardin.
- En exactement `5000` pas, il peut atteindre `*16733044*` parcelles de jardin.

Cependant, le nombre de pas dont l'Elfe a besoin est beaucoup plus important ! En partant de la parcelle de jardin marquée `S` sur votre carte infinie, *combien de parcelles de jardin l'Elfe pourrait-il atteindre en exactement `26501365` pas?*
