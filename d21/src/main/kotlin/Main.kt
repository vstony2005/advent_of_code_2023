import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun part1() {
    //readfile1("test")
    readfile1("input21")
}

fun part2() {
    readfile2("test")
    //readfile2("input21")
}

//region my types
operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, Direction>>


enum class Direction (val pos: PairInt) {
    UP (Pair(-1,0)),
    RIGHT (Pair(0,1)),
    DOWN (Pair(1,0)),
    LEFT (Pair(0,-1))
}
//endregion

fun divers() {
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()
    val grid: MutListChars = mutableListOf()
    lines.map { grid.add(it.toMutableList()) }
    grid.joinToString("\n").println()

    val moves: MutableList<PairInt> = mutableListOf()
    lines@ for (l in grid.indices)
        for (c in grid[l].indices)
            if (grid[l][c] == 'S') {
                moves.add(Pair(l, c))
                break@lines
            }

    moves.println()

    repeat (64) {
        //"loop: ${it + 1}"
        moveOneStepV1(moves, grid)
        //moves.println()
    }

    var res: Int = moves.size
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

val dirs: List<PairInt> = listOf(Pair(0,-1),Pair(1,0),Pair(0,1),Pair(-1,0))

fun moveOneStepV1(moves: MutableList<PairInt>, grid: ListChars) {
    val height: Int = grid.size
    val width: Int = grid[0].size
    val newMoves: MutableList<PairInt> = mutableListOf()

    for (move in moves) {
        for (d in dirs) {
            val newPos = move + d
            if (newPos.first < 0 || newPos.first > width-1 || newPos.second < 0 || newPos.second > height-1)
                continue
            if (grid[newPos] != '#' && !newMoves.contains(newPos))
                newMoves.add(newPos)
        }
    }

    moves.clear()
    moves.addAll(newMoves)
}
fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()
    val grid: MutListChars = mutableListOf()
    lines.map { grid.add(it.toMutableList()) }
    grid.joinToString("\n").println()

    val moves: MutableList<PairInt> = mutableListOf()
    lines@ for (l in grid.indices)
        for (c in grid[l].indices)
            if (grid[l][c] == 'S') {
                moves.add(Pair(l, c))
                break@lines
            }

    repeat (5000) {
        //"loop: ${it + 1}"
        moveOneStepV2(moves, grid)
        //moves.println()
    }

    var res: Int = moves.size
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun moveOneStepV2(moves: MutableList<PairInt>, grid: ListChars) {
    val height: Int = grid.size
    val width: Int = grid[0].size
    val newMoves: MutableList<PairInt> = mutableListOf()

    for (move in moves) {
        for (d in dirs) {
            val newPos: PairInt = move + d
            var posCtrl: PairInt = Pair(newPos.first % width, newPos.second % height)

            if (posCtrl.first < 0)
                posCtrl = Pair(posCtrl.first + height, posCtrl.second)
            else if (posCtrl.first > height - 1)
                posCtrl = Pair(0, posCtrl.second)

            if (posCtrl.second < 0)
                posCtrl = Pair(posCtrl.first, posCtrl.second + width)
            else if (posCtrl.second > width - 1)
                posCtrl = Pair(posCtrl.first, 0)

            if (grid[posCtrl] != '#' && !newMoves.contains(newPos))
                newMoves.add(newPos)
        }
    }

    moves.clear()
    moves.addAll(newMoves)
}
