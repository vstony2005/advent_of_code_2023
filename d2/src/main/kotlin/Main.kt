import java.io.File

fun main(args: Array<String>) {
    //part1()
    part2()
}

fun part1() {
    readfile1(".\\test1.txt")
    readfile1(".\\input1.txt")
}

fun part2() {
    readfile2(".\\test1.txt")
    readfile2(".\\input1.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var limits = mapOf("red" to 12, "green" to 13, "blue" to 14)
    var res: Int = 0

    f.forEachLine {
        val num: Int = Regex("Game ([0-9]+)").find(it)?.groupValues?.get(1)?.toInt() ?: 0
        val games: String = it.split(":")[1]

        var color: String
        var nb: Int

        println("- Game $num")
        games.split(";").forEach games@{
            // list games
            println(it)
            it.split(",").forEach {
                // list colors
                var regRes = Regex("([0-9]+) ([a-z]+)").find(it)
                nb = regRes?.groupValues?.get(1)?.toInt() ?: 0
                color = regRes?.groupValues?.get(2) ?: ""

                if (nb > limits[color]!!) {
                    return@forEachLine
                }
            }
        }
        println("Game $num ok")
        res += num
    }

    println("res: $res")
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Int = 0

    f.forEachLine {
        val num: Int = Regex("Game ([0-9]+)").find(it)?.groupValues?.get(1)?.toInt() ?: 0
        val games: String = it.split(":")[1]

        var color: String
        var nb: Int

        var nbColors = mutableMapOf("red" to 0, "green" to 0, "blue" to 0)

        println("- Game $num")
        games.split(";").forEach games@{
            // list games
            println(it)
            it.split(",").forEach {
                // list colors
                var regRes = Regex("([0-9]+) ([a-z]+)").find(it)
                nb = regRes?.groupValues?.get(1)?.toInt() ?: 0
                color = regRes?.groupValues?.get(2) ?: ""

                if (nb > nbColors[color]!!) {
                    nbColors[color] = nb
                }
            }
        }
        println("Game $num")
        println(nbColors)
        res += nbColors["red"]!! * nbColors["green"]!! * nbColors["blue"]!!
    }

    println("res: $res")
}
