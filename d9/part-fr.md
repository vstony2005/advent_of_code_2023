# --- Jour 9 : Maintenance du Mirage ---

## --- Première partie ---

Vous montez le chameau à travers la tempête de sable et vous vous arrêtez là où les cartes du fantôme vous ont dit de vous arrêter. La tempête de sable se calme ensuite, et vous vous retrouvez dans une *oasis* !

Le chameau va chercher de l'eau et vous vous étirez le cou. En levant les yeux, vous découvrez ce qui doit être une autre île flottante géante, celle-ci en métal ! C'est de là que viennent les *pièces pour réparer les machines à sable*.

Il y a même un [deltaplane] (https://en.wikipedia.org/wiki/Hang_gliding) partiellement enterré dans le sable ici ; une fois que le soleil se lève et réchauffe le sable, vous pourrez peut-être utiliser le deltaplane et l'air chaud pour monter jusqu'à l'île de métal !

En attendant que le soleil se lève, vous admirez l'oasis cachée au milieu de l'île déserte. Elle doit avoir un écosystème délicat ; vous pourriez faire quelques relevés écologiques pendant que vous attendez. Peut-être pourriez-vous signaler à quelqu'un les instabilités environnementales que vous trouverez, afin que l'oasis soit toujours là pour le prochain voyageur usé par les tempêtes de sable.

Vous sortez votre *détecteur d'instabilité de l'oasis et du sable* et analysez votre environnement. L'OASIS produit un rapport contenant de nombreuses valeurs et leur évolution dans le temps (les données de votre puzzle). Chaque ligne du rapport contient l'histoire d'une seule valeur. Par exemple :

```
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
```

Pour protéger au mieux l'oasis, votre rapport environnemental doit inclure une *prédiction de la valeur suivante* dans chaque historique. Pour ce faire, commencez par créer une nouvelle séquence à partir de la *différence à chaque étape* de votre historique. Si cette séquence n'est *pas* entièrement nulle, répétez ce processus en utilisant la séquence que vous venez de générer comme séquence d'entrée. Une fois que toutes les valeurs de votre dernière séquence sont nulles, vous pouvez extrapoler la valeur suivante de l'historique original.

Dans l'ensemble de données ci-dessus, le premier historique est `0 3 6 9 12 15`. Comme les valeurs augmentent de `3` à chaque étape, la première séquence de différences que vous générez sera `3 3 3 3 3`. Notez que cette séquence a une valeur de moins que la séquence d'entrée parce qu'à chaque étape, elle considère deux nombres de l'entrée. Puisque ces valeurs ne sont pas *toutes à zéro*, répétez le processus : les valeurs diffèrent de `0` à chaque étape, donc la séquence suivante est `0 0 0 0`. Cela signifie que vous avez suffisamment d'informations pour extrapoler l'historique ! Visuellement, ces séquences peuvent être organisées comme suit :

```
0   3   6   9  12  15
  3   3   3   3   3
    0   0   0   0
```

Pour extrapoler, commencez par ajouter un nouveau zéro à la fin de votre liste de zéros ; comme les zéros représentent des différences entre les deux valeurs qui les précèdent, cela signifie aussi qu'il y a maintenant un espace réservé dans chaque séquence qui la précède :

```
0   3   6   9  12  15   B
  3   3   3   3   3   A
    0   0   0   0   0
```

Vous pouvez alors commencer à remplir les espaces réservés de bas en haut. `A` doit être le résultat de l'augmentation de `3` (la valeur à sa gauche) par `0` (la valeur en dessous) ; cela signifie que `A` doit être `*3*` :

```
0   3   6   9  12  15   B
  3   3   3   3   3   3
    0   0   0   0   0
```

Enfin, vous pouvez remplir `B`, qui doit être le résultat de l'augmentation de `15` (la valeur à sa gauche) par `3` (la valeur en dessous), ou `*18*` :

```
0   3   6   9  12  15  18
  3   3   3   3   3   3
    0   0   0   0   0
```

Ainsi, la valeur suivante du premier historique est `*18*`.

La recherche de différences entièrement nulles pour le deuxième historique nécessite une séquence supplémentaire :

```
1   3   6  10  15  21
  2   3   4   5   6
    1   1   1   1
      0   0   0
```

Ensuite, en suivant le même processus que précédemment, calculez la valeur suivante dans chaque séquence, de bas en haut :

```
1   3   6  10  15  21  28
  2   3   4   5   6   7
    1   1   1   1   1
      0   0   0   0
```

Ainsi, la prochaine valeur de la deuxième histoire est `*28*`.

La troisième histoire nécessite encore plus de séquences, mais sa prochaine valeur peut être trouvée de la même manière :

```
10  13  16  21  30  45  68
   3   3   5   9  15  23
     0   2   4   6   8
       2   2   2   2
         0   0   0
```

Ainsi, la valeur suivante de la troisième histoire est `*68*`.

Si vous trouvez la valeur suivante pour chaque historique dans cet exemple et que vous les additionnez, vous obtenez `*114*`.

Analysez votre rapport OASIS et extrapolez la valeur suivante pour chaque historique. *Quelle est la somme de ces valeurs extrapolées ?

Pour commencer, [obtenez les données de votre puzzle] (https://adventofcode.com/2023/day/9/input).

## --- Deuxième partie ---

Bien sûr, il serait agréable d'avoir *encore plus d'historique* dans votre rapport. Il n'est donc pas dangereux de procéder à une *extrapolation vers l'arrière*, n'est-ce pas ?

Pour chaque historique, répétez le processus de recherche des différences jusqu'à ce que la séquence des différences soit entièrement nulle. Ensuite, au lieu d'ajouter un zéro à la fin et de remplir les valeurs suivantes de chaque séquence précédente, vous devriez plutôt ajouter un zéro au *début* de votre séquence de zéros, puis remplir de nouvelles *premières* valeurs pour chaque séquence précédente.

En particulier, voici à quoi ressemble l'historique du troisième exemple lorsque l'on extrapole dans le temps :

```
5  10  13  16  21  30  45
  5   3   3   5   9  15
   -2   0   2   4   6
      2   2   2   2
        0   0   0
```

En ajoutant les nouvelles valeurs à gauche de chaque séquence, de bas en haut, on obtient la nouvelle valeur historique la plus à gauche : `*5*`.

En procédant de la même manière pour les autres données de l'exemple ci-dessus, on obtient les valeurs précédentes de `*-3*` pour le premier historique et de `*0*` pour le deuxième historique. En additionnant les trois nouvelles valeurs, on obtient `*2*`.

Analysez à nouveau votre rapport OASIS, en extrapolant cette fois la valeur *précédente* pour chaque historique. *Quelle est la somme de ces valeurs extrapolées ?
