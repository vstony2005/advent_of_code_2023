import java.io.File

fun main(args: Array<String>) {
    part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1(".\\test.txt")
    readfile1(".\\input.txt")
}

fun part2() {
    readfile2(".\\test.txt")
    readfile2(".\\input.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        lines.add(line)
    }

    var res: Int = 0

    val reg = Regex("(\\-?[0-9]+)")
    for (line in lines) {
        if (line.isEmpty())
            continue
        val list: List<Int> = reg.findAll(line).map{ it.value.toInt() }.toList()
        res += getNextV1(list)
    }

    println(res)
}

fun getNextV1(list: List<Int>): Int {
    var l1: MutableList<List<Int>> = mutableListOf()
    l1.add(list.toList())

    do {
        var isAll0: Boolean = true
        val last = l1.last()
        val newList: MutableList<Int> = mutableListOf()
        for (i in 1..<last.size) {
            val newVal: Int = last[i] - last[i-1]
            newList.add(newVal)
            isAll0 = isAll0 and (newVal == 0)
        }
        l1.add(newList)
    } while (!isAll0)

    var res: Int = 0
    for (i in l1.size-1 downTo 0) {
        res += l1[i].last()
    }

    return res
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        lines.add(line)
    }

    var res: Int = 0

    val reg = Regex("(\\-?[0-9]+)")
    for (line in lines) {
        if (line.isEmpty())
            continue
        val list: List<Int> = reg.findAll(line).map{ it.value.toInt() }.toList()
        res += getNextV2(list)
    }

    println(res)
}

fun getNextV2(list: List<Int>): Int {
    var l1: MutableList<List<Int>> = mutableListOf()
    l1.add(list.toList())

    do {
        var isAll0: Boolean = true
        val last = l1.last()
        val newList: MutableList<Int> = mutableListOf()
        for (i in 1..<last.size) {
            val newVal: Int = last[i] - last[i-1]
            newList.add(newVal)
            isAll0 = isAll0 and (newVal == 0)
        }
        l1.add(newList)
    } while (!isAll0)

    var res: Int = 0
    for (i in l1.size-1 downTo 0) {
        val first = l1[i].first()
        res = -res + first
    }

    return res
}
