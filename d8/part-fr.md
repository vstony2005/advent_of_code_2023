# --- Jour 8 : Friche hantée ---

## --- Première partie ---

Vous traversez l'île déserte à dos de chameau lorsque vous apercevez une tempête de sable qui approche à grands pas. Lorsque vous vous retournez pour avertir l'Elfe, elle disparaît sous vos yeux ! Pour être honnête, elle avait fini de vous mettre en garde contre les *fantômes* il y a quelques minutes.

L'une des poches du chameau est étiquetée "cartes" - c'est vrai qu'elle est pleine de documents (que vous avez introduits dans le puzzle) sur la façon de naviguer dans le désert. L'un des documents contient une liste d'instructions gauche/droite, et les autres documents semblent décrire une sorte de *réseau* de nœuds étiquetés.

Il semble que vous soyez censé utiliser les instructions *gauche/droite* pour *naviguer dans le réseau*. Peut-être que si vous demandez au chameau de suivre les mêmes instructions, vous pourrez échapper au désert hanté !

Après avoir examiné les cartes pendant un certain temps, deux nœuds ressortent : `AAA` et `ZZZ`. Vous avez l'impression que `AAA` est l'endroit où vous êtes maintenant, et que vous devez suivre les instructions gauche/droite jusqu'à ce que vous atteigniez `ZZZ`.

Ce format définit chaque *nœud* du réseau individuellement. Par exemple, ce format définit chaque *nœud* du réseau individuellement :

```
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
```

En commençant par `AAA`, vous devez *chercher l'élément suivant* en vous basant sur l'instruction gauche/droite suivante dans votre entrée. Dans cet exemple, commencez par `AAA` et allez *à droite* (`R`) en choisissant l'élément de droite de `AAA`, `*CCC*`. Ensuite, `L` signifie choisir l'élément *gauche* de `CCC`, `*ZZZ*`. En suivant les instructions gauche/droite, vous atteignez `ZZZ` en `*2*` étapes.

Bien sûr, il se peut que vous ne trouviez pas `ZZZ` tout de suite. Si vous n'avez plus d'instructions gauche/droite, répétez toute la séquence d'instructions si nécessaire : RL" signifie en réalité "RLRLRLRLRLRLRLRLRLRL...` et ainsi de suite. Par exemple, voici une situation qui nécessite `*6*` étapes pour atteindre `ZZZ` :

```
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
```

En partant de `AAA`, suivez les instructions gauche/droite. *Combien d'étapes sont nécessaires pour atteindre `ZZZ` ?

Pour commencer, [obtenez l'entrée de votre puzzle] (https://adventofcode.com/2023/day/8/input).

## --- Deuxième partie ---

La tempête de sable s'abat sur vous et vous n'êtes pas près d'échapper au désert. Vous avez demandé au chameau de suivre les instructions, mais vous avez à peine quitté votre position de départ. Il va falloir faire *beaucoup plus de pas* pour s'échapper !

Et si la carte n'était pas pour les gens - et si la carte était pour les *fantômes* ? Les fantômes sont-ils liés aux lois de l'espace-temps ? Il n'y a qu'une seule façon de le savoir.

Après avoir examiné les cartes un peu plus longtemps, votre attention est attirée par un fait curieux : le nombre de nœuds dont le nom se termine par "A" est égal au nombre de nœuds dont le nom se termine par "Z" ! Si vous étiez un fantôme, vous devriez probablement *démarrer à chaque nœud se terminant par `A`* et suivre tous les chemins en même temps jusqu'à ce qu'ils aboutissent tous simultanément à des nœuds se terminant par `Z`.

Par exemple :

```
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
```

Ici, il y a deux nœuds de départ, `11A` et `22A` (parce qu'ils se terminent tous les deux par `A`). Lorsque vous suivez chaque instruction gauche/droite, utilisez cette instruction pour vous éloigner *simultanément* des deux nœuds sur lesquels vous vous trouvez. Répétez ce processus jusqu'à ce que *tous* les noeuds sur lesquels vous vous trouvez se terminent par `Z`. (Si seulement certains des noeuds sur lesquels vous vous trouvez se terminent par `Z`, ils agissent comme n'importe quel autre noeud et vous continuez normalement). Dans cet exemple, vous devez procéder comme suit :

- Etape 0 : Vous êtes à `11A` et `22A`.
- Etape 1 : Vous choisissez tous les chemins *gauche*, qui vous mènent à `11B` et `22B`.
- Etape 2 : Vous choisissez tous les chemins *droits*, qui vous mènent à `*11Z*` et `22C`.
- Etape 3 : Vous choisissez tous les chemins *gauche*, qui vous mènent à `11B` et `*22Z*`.
- Etape 4 : Vous choisissez tous les chemins *droits*, qui vous mènent à `*11Z*` et `22B`.
- Etape 5 : Vous choisissez tous les chemins *gauche*, qui vous mènent à `11B` et `22C`.
- Étape 6 : Vous choisissez tous les chemins *droits*, ce qui vous mène à `*11Z*` et `*22Z*`.

Donc, dans cet exemple, vous vous retrouvez entièrement sur les noeuds qui se terminent par `Z` après `*6*` étapes.

Commencez simultanément sur tous les noeuds qui se terminent par `A`. *Combien de pas faut-il pour se retrouver uniquement sur les noeuds se terminant par `Z` ?
