import java.io.File

fun main(args: Array<String>) {
    part1()
    part2()
    println("end.")
}

fun myTests() {
    println(firstMulti(listOf<ULong>(2u, 10u, 5u, 4u)))
    println(firstMulti(listOf<ULong>(2u, 10u, 5u, 3u, 4u)))
}

fun part1() {
    readfile1(".\\test1.txt")
    readfile1(".\\test1-2.txt")
    readfile1(".\\input1.txt")
}

fun part2() {
    readfile2(".\\test2.txt")
    readfile2(".\\input1.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    var res: Int = 0

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        lines.add(line)
    }

    val instructions: String = lines[0]
    lines.removeAt(0)
    lines.removeAt(0)

    val nodes: MutableMap<String, List<String>> = mutableMapOf<String, List<String>>()

    var reg = Regex("([A-Z]+) = \\(([A-Z]+), ([A-Z]+)\\)")
    for (line in lines) {
        if (line.isEmpty())
            continue
        val s1 = reg.find(line)!!.groupValues[1]
        val s2 = reg.find(line)!!.groupValues[2]
        val s3 = reg.find(line)!!.groupValues[3]

        nodes[s1] = listOf(s2, s3)
    }

    var currentNode: String = "AAA"
    while (currentNode != "ZZZ") {
        for (c in instructions) {
            res += 1
            var i = if (c == 'L') 0 else 1
            currentNode = nodes[currentNode]!![i]
            if (currentNode == "ZZZ")
                break
        }
    }

    println(res)
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        lines.add(line)
    }

    val instructions: String = lines[0]
    lines.removeAt(0)
    lines.removeAt(0)

    val nodes: MutableMap<String, Map<Char, String>> = mutableMapOf()
    var currentNodes: MutableList<String> = mutableListOf()

    var reg = Regex("([0-9A-Z]+) = \\(([0-9A-Z]+), ([0-9A-Z]+)\\)")
    for (line in lines) {
        if (line.isEmpty())
            continue
        val s1 = reg.find(line)!!.groupValues[1]
        val s2 = reg.find(line)!!.groupValues[2]
        val s3 = reg.find(line)!!.groupValues[3]

        if (s1.endsWith('A'))
            currentNodes.add(s1)

        nodes[s1] = mapOf('L' to s2, 'R' to s3)
    }

    var nb: MutableList<ULong> = mutableListOf()
    for (current in currentNodes.indices) {
        var n: ULong = 0u
        while (!currentNodes[current].endsWith('Z')) {
            for (c in instructions) {
                n += 1u
                currentNodes[current] = nodes[currentNodes[current]]!![c]!!
                if (currentNodes[current].endsWith('Z'))
                    break
            }
        }
        nb.add(n)
    }

    println(firstMulti(nb))
}

fun firstMulti(aList: List<ULong>): ULong {
    val l = aList.sorted()
    var n: ULong = 0.toULong()
    for (i in l.indices) {
        if (n == 0.toULong()) {
            n = l[i]
            continue
        }
        if (l[i] == n) continue

        n = l[i] * n / euclide(l[i], n)
    }
    return n
}

fun euclide(a: ULong, b: ULong): ULong {
    var a1 = a
    var b1 = b
    while (b1 != 0.toULong()) {
        val t = b1
        b1 = a1 % b1
        a1 = t
    }

    return a1
}
