import java.io.File
import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    divers()
    //part1()
    part2()
    println("end.")
}

fun divers() {
    var list5 = List<Int>(5) { 0 }
    println(list5)
    // (0,0,0,0,0)
    println(list5.joinToString("-"))
}

fun part1() {
    readfile1(".\\test.txt") // 21
    readfile1(".\\input.txt") // 7191
}

fun part2() {
    readfile2(".\\test.txt") // 525152
    //readfile2(".\\input.txt")
}

class Record(aCode: String, aValues: List<Int>) {
    val code: String = aCode
    val values: List<Int> = aValues
    val listCodes: MutableList<String> = mutableListOf()
    val nbValid: Int

    init {
        searchCodes()
        nbValid = listCodes.size
    }

    private fun searchCodes() {
        val strings: MutableList<String> = mutableListOf()
        var isFirst: Boolean = true

        for (c in code) {
            if (isFirst) {
                isFirst = false
                if (c == '?') {
                    strings.add(".")
                    strings.add("#")
                } else {
                    strings.add(c.toString())
                }
            } else {
                if (c == '?') {
                    for (i in strings.indices) {
                        strings.add(strings[i] + '.')
                        strings[i] = strings[i] + '#'
                    }
                } else {
                    for (i in strings.indices) {
                        strings[i] = strings[i] + c
                    }
                }
            }
            if (strings.isEmpty())
                break
        }
        for (str in strings) {
            if (isStrValid(str)) {
                listCodes.add(str)
            }
        }
    }

    private fun isStrValid(aCode: String): Boolean {
        //println(aCode)
        //println(values)
        if (aCode.contains("\\?"))
            return false
        val lst: List<String> = aCode.replace('.',' ')
            .trim()
            .replace("\\s+".toRegex(), " ")
            .split(" ")
        //println(lst)
        if (lst.size != values.size)
            return false
        for (i in values.indices) {
            if (values[i] != lst[i].length)
                return false
        }
        //println("true")
        return true
    }

    override fun toString(): String {
        return "$code - $values = $nbValid"
    }
}

class Record5(aCode: String, aValues: List<Int>) {
    val code: String
    val values: List<Int>
    val listCodes: MutableList<String> = mutableListOf()
    val nbValid: Int

    init {
        var list5 = List<String>(5) { aCode }
        //code = aCode.repeat(5)
        code = list5.joinToString("?")
        var l: MutableList<Int> = mutableListOf()
        repeat(5){
            l.addAll(aValues)
        }
        values = l
        //searchCodes()
        nbValid = listCodes.size
    }

    fun getCodeValues() : String =
        "$code ${values.joinToString(",")}"

    private fun isStartValid(aCode: String): Boolean {
        val lst: List<String> = aCode.replace('.',' ')
            .trim()
            .replace("\\s+".toRegex(), " ")
            .split(" ")

        //println(aCode)
        //println(lst)
        //println(values)
        if (lst.size > values.size)
            return false

        for (i in lst.indices) {
            if (i == lst.size-1) {
                if (lst[i].length > values[i])
                    return false
            } else if (lst[i].length != values[i])
                return false
        }
        //println(true)
        return true
    }

    private fun searchCodes() {
        val strings: MutableList<String> = mutableListOf()
        var str: String
        var isFirst: Boolean = true

        for (c in code) {
            //val car: Char = if (c == '?') ' ' else c
            if (isFirst) {
                isFirst = false
                if (c == '?') {
                    strings.add(".")
                    strings.add("#")
                } else {
                    strings.add(c.toString())
                }
            } else {
                if (c == '?') {
                    for (i in strings.indices) {
                        str = strings[i] + '.'
                        if (isStartValid(str))
                            strings.add(str)
                        str = strings[i] + '#'
                        if (isStartValid(str))
                            strings[i] = str
                    }
                } else {
                    for (i in strings.size-1 downTo 0) {
                        str = strings[i] + c
                        if (isStartValid(str))
                            strings[i] = str
                        else
                            strings.removeAt(i)
                    }
                }
                if (strings.isEmpty())
                    break
            }
        }
        for (st2 in strings) {
            if (isStrValid(st2)) {
                listCodes.add(st2)
            }
        }
    }

    private fun isStrValid(aCode: String): Boolean {
        //println(aCode)
        //println(values)
        if (aCode.contains("\\?"))
            return false
        val lst: List<String> = aCode.replace('.',' ')
            .trim()
            .replace("\\s+".toRegex(), " ")
            .split(" ")
        //println(lst)
        if (lst.size != values.size)
            return false
        for (i in values.indices) {
            if (values[i] != lst[i].length)
                return false
        }
        //println("true")
        return true
    }

    override fun toString(): String =
        "$code - $values = $nbValid"
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    //region file to lines
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }
    //endregion

    val records: MutableList<Record> = mutableListOf()
    for (i in lines.indices) {
        val str: String = lines[i].split(' ')[0]//.toList()
        val st2: List<Int> = lines[i].split(' ')[1].split(',').map{ it.toInt() }

        records.add(Record(str, st2))
    }

    var res: Int = 0

    for (rec in records) {
        res += rec.nbValid
    }

    println(res)

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    //region file to lines
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }
    //endregion

    val records: MutableList<Record5> = mutableListOf()
    for (i in lines.indices) {
        val str: String = lines[i].split(' ')[0]//.toList()
        val st2: List<Int> = lines[i].split(' ')[1].split(',').map{ it.toInt() }

        records.add(Record5(str, st2))
    }

    for (rec in records) {
        println(rec.getCodeValues())
    }


    var res: Int = 0

    for (rec in records) {
        res += rec.nbValid
    }

    println(res)

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}
