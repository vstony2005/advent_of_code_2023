# --- Day 12: Hot Springs ---

## --- Première partie ---

Vous atteignez enfin les sources d'eau chaude ! Vous pouvez voir de la vapeur s'échapper d'endroits isolés attachés au bâtiment principal et orné.

Alors que vous vous apprêtez à entrer, le [chercheur](https://adventofcode.com/2023/day/11) vous arrête. "Attendez, je croyais que vous cherchiez les sources d'eau chaude, n'est-ce pas ?" Vous indiquez qu'il s'agit bien de sources d'eau chaude.

"Oh, désolé, c'est une erreur ! Il s'agit en fait du [onsen](https://en.wikipedia.org/wiki/Onsen) ! Les sources d'eau chaude sont à côté."

Vous regardez dans la direction indiquée par le chercheur et remarquez soudain les énormes hélices métalliques qui surplombent la ville. "Par ici !"

Il ne vous faut que quelques pas de plus pour atteindre la porte principale de l'immense zone clôturée où se trouvent les sources. Vous passez la porte et entrez dans un petit bâtiment administratif.

"Bonjour ! Qu'est-ce qui vous amène aux sources d'eau chaude aujourd'hui ? Désolé, elles ne sont pas très chaudes en ce moment, nous avons une *pénurie de lave* en ce moment." Vous vous renseignez sur les pièces de machines manquantes pour l'île déserte.

"Oh, toute l'île aux pignons est actuellement hors ligne ! Nous ne fabriquons rien pour l'instant, pas avant d'avoir plus de lave pour chauffer nos forges. Et nos ressorts. Les ressorts ne sont pas très élastiques s'ils ne sont pas chauds !"

"Dites, pourriez-vous aller voir pourquoi la lave s'est arrêtée de couler ? Les sources sont trop froides pour fonctionner normalement, mais nous devrions pouvoir en trouver une assez élastique pour vous lancer là-haut !"

Il y a juste un problème : de nombreux ressorts sont tombés en ruine, si bien qu'ils ne sont pas sûrs de savoir quels ressorts seraient *sûrs* à utiliser ! Pire encore, leurs *enregistrements de l'état des ressorts endommagés* (votre entrée dans l'énigme) sont également endommagés ! Vous devrez les aider à réparer les enregistrements endommagés.

Dans le champ géant qui se trouve juste à l'extérieur, les ressorts sont disposés en *rangées*. Pour chaque rangée, les enregistrements de condition montrent chaque ressort et indiquent s'il est *opérationnel* (`.`) ou *endommagé* (`#`). Il s'agit de la partie de l'enregistrement des conditions qui est elle-même endommagée ; pour certains ressorts, il est simplement *inconnu* (`?`) si le ressort est opérationnel ou endommagé.

Cependant, l'ingénieur qui a produit les rapports d'état a également dupliqué certaines de ces informations dans un format différent ! Après la liste des ressorts pour une ligne donnée, la taille de chaque *groupe contigu de ressorts endommagés* est indiquée dans l'ordre où ces groupes apparaissent dans la ligne. Cette liste tient toujours compte de chaque ressort endommagé, et chaque chiffre correspond à la taille totale de son groupe contigu (c'est-à-dire que les groupes sont toujours séparés par au moins un ressort opérationnel) : `####` serait toujours `4`, jamais `2,2`).

Ainsi, les enregistrements de conditions sans conditions de ressorts inconnus pourraient ressembler à ceci :

```
#.#.### 1,1,3
.#...#....###. 1,1,3
.#.###.#.###### 1,3,1,6
####.#...#... 4,1,1
#....######..#####. 1,6,5
.###.##....# 3,2,1
```

Cependant, les enregistrements des conditions sont partiellement endommagés ; certaines conditions des ressorts sont en fait *inconnues* (`?`). Par exemple :

```
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
```

Muni de ces informations, il vous appartient de déterminer *combien d'arrangements différents* de ressorts opérationnels et cassés correspondent aux critères donnés dans chaque ligne.

Dans la première ligne (` ???.### 1,1,3`), il y a exactement *une* façon dont des groupes distincts d'un, un et trois ressorts cassés (dans cet ordre) peuvent apparaître dans cette ligne : les trois premiers ressorts inconnus doivent être cassés, puis opérationnels, puis cassés (`#.#`), ce qui fait de la ligne entière `#.#.###`.

La deuxième ligne est plus intéressante : `. ??.. ??...?##. 1,1,3` pourrait être un total de *quatre* arrangements différents. Le dernier `?` doit toujours être cassé (pour satisfaire le groupe contigu final de trois ressorts cassés), et chaque `??` doit cacher exactement un des deux ressorts cassés. (Aucun des `??` ne peut être un ressort cassé, sinon ils formeraient un seul groupe contigu de deux ; si c'était vrai, les nombres suivants auraient été `2,3` à la place). Puisque chaque `??` peut être soit `#.` soit `.#`, il y a quatre arrangements possibles de ressorts.

La dernière ligne est en fait compatible avec *dix* arrangements différents ! Puisque le premier chiffre est `3`, le premier et le deuxième `?` doivent tous deux être `.` (si l'un ou l'autre était `#`, le premier chiffre devrait être `.4` ou plus). Cependant, la série restante de ressorts inconnus a de nombreuses façons différentes de contenir des groupes de deux et un ressorts cassés :

```
?###???????? 3,2,1
.###.##.#...
.###.##..#..
.###.##...#.
.###.##....#
.###..##.#..
.###..##..#.
.###..##...#
.###...##.#.
.###...##..#
.###....##.#
```

Dans cet exemple, le nombre d'arrangements possibles pour chaque ligne est le suivant :

- `???.### 1,1,3` - `*1*` arrangement
- `.??..??...?##. 1,1,3` - `*4*` arrangements
- `?#?#?#?#?#?#?#? 1,3,1,6` - `*1*` arrangement
- `????.#...#... 4,1,1` - `*1*` arrangement
- `????.######..#####. 1,6,5` - `*4*` arrangements
- `?###???????? 3,2,1` - `*10*` arrangements

En additionnant tous les arrangements possibles, on obtient un total de `*21*` arrangements.

Pour chaque ligne, comptez tous les arrangements différents de ressorts opérationnels et cassés qui répondent aux critères donnés. *Quelle est la somme de ces nombres ?*

Pour commencer, [faites votre entrée dans le puzzle] (https://adventofcode.com/2023/day/12/input).

## --- Deuxième partie ---

En regardant le champ de ressorts, vous avez l'impression qu'il y a beaucoup plus de ressorts que la liste des relevés de conditions. Lorsque vous examinez les enregistrements, vous découvrez qu'ils étaient en fait *pliés* depuis le début !

Pour *déplier les enregistrements*, sur chaque ligne, remplacez la liste des conditions des ressorts par cinq copies d'elle-même (séparées par `?`) et remplacez la liste des groupes contigus de ressorts endommagés par cinq copies d'elle-même (séparées par `,`).


Ainsi, cette ligne :

```
.# 1
```

deviendrait :

```
.#?.#?.#?.#?.# 1,1,1,1,1
```

La première ligne de l'exemple ci-dessus deviendrait :

```
???.###????.###????.###????.###????.### 1,1,3,1,1,3,1,1,3,1,1,3,1,1,3
```

Dans l'exemple ci-dessus, après dépliage, le nombre d'arrangements possibles pour certaines lignes est maintenant beaucoup plus grand :

- ` ???.### 1,1,3` - arrangement `*1*`.
- `. ??.. ??...?##. 1,1,3` - `*16384*` arrangements
- `?#?#?#?#?#?#?# ? 1,3,1,6` - `*1*` arrangement
- ` ????.#...#... 4,1,1` - `*16*` arrangements
- ` ????.######..#####. 1,6,5` - `*2500*` arrangements
- `?### ???????? 3,2,1` - `*506250*` arrangements

Après dépliage, l'addition de tous les nombres d'arrangements possibles donne `*525152*`.

Déployez vos enregistrements de conditions ; *quelle est la nouvelle somme des nombres d'arrangements possibles ?
