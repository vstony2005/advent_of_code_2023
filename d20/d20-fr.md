# --- Jour 20 : Propagation des impulsions ---

## --- Première partie ---

Avec votre aide, les Elfes ont réussi à trouver les bonnes pièces et à réparer toutes les machines. Il ne leur reste plus qu'à envoyer la commande pour démarrer les machines et faire couler le sable à nouveau.

Les machines sont éloignées les unes des autres et reliées par de longs *câbles*. Les câbles ne se connectent pas directement aux machines, mais plutôt à des *modules* de communication attachés aux machines, qui effectuent diverses tâches d'initialisation et servent également de relais de communication.

Les modules communiquent par *impulsions*. Chaque impulsion est soit une *impulsion haute*, soit une *impulsion basse*. Lorsqu'un module envoie une impulsion, il envoie ce type d'impulsion à chaque module de sa liste de *modules de destination*.

Il existe plusieurs types de modules :

Les modules *flip-flop* (préfixe `%`) sont soit *on* soit *off* ; ils sont initialement *off*. Si un module flip-flop reçoit une impulsion haute, elle est ignorée et rien ne se passe. Cependant, si un module flip-flop reçoit une impulsion basse, il *bascule entre on et off*. S'il était éteint, il s'allume et envoie une impulsion haute. S'il était allumé, il s'éteint et envoie une impulsion basse.

Les modules *conjonction* (préfixe `&`) *se souviennent* du type de l'impulsion la plus récente reçue de *chacun* des modules d'entrée qui leur sont connectés ; par défaut, ils se souviennent d'une *impulsion faible* pour chaque entrée. Lorsqu'une impulsion est reçue, le module de conjonction met d'abord à jour sa mémoire pour cette entrée. Ensuite, s'il se souvient d'impulsions *hautes* pour toutes les entrées, il envoie une impulsion *basse* ; sinon, il envoie une impulsion *haute*.

Il y a un seul *module de diffusion* (nommé `broadcaster'). Lorsqu'il reçoit une impulsion, il envoie la même impulsion à tous ses modules de destination.

Ici, au siège de Desert Machine, il y a un module avec un seul bouton appelé, à juste titre, le *module bouton*. Lorsque vous appuyez sur le bouton, une seule *impulsion basse* est envoyée directement au module `broadcaster`.

Après avoir appuyé sur le bouton, vous devez attendre que toutes les impulsions aient été délivrées et traitées avant d'appuyer à nouveau sur le bouton. N'appuyez jamais sur le bouton si des modules sont encore en train de traiter des impulsions.

Les impulsions sont toujours traitées *dans l'ordre où elles sont envoyées*. Donc, si une impulsion est envoyée aux modules `a`, `b`, et `c`, et que le module `a` traite son impulsion et envoie d'autres impulsions, les impulsions envoyées aux modules `b` et `c` devront être traitées en premier.

La configuration du module (votre entrée puzzle) liste chaque module. Le nom du module est précédé d'un symbole identifiant son type, le cas échéant. Le nom est ensuite suivi d'une flèche et d'une liste de modules de destination. Voici un exemple :

```
broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a
```

Dans cette configuration de modules, le diffuseur a trois modules de destination nommés `a`, `b`, et `c`. Chacun de ces modules est un module flip-flop (comme indiqué par le préfixe `%`). `a` sort vers `b` qui sort vers `c` qui sort vers un autre module nommé `inv`. `inv` est un module de conjonction (comme indiqué par le préfixe `&`) qui, parce qu'il n'a qu'une entrée, agit comme un inverseur (il envoie l'opposé du type d'impulsion qu'il reçoit) ; il sort vers `a`.

En appuyant une fois sur le bouton, les impulsions suivantes sont envoyées :

```
button -low-> broadcaster
broadcaster -low-> a
broadcaster -low-> b
broadcaster -low-> c
a -high-> b
b -high-> c
c -high-> inv
inv -low-> a
a -low-> b
b -low-> c
c -low-> inv
inv -high-> a
```

Après cette séquence, les modules de bascule se retrouvent tous *désactivés*, de sorte qu'une nouvelle pression sur le bouton répète la même séquence.

Voici un exemple plus intéressant :

```
broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output
```

Cette configuration de modules comprend le `broadcaster`, deux bascules (nommées `a` et `b`), un module de conjonction à une entrée (`inv`), un module de conjonction à plusieurs entrées (`con`), et un module non typé nommé `output` (à des fins de test). Le module de conjonction à entrées multiples `con` surveille les deux modules flip-flop et, s'ils sont tous les deux activés, envoie une impulsion *low* au module `output`.

Voici ce qui se passe si vous appuyez une fois sur le bouton :

```
button -low-> broadcaster
broadcaster -low-> a
a -high-> inv
a -high-> con
inv -low-> b
con -high-> output
b -high-> con
con -low-> output
```

Les deux bascules sont activées et une impulsion basse est envoyée à la sortie ! Cependant, maintenant que les deux bascules sont activées et que `con` se souvient d'une impulsion haute de chacune de ses deux entrées, le fait d'appuyer sur le bouton une seconde fois a un effet différent :

```
button -low-> broadcaster
broadcaster -low-> a
a -low-> inv
a -low-> con
inv -high-> b
con -high-> output
```

La bascule `a` s'éteint ! Maintenant, `con` se souvient d'une impulsion basse du module `a`, et donc il n'envoie qu'une impulsion haute à `output`.

Appuyez sur le bouton une troisième fois :

```
button -low-> broadcaster
broadcaster -low-> a
a -high-> inv
a -high-> con
inv -low-> b       !!
con -low-> output  !!
b -low-> con
con -high-> output
```

Cette fois, la bascule `a` s'allume, puis la bascule `b` s'éteint. Cependant, avant que `b` puisse s'éteindre, l'impulsion envoyée à `con` est traitée en premier, donc elle *se souvient brièvement de toutes les impulsions hautes* pour ses entrées et envoie une impulsion basse à `output`. Ensuite, la bascule `b` s'éteint, ce qui amène `con` à mettre à jour son état et à envoyer une impulsion haute à `output`.

Enfin, avec `a` activé et `b` désactivé, appuyez sur le bouton une quatrième fois :

```
button -low-> broadcaster
broadcaster -low-> a
a -low-> inv
a -low-> con
inv -high-> b
con -high-> output
```

Le cycle s'achève : `a` s'éteint, ce qui fait que `con` ne se souvient que des impulsions basses et ramène tous les modules à leur état d'origine.

Pour réchauffer les câbles, les Elfes ont appuyé sur le bouton `1000` fois. Combien d'impulsions ont été envoyées en conséquence (y compris les impulsions envoyées par le bouton lui-même) ?

Dans le premier exemple, la même chose se produit à chaque fois que l'on appuie sur le bouton : 8 impulsions basses et 4 impulsions hautes sont envoyées. Donc, après avoir appuyé sur le bouton `1000` fois, `8000` impulsions basses et `4000` impulsions hautes sont envoyées. En les multipliant, on obtient `*32000000*`.

Dans le deuxième exemple, après avoir appuyé sur le bouton `1000` fois, `4250` impulsions basses et `2750` impulsions hautes sont envoyées. En les multipliant, on obtient `*11687500*`.

Consultez la configuration de votre module ; déterminez le nombre d'impulsions basses et d'impulsions hautes qui seraient envoyées après avoir appuyé sur le bouton `1000` fois, en attendant que toutes les impulsions soient entièrement traitées après chaque appui sur le bouton. *Qu'obtenez-vous si vous multipliez le nombre total d'impulsions basses envoyées par le nombre total d'impulsions hautes envoyées ?

Pour commencer, [faites votre entrée dans le puzzle] (https://adventofcode.com/2023/day/20/input).

## --- Deuxième partie ---

La machine finale chargée de déplacer le sable jusqu'à Island Island est équipée d'un module nommé `rx`. La machine s'allume lorsqu'une *impulsion basse unique* est envoyée à `rx`.

Réinitialiser tous les modules à leur état par défaut. En attendant que toutes les impulsions soient entièrement traitées après chaque pression sur un bouton, *quel est le plus petit nombre de pressions sur un bouton nécessaire pour envoyer une seule impulsion basse au module nommé `rx` ?

Bien qu'il n'ait pas changé, vous pouvez toujours [obtenir votre contribution au puzzle](https://adventofcode.com/2023/day/20/input).
