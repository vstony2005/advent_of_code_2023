import java.util.concurrent.TimeUnit
import kotlin.concurrent.timer
import kotlin.jvm.Throws

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun part1() {
    //readfile1("test1")
    //readfile1("test2")
    readfile1("input")
}

fun part2() {
    //readfile2("test")
    readfile2("input")
}

//region my types
operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, Direction>>


enum class Direction (val pos: PairInt) {
    UP (Pair(-1,0)),
    RIGHT (Pair(0,1)),
    DOWN (Pair(1,0)),
    LEFT (Pair(0,-1))
}
//endregion

fun divers() {
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val dico: HashMap<String, MyData> = hashMapOf()
    MyData.allObjects = dico

    for (line in lines) {
        val splitLine: Array<String> = line.split(" -> ").toTypedArray()
        val mdata: MyData = MyData(splitLine[0], splitLine[1].split(", ").toTypedArray())
        dico[mdata.name] = mdata
    }

    val emptyCodes: MutableList<MyData> = mutableListOf()
    dico.values.map { it.codes.map { code ->
        if (!dico.contains(code))
            emptyCodes.add(MyData("@$code", arrayOf()))
    } }
    emptyCodes.map { dico[it.name] = it }

    dico.values.map { data ->
        data.codes.map { code ->
            if (dico[code]!!.typeData == TypeData.CONJUNCTION)
                dico[code]!!.entries[data.name] = Impulsion.LOW
        }
    }

    //dico.map { it.println() }

    repeat(1000) {
        //"\nloop ${it+1}".println()
        dico["broadcaster"]?.receiveImpulse(null, Impulsion.LOW)
        while (MyData.actions.any()) {
            val action = MyData.actions.removeFirst()
            if (action.second == null)
                throw IllegalArgumentException("Error with $action")
            else
                action.second!!.receiveImpulse(action.first, action.third)
        }
    }

    "Low: ${MyData.nbLow} - High: ${MyData.nbHight}".println()

    var res: Int = MyData.nbLow * MyData.nbHight
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

enum class TypeData { FLIPFLOP,  CONJUNCTION, BROADCAST, NONE }
enum class Impulsion { HIGHT, LOW }
enum class StateData { ON, OFF }

class MyData(code: String, joins: Array<String>) {
    val name: String
    val typeData: TypeData
    val codes: Array<String> = joins
    val entries: HashMap<String, Impulsion> = hashMapOf()
    private var impulsion: Impulsion = Impulsion.LOW
    private var state: StateData = StateData.OFF

    init {
        name = if (code.first() in listOf('%', '&', '@'))
            code.drop(1)
        else
            code

        typeData = if (code.first() == '%')
            TypeData.FLIPFLOP
        else if (code.first() == '&')
            TypeData.CONJUNCTION
        else if (code.first() == '@')
            TypeData.NONE
        else if (code.startsWith("broadcaster"))
            TypeData.BROADCAST
        else
            throw IllegalArgumentException("code \"${code}\" incorrect")

        //if (typeData == TypeData.CONJUNCTION) {
        //    MyData.conjunctions.add(this)
        //}
    }

    fun receiveImpulse(from: MyData?, impulsionSend: Impulsion) {
        //"(receiveImpulse) ${from?.name} -${impulsionSend.toString()}-> $name".println()
        MyData.addSignal(impulsionSend)
        if (name == "rx" && impulsionSend == Impulsion.LOW)
            MyData.nbRxLow++
        when (typeData) {
            TypeData.FLIPFLOP -> {
                if (impulsionSend == Impulsion.HIGHT)
                    return

                if (state == StateData.OFF) {
                    state = StateData.ON
                    codes.map {
                        //"${typeData} send HIGHT".println()
                        actions.add(Triple(this, allObjects!![it], Impulsion.HIGHT))
                    }
                } else {
                    state = StateData.OFF
                    codes.map {
                        //"${typeData} send LOW".println()
                        actions.add(Triple(this, allObjects!![it], Impulsion.LOW))
                    }
                }
            }
            TypeData.CONJUNCTION -> {
                entries[from!!.name] = impulsionSend
                //val impuls = if (MyData.conjunctions.all { it.impulsion == Impulsion.HIGHT })
                val impuls = if (entries.keys.all { entries[it]!! == Impulsion.HIGHT })
                    Impulsion.LOW
                else
                    Impulsion.HIGHT
                codes.map {
                    //"${typeData} send $impuls".println()
                    actions.add(Triple(this, allObjects!![it], impuls))
                }

            }
            TypeData.BROADCAST ->
                codes.map {
                    //"${typeData} send $impulsionSend".println()
                    actions.add(Triple(this, allObjects!![it], impulsionSend))
                }
            TypeData.NONE -> {}
        }
    }

    companion object {
        val actions: MutableList<Triple<MyData?, MyData?, Impulsion>> = mutableListOf()
        var allObjects: HashMap<String, MyData>? = null
        var nbHight: Int = 0
        var nbLow: Int = 0
        //val conjunctions: MutableList<MyData> = mutableListOf()
        var nbRxLow: Int = 0

        fun addSignal(impulsion: Impulsion) {
            if (impulsion == Impulsion.HIGHT)
                nbHight++
            else
                nbLow++
        }

        fun initAll() {
            nbHight = 0
            nbLow = 0
        }

        fun initRx() {
            nbRxLow = 0
        }
    }

    override fun toString(): String =
        "$name / ${typeData.toString()} [${impulsion.toString()}] -> (${codes.joinToString("-")})"
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val dico: HashMap<String, MyData> = hashMapOf()
    MyData.allObjects = dico

    for (line in lines) {
        val splitLine: Array<String> = line.split(" -> ").toTypedArray()
        val mdata: MyData = MyData(splitLine[0], splitLine[1].split(", ").toTypedArray())
        dico[mdata.name] = mdata
    }

    val emptyCodes: MutableList<MyData> = mutableListOf()
    dico.values.map { it.codes.map { code ->
        if (!dico.contains(code))
            emptyCodes.add(MyData("@$code", arrayOf()))
    } }
    emptyCodes.map { dico[it.name] = it }

    dico.values.map { data ->
        data.codes.map { code ->
            if (dico[code]!!.typeData == TypeData.CONJUNCTION)
                dico[code]!!.entries[data.name] = Impulsion.LOW
        }
    }

    //dico.map { it.println() }

    var loop: Int = 0
    do {
        loop++
        //MyData.initRx()
        //"\nloop ${it+1}".println()
        dico["broadcaster"]?.receiveImpulse(null, Impulsion.LOW)
        while (MyData.actions.any()) {
            val action = MyData.actions.removeFirst()
            if (action.second == null)
                throw IllegalArgumentException("Error with $action")
            else
                action.second!!.receiveImpulse(action.first, action.third)
            if (MyData.nbRxLow > 0)
                break
        }
    } while (MyData.nbRxLow == 0)

    var res: Int = 0
    "res: ${loop}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}
