# --- Day 7: Camel Cards ---

Votre voyage tous frais payés se résume à un aller simple de cinq minutes dans un [Dirigeable](https://en.wikipedia.org/wiki/Airship). (Au moins, c'est un dirigeable *cool* !) Il vous dépose au bord d'un vaste désert et redescend sur l'île.

"Vous avez apporté les pièces ?

Vous vous retournez pour voir un Elfe complètement couvert de vêtements blancs, portant des lunettes, et chevauchant un grand [chameau](https://en.wikipedia.org/wiki/Dromedary).

"Elle vous demande à nouveau, plus fort cette fois, si vous avez apporté les pièces détachées. Vous ne savez pas quelles pièces elle cherche ; vous êtes ici pour comprendre pourquoi le sable s'est arrêté.

"Les pièces ! Pour le sable, oui ! Venez avec moi, je vais vous montrer." Elle vous fait signe de monter sur le chameau.

Après avoir parcouru un peu le sable de l'île déserte, vous apercevez ce qui ressemble à de très gros rochers couvrant la moitié de l'horizon. L'elfe explique que ces rochers se trouvent le long de la partie de l'île déserte qui se trouve juste au-dessus de l'île, ce qui rend difficile l'accès à cette dernière. Normalement, ils utilisent de grosses machines pour déplacer les rochers et filtrer le sable, mais les machines sont tombées en panne parce que l'île déserte a récemment cessé de recevoir les *pièces* dont elle a besoin pour réparer les machines.

Vous avez déjà supposé que c'est à vous de comprendre pourquoi les pièces se sont arrêtées lorsqu'elle vous demande si vous pouvez l'aider. Vous acceptez automatiquement.

Comme le voyage va durer quelques jours, elle vous propose de vous apprendre le jeu des *Cartes de caramel*. Les cartes de chameau ressemblent un peu au [poker](https://en.wikipedia.org/wiki/List_of_poker_hands), sauf qu'elles sont conçues pour être jouées plus facilement à dos de chameau.

Aux cartes de chameau, vous obtenez une liste de *mains*, et votre but est de les classer en fonction de la *force* de chaque main. Une main est constituée de *cinq cartes* étiquetées parmi `A`, `K`, `Q`, `J`, `T`, `9`, `8`, `7`, `6`, `5`, `4`, `3`, ou `2`. La force relative de chaque carte suit cet ordre, où `A` est la plus forte et `2` la plus faible.

Chaque main est exactement d'un *type*. Du plus fort au plus faible, ils sont :

- *Cinq cartes identiques*, où les cinq cartes ont la même étiquette : `AAAAA`
- Quatre cartes identiques*, quatre cartes ayant la même valeur et une carte ayant une valeur différente : `AA8AA`.
- Full house* : trois cartes ont la même valeur et les deux cartes restantes ont une valeur différente : 233332
- Brelan*, où trois cartes ont la même étiquette, et les deux cartes restantes sont différentes de toutes les autres cartes de la main : `TTT98`
- *Deux paires*, où deux cartes partagent une étiquette, deux autres cartes partagent une deuxième étiquette, et la carte restante a une troisième étiquette : `23432`
- Une paire*, où deux cartes partagent une étiquette, et les trois autres cartes ont une étiquette différente de la paire et l'une de l'autre : A23A4`
- Carte haute*, où les étiquettes de toutes les cartes sont distinctes : `23456`

Les mains sont d'abord classées en fonction de leur type ; par exemple, chaque *maison complète* est plus forte qu'un *triple*.

Si deux mains ont le même type, une deuxième règle d'ordre s'applique. Commencez par comparer la *première carte de chaque main*. Si ces cartes sont différentes, la main dont la première carte est la plus forte est considérée comme la plus forte. Si la première carte de chaque main a la même *étiquette*, passez à la *deuxième carte de chaque main*. Si elles sont différentes, la main avec la deuxième carte la plus forte gagne ; sinon, continuez avec la troisième carte de chaque main, puis la quatrième, puis la cinquième.

Ainsi, `33332` et `2AAAA` sont toutes deux des mains *quatre cartes*, mais `33332` est plus forte parce que sa première carte est plus forte. De la même façon, `77888` et `77788` sont tous deux un *maison pleine*, mais `77888` est plus fort parce que sa troisième carte est plus forte (et les deux mains ont la même première et deuxième carte).

Pour jouer aux cartes Camel, on vous donne une liste de mains et leur *enchère* correspondante (votre entrée dans le puzzle). Par exemple :

```
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
```

Cet exemple montre cinq mains ; chaque main est suivie du montant de son *enchère*. Chaque main gagne un montant égal à son enchère multipliée par son *rang*, où la main la plus faible obtient le rang 1, la deuxième main la plus faible obtient le rang 2, et ainsi de suite jusqu'à la main la plus forte. Comme il y a cinq mains dans cet exemple, la main la plus forte aura le rang 5 et son enchère sera multipliée par 5.

La première étape consiste donc à classer les mains par ordre de force :

- `32T3K` est la seule *paire* et les autres mains sont toutes d'un type plus fort, donc elle obtient le rang *1*.
- `KK677` et `KTJJT` sont toutes deux des *deux paires*. Leurs premières cartes ont la même étiquette, mais la seconde carte de `KK677` est plus forte (`K` contre `T`), donc `KTJJT` obtient le rang *2* et `KK677` obtient le rang *3*.
- `T55J5` et `QQQJA` sont tous les deux des *trois du même genre*. `QQQJA` a une première carte plus forte, donc elle prend la rangée *5* et `T55J5` prend la rangée *4*.

Maintenant, vous pouvez déterminer le gain total de cet ensemble de mains en additionnant le résultat de la multiplication de l'enchère de chaque main avec son rang (`765` * 1 + `220` * 2 + `28` * 3 + `684` * 4 + `483` * 5). Le *gain total* dans cet exemple est donc de `*6440*`.

Trouvez le rang de chaque main de votre jeu. *Quels sont les gains totaux ?*

To begin, [get your puzzle input](https://adventofcode.com/2023/day/7/input).