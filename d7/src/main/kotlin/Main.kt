import java.io.File

fun main(args: Array<String>) {
    part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1(".\\test.txt")
    readfile1(".\\input.txt")
}

fun part2() {
    readfile2(".\\test.txt")
    readfile2(".\\input.txt")
}

enum class TYPE_HAND(val weight: Int) {
    CARD(1),
    PAIR_1(2),
    PAIR_2(3),
    THREE(4),
    FULL_HOUSE(5),
    FOUR(6),
    FIVE(7)
}

class Hand(cardsHand: String, valHand: Int, withJoker: Boolean = false) {
    val cards: String = cardsHand
    val value: Int = valHand
    val order: String
    val typeHand: TYPE_HAND
    val bestHand: String

    companion object {
        fun getCorresp(ch: Char, isWithJoker: Boolean): Char {
            if (isWithJoker)
                return getCorrespV2(ch)
            return getCorrespV1(ch)
        }
        fun getCorrespV1(ch: Char): Char {
            val corresp: Map<Char, Char> = mapOf(
                'A' to 'm',
                'K' to 'l',
                'Q' to 'k',
                'J' to 'j',
                'T' to 'i',
                '9' to 'h',
                '8' to 'g',
                '7' to 'f',
                '6' to 'e',
                '5' to 'd',
                '4' to 'c',
                '3' to 'b',
                '2' to 'a'
            )
            return corresp[ch]!!
        }
        fun getCorrespV2(ch: Char): Char {
            val corresp: Map<Char, Char> = mapOf(
                'A' to 'm',
                'K' to 'l',
                'Q' to 'k',
                'T' to 'j',
                '9' to 'i',
                '8' to 'h',
                '7' to 'g',
                '6' to 'f',
                '5' to 'e',
                '4' to 'd',
                '3' to 'c',
                '2' to 'b',
                'J' to 'a'
            )
            return corresp[ch]!!
        }
    }

    init {
        //region prepare type hand
        val dic: MutableMap<Char, Int> = mutableMapOf()
        for (c in cards) {
            if (c in dic)
                dic[c] = dic[c]!! + 1
            else
                dic[c] = 1
        }

        var c5 = 0   // x 5
        var c4 = 0   // x 4
        var c3 = 0   // x 3
        var c2 = 0   // pairs
        for (v in dic.values) {
            if (v == 5) {
                c5 = 1
                break
            } else if (v == 4) {
                c4 = 1
                break
            } else if (v == 3) {
                c3 = 1
            } else if (v == 2) {
                c2 += 1
            }
        }
        //endregion

        //region joker
        if (withJoker and ('J' in cards)) {
            var best: String = ""

            var high: Char = ' '
            var level: Int = 10

            for ((k, v) in dic) {
                val highIsEmpty: Boolean = (high == ' ')
                val highSuppK: Boolean = if (highIsEmpty) false
                        else (Hand.getCorresp(k, withJoker) > Hand.getCorresp(high, withJoker))

                if (v > 2) {
                    if (k != 'J') {
                        high = k
                        level = 1
                    }
                } else if (v == 2) {
                    if (k != 'J') {
                        if (level > 2)
                            high = k
                        else if ((level == 2) and (highIsEmpty or highSuppK))
                            high = k

                        if (level > 2)
                            level = 2
                    }
                } else {
                    if (k != 'J') {
                        if ((level >= 3) and (highIsEmpty or highSuppK))
                            high = k

                        if (level > 3)
                            level = 3

                    }
                }
            }

            if (high == ' ')
                best = "AAAAA"
            else
                best = cards.replace('J', high)

            bestHand = best
        } else {
            bestHand = cards
        }
        //endregion

        //region update type hand
        if (withJoker and ('J' in cards)) {
            dic.clear()
            for (c in bestHand) {
                if (c in dic)
                    dic[c] = dic[c]!! + 1
                else
                    dic.put(c, 1)
            }

            c5 = 0   // x 5
            c4 = 0   // x 4
            c3 = 0   // x 3
            c2 = 0   // pairs
            for (v in dic.values) {
                if (v == 5) {
                    c5 = 1
                    break
                } else if (v == 4) {
                    c4 = 1
                    break
                } else if (v == 3) {
                    c3 = 1
                } else if (v == 2) {
                    c2 += 1
                }
            }
        }

        if (c5 > 0)
            typeHand = TYPE_HAND.FIVE
        else if (c4 > 0)
            typeHand = TYPE_HAND.FOUR
        else if ((c3 > 0) and (c2 > 0))
            typeHand = TYPE_HAND.FULL_HOUSE
        else if (c3 > 0)
            typeHand = TYPE_HAND.THREE
        else if (c2 > 1)
            typeHand = TYPE_HAND.PAIR_2
        else if (c2 > 0)
            typeHand = TYPE_HAND.PAIR_1
        else
            typeHand = TYPE_HAND.CARD
        //endregion

        //region order
        var ord = typeHand.weight.toString()
        for (c in cards) {
            ord += Hand.getCorresp(c, withJoker)
        }
        order = ord
        //endregion
    }

    override fun toString(): String {
        return "$cards / $bestHand ($value) [$order]"
    }
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var lines: MutableList<List<String>> = mutableListOf()
    var classmt: MutableMap<TYPE_HAND, MutableList<Hand>> = mutableMapOf(
        TYPE_HAND.CARD to mutableListOf(),   // carte haute
        TYPE_HAND.PAIR_1 to mutableListOf(), // 1 pair
        TYPE_HAND.PAIR_2 to mutableListOf(), // 2 pairs
        TYPE_HAND.THREE to mutableListOf(),
        TYPE_HAND.FULL_HOUSE to mutableListOf(), // full house
        TYPE_HAND.FOUR to mutableListOf(),
        TYPE_HAND.FIVE to mutableListOf()
    )

    f.forEachLine { line ->
        lines.add(line.split(" "))
    }

    for (line in lines) {
        val h = Hand(line[0], line[1].toInt())
        classmt[h.typeHand]!!.add(h)
    }

    val listTypes: List<TYPE_HAND> = listOf(
        TYPE_HAND.CARD,
        TYPE_HAND.PAIR_1,
        TYPE_HAND.PAIR_2,
        TYPE_HAND.THREE,
        TYPE_HAND.FULL_HOUSE,
        TYPE_HAND.FOUR,
        TYPE_HAND.FIVE)
    for (c in classmt.values) {
        c.sortBy { it.order }
    }

    var res: Int = 0
    var rang: Int = 1
    for (typ in listTypes) {
        for (hand in classmt[typ]!!) {
            res += rang * hand.value
            rang += 1
        }
    }

    println(res)
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    val lines: MutableList<List<String>> = mutableListOf()
    val classmt: MutableMap<TYPE_HAND, MutableList<Hand>> = mutableMapOf(
        TYPE_HAND.CARD to mutableListOf(),   // carte haute
        TYPE_HAND.PAIR_1 to mutableListOf(), // 1 pair
        TYPE_HAND.PAIR_2 to mutableListOf(), // 2 pairs
        TYPE_HAND.THREE to mutableListOf(),
        TYPE_HAND.FULL_HOUSE to mutableListOf(), // full house
        TYPE_HAND.FOUR to mutableListOf(),
        TYPE_HAND.FIVE to mutableListOf()
    )

    f.forEachLine { line ->
        lines.add(line.split(" "))
    }

    for (line in lines) {
        val h = Hand(line[0], line[1].toInt(), true)
        classmt[h.typeHand]!!.add(h)
    }

    val listTypes: List<TYPE_HAND> = listOf(
        TYPE_HAND.CARD,
        TYPE_HAND.PAIR_1,
        TYPE_HAND.PAIR_2,
        TYPE_HAND.THREE,
        TYPE_HAND.FULL_HOUSE,
        TYPE_HAND.FOUR,
        TYPE_HAND.FIVE)
    for (c in classmt.values) {
        c.sortBy { it.order }
    }

    var res: Int = 0
    var rang: Int = 1
    for (typ in listTypes) {
        //println(typ)
        for (hand in classmt[typ]!!) {
            //println(hand)
            res += rang * hand.value
            rang += 1
        }
    }

    println(res)
}
