
# correspondances des cartes pour tri
corresp = {
    'A': 'n',
    'K': 'm',
    'Q': 'l',
    'J': 'k',
    'T': 'j',
    '9': 'i',
    '8': 'h',
    '7': 'g',
    '6': 'f',
    '5': 'e',
    '4': 'd',
    '3': 'c',
    '2': 'b',
    'J': 'a'
}

# remplacer j par meilleure carte
def get_best_hand(h) -> str:
    if 'J' in h:
        dic = {}
        # compter cartes identiques
        for c in h:
            if c in dic:
                dic[c] += 1
            else:
                dic[c] = 1

        c5 = 0   # x 5
        c4 = 0   # x 4
        c3 = 0   # x 3
        c2 = 0   # pairs
        high_card = ''
        current_level = 10
        for k, v in dic.items():
            if v > 2:
                if k != 'J':
                    high_card = k
                    current_level = 1
            elif v == 2:
                if k != 'J':
                    if (current_level > 2):
                        high_card = k
                    elif (current_level == 2) and (high_card == '' or corresp[k] > corresp[high_card]):
                        high_card = k
                    if current_level > 2:
                        current_level = 2
            else:
                if k != 'J':
                    if (current_level >= 3) and (high_card == '' or corresp[k] > corresp[high_card]):
                        high_card = k
                    if current_level > 3:
                        current_level = 3

        if high_card == '':
            return 'AAAAA'
        return h.replace('J', high_card)
    return h

lines = []
#filename = "test.txt"
filename = "input.txt"

# lecture fichier
with open(filename, 'r') as filin:
    file = filin.readlines()
    for line in file:
        l = line.strip().split()
        l.insert(0, l[0])
        l[1] = get_best_hand(l[1])
        lines.append(l)

# +faible -> +fort
ordre = ["c","c1p","c2p","c3","cfh","c4","c5"]
classmt = {
    "c": [],   # carte haute
    "c1p": [], # 1 pair
    "c2p": [], # 2 pairs
    "c3": [],
    "cfh": [], # full house
    "c4": [],
    "c5": []
}

# tri des mains par résultat
for line in lines:
    hand = line[1]

    dic = {}
    # compter cartes identiques
    for c in hand:
        if c in dic:
            dic[c] += 1
        else:
            dic[c] = 1

    c5 = 0   # x 5
    c4 = 0   # x 4
    c3 = 0   # x 3
    c2 = 0   # pairs
    for v in dic.values():
        if v == 5:
            c5 = 1
            break
        elif v == 4:
            c4 = 1
            break
        elif v == 3:
            c3 = 1
        elif v == 2:
            c2 += 1

    if c5 > 0:
        classmt["c5"].append(line)
    elif c4 > 0:
        classmt["c4"].append(line)
    elif (c3 > 0) and (c2 > 0):
        classmt["cfh"].append(line)
    elif c3 > 0:
        classmt["c3"].append(line)
    elif c2 > 1:
        classmt["c2p"].append(line)
    elif c2 > 0:
        classmt["c1p"].append(line)
    else:
        classmt["c"].append(line)

# tri des mains
def to_sort(a_word):
    s = ''
    for c in a_word[0]:
        s += corresp[c]
    return s

# A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J
for o in ordre:
    classmt[o].sort(key = to_sort) # reverse=True

# calcul résultat
i = 1
res = 0
for o in ordre:
    for c in classmt[o]:
        res += (i * int(c[2]))
        i += 1
print(res)
