lines = []
#filename = "test.txt" # 6440
filename = "input.txt" # 250946742
with open(filename, 'r') as filin:
    file = filin.readlines()
    for line in file:
        lines.append(line.strip().split())

ordre = ["c","c1p","c2p","c3","cfh","c4","c5"]
classmt = {
    "c": [],   # carte haute
    "c1p": [], # 1 pair
    "c2p": [], # 2 pairs
    "c3": [],
    "cfh": [], # full house
    "c4": [],
    "c5": []
}

for line in lines:
    hand = line[0]

    dic = {}
    # compter cartes identiques
    for c in hand:
        if c in dic:
            dic[c] += 1
        else:
            dic[c] = 1

    c5 = 0   # x 5
    c4 = 0   # x 4
    c3 = 0   # x 3
    c2 = 0   # pairs
    for v in dic.values():
        if v == 5:
            c5 = 1
            break
        elif v == 4:
            c4 = 1
            break
        elif v == 3:
            c3 = 1
        elif v == 2:
            c2 += 1

    if c5 > 0:
        classmt["c5"].append(line)
    elif c4 > 0:
        classmt["c4"].append(line)
    elif (c3 > 0) and (c2 > 0):
        classmt["cfh"].append(line)
    elif c3 > 0:
        classmt["c3"].append(line)
    elif c2 > 1:
        classmt["c2p"].append(line)
    elif c2 > 0:
        classmt["c1p"].append(line)
    else:
        classmt["c"].append(line)

corresp = {
    'A': 'm',
    'K': 'l',
    'Q': 'k',
    'J': 'j',
    'T': 'i',
    '9': 'h',
    '8': 'g',
    '7': 'f',
    '6': 'e',
    '5': 'd',
    '4': 'c',
    '3': 'b',
    '2': 'a'
}
def to_sort(a_word):
    s = ''
    for c in a_word[0]:
        s += corresp[c]
    #print(a_word[0], s)
    return s

# A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2
for o in ordre:
    classmt[o].sort(key = to_sort) # reverse=True

#print(classmt)
i = 1
res = 0
for o in ordre:
    for c in classmt[o]:
        res += (i * int(c[1]))
        i += 1

print(res)
