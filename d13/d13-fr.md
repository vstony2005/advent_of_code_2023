# --- Jour 13 : Point d'Incidence ---

## --- Partie Un ---

Avec votre aide, l'équipe des sources d'eau chaude trouve une source appropriée qui vous lance proprement et précisément jusqu'au bord de *l'île de lave*.

Il y a juste un problème : vous ne voyez pas de *lava*.

En revanche, on voit beaucoup de cendres et de roches ignées ; il y a même ce qui ressemble à des montagnes grises éparpillées un peu partout. Au bout d'un moment, vous vous dirigez vers un groupe de montagnes tout proche et vous découvrez que la vallée qui les sépare est complètement remplie de grands *miroirs*.  La plupart des miroirs semblent alignés de manière cohérente ; peut-être devriez-vous vous diriger dans cette direction ?

Alors que vous avancez dans la vallée de miroirs, vous constatez que plusieurs d'entre eux sont tombés des grands cadres métalliques qui les maintiennent en place. Les miroirs sont extrêmement plats et brillants, et beaucoup de ceux qui sont tombés se sont logés dans la cendre à des angles étranges. Comme le terrain est d'une seule couleur, il est difficile de savoir où l'on peut marcher en toute sécurité et où l'on risque de tomber sur un miroir.

Vous notez les motifs de cendres (`.`) et de rochers (`#`) que vous voyez en marchant (votre entrée d'énigme) ; peut-être qu'en analysant attentivement ces motifs, vous pourrez trouver où se trouvent les miroirs !

En analysant attentivement ces motifs, vous pourrez peut-être trouver où se trouvent les miroirs :

```
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
```

Pour trouver le reflet dans chaque motif, vous devez trouver un reflet parfait sur une ligne horizontale entre deux rangées ou sur une ligne verticale entre deux colonnes.

Dans la première figure, la réflexion se fait sur une ligne verticale entre deux colonnes ; les flèches sur chacune des deux colonnes pointent vers la ligne entre les colonnes :

```
123456789
    ><   
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.
    ><   
123456789
```

Dans ce motif, la ligne de réflexion est la ligne verticale entre les colonnes 5 et 6. Comme la ligne verticale n'est pas parfaitement au milieu du motif, une partie du motif (la colonne 1) n'a nulle part où se refléter et peut être ignorée ; toutes les autres colonnes ont une colonne réfléchie dans le motif et doivent correspondre exactement : la colonne 2 correspond à la colonne 9, la colonne 3 correspond à la colonne 8, la colonne 4 correspond à la colonne 7, et la colonne 5 correspond à la colonne 6.

Le second motif se reflète plutôt sur une ligne horizontale :

```
1 #...##..# 1
2 #....#..# 2
3 ..##..### 3
4v#####.##.v4
5^#####.##.^5
6 ..##..### 6
7 #....#..# 7
```

Ce motif se reflète sur la ligne horizontale entre les rangs 4 et 5. La rangée 1 correspondrait à une hypothétique rangée 8, mais comme ce n'est pas dans le motif, la rangée 1 n'a pas besoin de correspondre à quoi que ce soit. Les autres lignes correspondent : la ligne 2 correspond à la ligne 7, la ligne 3 correspond à la ligne 6 et la ligne 4 correspond à la ligne 5.

Pour *résumer* vos notes sur les motifs, additionnez *le nombre de colonnes* à gauche de chaque ligne de réflexion verticale ; ajoutez-y *100 multiplié par le nombre de rangées* au-dessus de chaque ligne de réflexion horizontale. Dans l'exemple ci-dessus, la ligne verticale du premier motif a `5` colonnes à sa gauche et la ligne horizontale du second motif a `4` rangées au-dessus d'elle, soit un total de `*405*`.

Trouvez la ligne de réflexion dans chacun des motifs dans vos notes. *Quel nombre obtenez-vous après avoir résumé toutes vos notes ?*

Pour commencer, [obtenez l'entrée de votre puzzle] (https://adventofcode.com/2023/day/13/input).

## --- Deuxième partie ---

Vous reprenez votre marche dans la vallée des miroirs et - *SMACK!* - vous vous heurtez directement à l'un d'entre eux. Espérons que personne ne regardait, car cela a dû être assez embarrassant.

En y regardant de plus près, vous découvrez que chaque miroir a exactement une *bavure* : exactement un `.` ou un `#` devrait être du type opposé.

Dans chaque modèle, vous devrez localiser et réparer la tache qui permet à une *ligne de réflexion différente* d'être valide. (L'ancienne ligne de réflexion ne restera pas nécessairement valide une fois la tache réparée).

Reprenons l'exemple ci-dessus :

```
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
```

La tache du premier motif se trouve dans le coin supérieur gauche. Si le `#` en haut à gauche était à la place de `.`, il aurait une ligne de réflexion horizontale différente :

```
1 ..##..##. 1
2 ..#.##.#. 2
3v##......#v3
4^##......#^4
5 ..#.##.#. 5
6 ..##..##. 6
7 #.#.##.#. 7
```

La tache dans le coin supérieur gauche ayant été réparée, il existe désormais une nouvelle ligne de réflexion horizontale entre les rangées 3 et 4. La ligne 7 n'a pas de ligne réfléchie correspondante et peut être ignorée, mais toutes les autres lignes correspondent exactement : la ligne 1 correspond à la ligne 6, la ligne 2 correspond à la ligne 5 et la ligne 3 correspond à la ligne 4.

Dans le deuxième modèle, la tache peut être corrigée en remplaçant le cinquième symbole de la ligne 2 de `.` par `#` :

```
1v#...##..#v1
2^#...##..#^2
3 ..##..### 3
4 #####.##. 4
5 #####.##. 5
6 ..##..### 6
7 #....#..# 7
```

Le motif présente désormais une ligne de réflexion horizontale différente entre les lignes 1 et 2.

Résumez vos notes comme précédemment, mais en utilisant les nouvelles lignes de réflexion. Dans cet exemple, la nouvelle ligne horizontale du premier motif a 3 rangs au-dessus d'elle et la nouvelle ligne horizontale du second motif a 1 rang au-dessus d'elle, ce qui donne la valeur `*400*`.

Dans chaque motif, corrigez la tache et trouvez la ligne de réflexion différente. *Quel nombre obtenez-vous après avoir résumé la nouvelle ligne de réflexion de chaque motif dans vos notes ?*
