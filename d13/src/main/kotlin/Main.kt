import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    part1()
    part2()
    println("end.")
}

fun divers() {
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    readfile2("input")
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.forEach{println(it)}


    val patterns: MutableList<PatternArray> = mutableListOf()
    val current: MutableList<String> = mutableListOf()
    for (l in lines) {
        if (l.isEmpty()) {
            if (current.isNotEmpty()) {
                patterns.add(PatternArray(current))
            }
            current.clear()
            continue
        }
        current.add(l)
    }
    var res: Int = 0
    patterns.forEach {
        res += it.getValLines()
        res += it.getValCols()
    }

    println(res)

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

class PatternArray(lines: List<String>) {
    val pattern: List<List<Char>>
    val nbLines: Int
    val nbCols: Int

    init {
        pattern = listStringToListChar(lines)
        nbLines = pattern.size
        nbCols = if (nbLines == 0) 0 else pattern[0].size
    }

    private fun listStringToListChar(list: List<String>): List<List<Char>> {
        val lines: MutableList<List<Char>> = mutableListOf()
        list.forEach { if (it.isNotEmpty()) lines.add(it.toList()) }
        return lines
    }

    fun getValLines(): Int {
        for (l in 0..<pattern.size-1) {
            if (pattern[l].joinToString() == pattern[l+1].joinToString()) {
                var lin1: Int = l - 1
                var lin2: Int = l + 2
                var isOk: Boolean = true
                while (isOk && lin1 >= 0 && lin2 < nbLines) {
                    if (pattern[lin1].joinToString() != pattern[lin2].joinToString())
                        isOk = false
                    lin1--
                    lin2++
                }
                if (isOk) return (l + 1) * 100
            }
        }
        return 0
    }

    fun getValCols(): Int {
        if (nbLines == 0) return -1
        for (c in 0..<pattern[0].size-1) {
            var isEq: Boolean = false
            for (l in pattern.indices) {
                isEq = (pattern[l][c] == pattern[l][c+1])
                if (!isEq) break
            }
            if (isEq) {
                var col1: Int = c - 1
                var col2: Int = c + 2

                while (isEq && col1 >= 0 && col2 < nbCols) {
                    for (l in pattern.indices) {
                        if (pattern[l][col1] != pattern[l][col2])
                            isEq = false
                    }
                    col1--
                    col2++
                }
                if (isEq) return c + 1
            }
        }
        return 0
    }

    override fun toString(): String =
        "Lines: $nbLines / Cols: $nbCols"

    fun printLines() {
        this.println()
        pattern.forEach { it.println() }
    }
}

fun readfile2(afile: String) {
}
