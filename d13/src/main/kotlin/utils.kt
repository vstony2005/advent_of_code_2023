import java.io.File
fun readTextFile(fileName: String): List<String> {
    val f = File(".\\$fileName.txt")
    if (!f.exists()) {
        println("no file")
        return listOf()
    }
    println(fileName)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line -> lines.add(line) }
    return lines
}

fun Any?.println() = println(this)
