import java.util.concurrent.TimeUnit
import kotlin.concurrent.timer
import kotlin.jvm.Throws

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun part1() {
    "part 1".println()
    readfile1("test")
    readfile1("input22")
}

fun part2() {
    "part 2".println()
    readfile2("test")
    readfile2("input22")
}

//region my types
operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, Direction>>


enum class Direction (val pos: PairInt) {
    UP (Pair(-1,0)),
    RIGHT (Pair(0,1)),
    DOWN (Pair(1,0)),
    LEFT (Pair(0,-1))
}
//endregion

fun divers() {

    fun isInner (b1: Brick, b2: Brick) {
        "$b1 && $b2 = ${b1.inner(b2)}".println()
        "$b2 && $b1 = ${b2.inner(b1)}".println()
    }

    val b1 = Brick(Coordonate(2, 0, 0), Coordonate(5, 0, 0))
    val b2 = Brick(Coordonate(3, 0, 0), Coordonate(4, 0, 0))
    val b3 = Brick(Coordonate(1, 0, 0), Coordonate(6, 0, 0))
    val b4 = Brick(Coordonate(4, 0, 0), Coordonate(6, 0, 0))
    val b5 = Brick(Coordonate(0, 0, 0), Coordonate(1, 0, 0))
    val b6 = Brick(Coordonate(8, 0, 0), Coordonate(10, 0, 0))
    val b7 = Brick(Coordonate(2, 0, 0), Coordonate(5, 0, 0))
    val b8 = Brick(Coordonate(5, 0, 0), Coordonate(9, 0, 0))
    val b9 = Brick(Coordonate(0, 0, 0), Coordonate(2, 0, 0))

    isInner(b1, b2)
    isInner(b1, b3)
    isInner(b1, b4)
    isInner(b1, b5)
    isInner(b1, b6)
    isInner(b1, b7)
    isInner(b1, b8)
    isInner(b1, b9)

}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val snapshot: Snapshot = Snapshot()
    lines.map { line ->
        val coords: List<String> = line.split("~")
        val i1 = coords[0].split(",").map { it.toInt() }
        val i2 = coords[1].split(",").map { it.toInt() }
        snapshot.addBrick(Brick(Coordonate(i1[0],i1[1],i1[2]), Coordonate(i2[0],i2[1],i2[2])))
    }

    snapshot.lowerAllBrick()
    snapshot.updateSupports()

    var res: Int = snapshot.countCanDelete()
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

class Coordonate (x: Int, y: Int, z: Int): Comparable<Coordonate> {
    val x: Int = x
    val y: Int = y
    var z: Int = z

    override fun compareTo(other: Coordonate): Int {
        if (this.z != other.z)
            return this.z.compareTo(other.z)

        if (this.x != other.x)
            return this.x.compareTo(other.x)

        return this.y.compareTo(other.y)
    }

    fun moveZ(n: Int) {
        this.z += n
    }

    override fun toString(): String = "($x,$y,$z)"
    }

class Brick (c1: Coordonate, c2: Coordonate): Comparable<Brick> {
    val code: Char
    var c1: Coordonate
    var c2: Coordonate
    val c1Final: Coordonate
    val c2Final: Coordonate
    var isDelete: Boolean = false

    init {
        if (c1 < c2) {
            this.c1 = c1
            this.c2 = c2
        } else {
            this.c2 = c1
            this.c1 = c2
        }

        c1Final = c1
        c2Final = c2

        code = Char(65 + value)
        value++
    }

    fun maxZ(): Int = c2.z
    fun minZ(): Int = c1.z
    fun minX(): Int {
        if (this.c1.x < this.c2.x)
            return c1.x
        return c2.x
    }
    fun minY(): Int {
        if (this.c1.y < this.c2.y)
            return c1.y
        return c2.y
    }

    fun drop(n: Int) {
        c1.moveZ(-n)
        c2.moveZ(-n)

        // position min = 1
        if (c1.z < 1) {
            c2.z += 1 - c1.z
            c1.z += 1 - c1.z
        }
    }

    override fun compareTo(other: Brick): Int {
        if (this.minZ() != other.minZ())
            return this.minZ().compareTo(other.minZ())
        if (this.minX() != other.minX())
            return this.minX().compareTo(other.minX())
        return this.minY().compareTo(other.minY())
    }

    fun inner(other: Brick): Boolean {
        return ((this.c1.x <= other.c1.x && this.c2.x >= other.c1.x)
                || (this.c1.x >= other.c1.x && this.c1.x <= other.c2.x))
                && ((this.c1.y <= other.c1.y && this.c2.y >= other.c1.y)
                || (this.c1.y >= other.c1.y && this.c1.y <= other.c2.y))
    }

    override fun toString(): String = "$code: $c1Final - $c2Final"

    companion object {
        var value: Int = 0
    }
}

class Snapshot {
    /**
     * List of bricks by code
     */
    val dicoBricks: HashMap<Char, Brick> = hashMapOf()
    /**
     * List of bricks above a brick
     * */
    val support: HashMap<Char, List<Char>> = hashMapOf()

    /**
     * List of supporting bricks
     */
    val supportBy: HashMap<Char, List<Char>> = hashMapOf()

    /**
     * List of bricks
     */
    val bricks: MutableList<Brick> = mutableListOf()

    fun addBrick(brick: Brick) {
        bricks.add(brick)
        bricks.sort()

        dicoBricks[brick.code] = brick
    }

    private fun findBrickUnder(brick: Brick): Brick? {
        //return bricks.filter { it.maxZ() < brick.minZ() && brick.inner(it) }.sortedByDescending { it.maxZ() }.first()
        return bricks.filter { it?.code != brick.code && it.maxZ() <= brick.minZ() && brick.inner(it) }.maxByOrNull { it.maxZ() }
    }

    fun lowerAllBrick() {
        bricks.map { b ->
            val brick: Brick? = findBrickUnder(b)
            if (brick == null)
                b.drop(b.minZ())
            else
                b.drop(b.minZ() - brick.maxZ() - 1)
        }
    }

    fun updateSupports() {
        support.clear()
        supportBy.clear()

        bricks.map { b ->
            val s: List<Char> = bricks.filter { it.inner(b) && it.minZ() == b.maxZ() + 1 }.map { it.code }
            if (s.any())
                support[b.code] = s

            val sb: List<Char> = bricks.filter { it.inner(b) && it.maxZ() + 1 == b.minZ() }.map { it.code }
            if (sb.any())
                supportBy[b.code] = sb
        }
        //support.println("support\n")
        //supportBy.println("supportBy\n")
    }

    fun countCanDelete(): Int {
        var n: Int = 0

        bricks.map { b ->
            val bs: List<Char>? = support[b.code]
            if (bs == null) {
                n++
            } else if (bs.all { supportBy[it]!!.count() > 1 }) {
                n++
            } else {
            }
        }

        return n
    }

    fun getBestChainReaction(): Int {
        return bricks.filter { it.minZ() == 1 }.maxOf { getChainReaction(it) }
    }

    private fun getChainReaction(brick: Brick): Int {
        bricks.map { it.isDelete = false }
        val level: MutableList<Char> = mutableListOf()
        level.add(brick.code)
        var n: Int = 0

        do {
            val b: Brick = dicoBricks[level.removeFirst()]!!
            if (b.isDelete)
                continue
            val sb: List<Char>? = supportBy[b.code]
            if (sb == null || sb.all { dicoBricks[it]!!.isDelete }) {
                b.isDelete = true
                n++
                support[b.code]?.map { level.add(it) }
            }
        } while (level.isNotEmpty())

        // 1150 too low
        return n
    }

    override fun toString(): String =
        bricks.joinToString("\n")
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val snapshot: Snapshot = Snapshot()
    lines.map { line ->
        val coords: List<String> = line.split("~")
        val i1 = coords[0].split(",").map { it.toInt() }
        val i2 = coords[1].split(",").map { it.toInt() }
        snapshot.addBrick(Brick(Coordonate(i1[0],i1[1],i1[2]), Coordonate(i2[0],i2[1],i2[2])))
    }

    snapshot.lowerAllBrick()
    snapshot.updateSupports()

    var res: Int = snapshot.getBestChainReaction()
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}
