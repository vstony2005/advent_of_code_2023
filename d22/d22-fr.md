# --- Day 22: Dalles de sable ---

## --- Première partie ---

Suffisamment de sable est tombé ; il peut enfin filtrer l'eau pour l'île Snow.

Enfin, *presque*.

Le sable est tombé sous forme de gros *briques* de sable compactées, s'empilant pour former une pile impressionnante ici, près du bord de l'île. Afin d'utiliser le sable pour filtrer l'eau, certaines des briques devront être brisées, voire *désintégrées*, pour redevenir du sable qui s'écoule librement.

La pile est suffisamment haute pour que vous deviez choisir avec soin les briques à désintégrer. Si vous désintégrez la mauvaise brique, de grandes parties de la pile pourraient s'écrouler, ce qui semble assez dangereux.

Les Elfes responsables des opérations de filtrage de l'eau ont pris une *capture des briques pendant qu'elles tombaient encore* (votre entrée d'énigme) qui devrait vous permettre de déterminer quelles briques peuvent être désintégrées en toute sécurité. Par exemple :

```
1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9
```

Chaque ligne de texte de l'instantané représente la position d'une seule brique au moment où l'instantané a été pris. La position est donnée sous la forme de deux coordonnées `x,y,z` - une pour chaque extrémité de la brique - séparées par un tilde (`~`). Chaque brique est composée d'une seule ligne droite de cubes, et les Elfes ont pris soin de choisir un moment pour l'instantané où toutes les briques en chute libre sont à *des positions entières au-dessus du sol*, de sorte que l'instantané entier est aligné sur une grille de cubes tridimensionnelle.

Une ligne comme `2,2,2~2,2,2` signifie que les deux extrémités de la brique sont à la même coordonnée - en d'autres termes, que la brique est un cube unique.

Des lignes comme `0,0,10~1,0,10` ou `0,0,10~0,1,10` représentent toutes deux des briques qui sont *deux cubes* en volume, tous deux orientés horizontalement. La première brique s'étend dans la direction `x`, tandis que la seconde s'étend dans la direction `y`.

Une ligne comme `0,0,1~0,0,10` représente une *brique de dix cubes* orientée *verticalement*. L'une des extrémités de la brique est le cube situé à `0,0,1`, tandis que l'autre extrémité de la brique est située directement au-dessus, à `0,0,10`.

Le sol est à `z=0` et est parfaitement plat ; la valeur `z` la plus basse qu'une brique puisse avoir est donc `1`. Ainsi, `5,5,1~5,6,1` et `0,2,1~0,2,5` reposent tous deux sur le sol, mais `3,3,2~3,3,3` était au-dessus du sol au moment de la prise de vue.

Comme la photo a été prise alors que les briques étaient encore en train de tomber, certaines briques seront *encore en l'air* ; vous devrez commencer par déterminer où elles finiront. Les briques sont stabilisées par magie, elles ne tournent donc *jamais*, même dans des situations bizarres comme lorsqu'une longue brique horizontale n'est soutenue qu'à une extrémité. Deux briques ne peuvent pas occuper la même position, de sorte qu'une brique qui tombe s'arrêtera sur la première autre brique qu'elle rencontrera.

Voici à nouveau le même exemple, cette fois-ci en attribuant une lettre à chaque brique pour qu'elle puisse être repérée dans les diagrammes :

```
1,0,1~1,2,1   <- A
0,0,2~2,0,2   <- B
0,2,3~2,2,3   <- C
0,0,4~0,2,4   <- D
2,0,5~2,2,5   <- E
0,1,6~2,1,6   <- F
1,1,8~1,1,9   <- G
```

Au moment de la prise de vue, du côté de l'axe `x` allant de gauche à droite, ces briques sont disposées comme suit :

```
 x
012
.G. 9
.G. 8
... 7
FFF 6
..E 5 z
D.. 4
CCC 3
BBB 2
.A. 1
--- 0
```

En faisant pivoter la perspective de 90 degrés pour que l'axe `y` aille de gauche à droite, les mêmes briques sont disposées comme suit :

```
 y
012
.G. 9
.G. 8
... 7
.F. 6
EEE 5 z
DDD 4
..C 3
B.. 2
AAA 1
--- 0
```

Une fois que toutes les briques sont tombées le plus bas possible, la pile ressemble à ceci, où `?` signifie que des briques sont cachées derrière d'autres briques à cet endroit :

```
 x
012
.G. 6
.G. 5
FFF 4
D.E 3 z
??? 2
.A. 1
--- 0
```

De nouveau sur le côté :

```
 y
012
.G. 6
.G. 5
.F. 4
??? 3 z
B.C 2
AAA 1
--- 0
```

Maintenant que toutes les briques se sont tassées, il est plus facile de déterminer quelles briques soutiennent quelles autres briques :

- La brique `A` est la seule brique qui supporte les briques `B` et `C`.
- La brique `B` est l'une des deux briques qui soutiennent les briques `D` et `E`.
- La brique `C` est l'autre brique qui soutient la brique `D` et la brique `E`.
- La brique `D` supporte la brique `F`.
- La brique `E` supporte également la brique `F`.
- La brique `F` supporte la brique `G`.
- La brique `G` ne supporte aucune brique.

Votre première tâche est de déterminer *quelles briques peuvent être désintégrées en toute sécurité*. Une brique peut être désintégrée en toute sécurité si, après l'avoir enlevée, *aucune autre brique* ne tombe plus directement vers le bas. Ne désintègre aucune brique, mais détermine ce qui se passerait si, pour chaque brique, seule cette brique était désintégrée. Les briques peuvent être désintégrées même si elles sont complètement entourées d'autres briques ; vous pouvez vous faufiler entre les briques si nécessaire.

Dans cet exemple, les briques peuvent être désintégrées comme suit :

- La brique `A` ne peut pas être désintégrée en toute sécurité ; si elle était désintégrée, les briques `B` et `C` tomberaient toutes les deux.
- La brique `B` *peut* être désintégrée ; les briques situées au-dessus (`D` et `E`) seraient encore soutenues par la brique `C`.
- La brique `C` *peut* être désintégrée ; les briques au-dessus (`D` et `E`) seraient encore soutenues par la brique `B`.
- La brique `D` *peut* être désintégrée ; la brique au-dessus (`F`) serait encore soutenue par la brique `E`.
- La brique `E` *peut* être désintégrée ; la brique au-dessus (`F`) serait encore soutenue par la brique `D`.
- La brique `F` ne peut pas être désintégrée ; la brique au-dessus (`G`) tomberait.
- La brique `G` *peut* être désintégrée ; elle ne soutient aucune autre brique.

Ainsi, dans cet exemple, `*5*` briques peuvent être désintégrées en toute sécurité.

Déterminez la façon dont les blocs se stabiliseront en fonction de l'instantané. Une fois qu'ils se sont stabilisés, envisagez de désintégrer une seule brique ; *combien de briques peuvent être choisies en toute sécurité pour être désintégrées ?*

Pour commencer, [obtenez les données de votre puzzle](https://adventofcode.com/2023/day/22/input).

## --- Deuxième partie ---

Désintégrer des briques une par une ne sera pas assez rapide. Cela peut sembler dangereux, mais ce dont vous avez vraiment besoin, c'est d'une *réaction en chaîne*.

Vous devez trouver la meilleure brique à désintégrer. Pour chaque brique, déterminez combien *d'autres briques tomberaient* si cette brique était désintégrée.

En utilisant le même exemple que ci-dessus :

- La désintégration de la brique `A` entraînerait la chute de toutes les `*6*` autres briques.
- La désintégration de la brique `F` entraînerait la chute de seulement `*1*` autre brique, `G`.

La désintégration de toute autre brique ne ferait tomber *aucune autre brique*. Donc, dans cet exemple, la somme du *nombre d'autres briques qui tomberaient* à la suite de la désintégration de chaque brique est `*7*`.

Pour chaque brique, déterminez combien *d'autres briques* tomberaient si cette brique était désintégrée. *Quelle est la somme du nombre d'autres briques qui tomberaient ?
