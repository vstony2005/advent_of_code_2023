# --- Day 6: Wait For It ---

## --- Part One ---

Le ferry vous fait rapidement traverser l'île Island. Après avoir demandé autour de vous, vous découvrez qu'il y a en effet normalement un gros tas de sable quelque part par ici, mais vous ne voyez rien d'autre que beaucoup d'eau et la petite île où le ferry s'est amarré.

Alors que vous essayez de trouver une solution, vous remarquez une affiche sur un mur près de l'embarcadère du ferry. "Courses de bateaux ! Ouvertes au public ! Le grand prix est un voyage tous frais payés sur *l'île déserte* !" C'est de là que vient le sable ! Mieux encore, les courses de bateaux commencent dans quelques minutes.

Vous réussissez à vous inscrire juste à temps pour participer aux courses de bateaux. L'organisateur vous explique qu'il ne s'agit pas vraiment d'une course traditionnelle : vous disposez d'un temps déterminé pendant lequel votre bateau doit aller le plus loin possible, et vous gagnez si votre bateau va le plus loin.

Lors de votre inscription, vous recevrez une feuille de papier (votre entrée de puzzle) qui indique le *temps* autorisé pour chaque course ainsi que la meilleure *distance* jamais enregistrée dans cette course. Pour être sûr de remporter le grand prix, vous devez vous assurer que vous allez *plus loin dans chaque course* que le détenteur actuel du record.

L'organisateur vous emmène dans la zone où se déroulent les courses de bateaux. Les bateaux sont beaucoup plus petits que ce à quoi vous vous attendiez - ce sont en fait des *bateaux jouets*, chacun avec un gros bouton sur le dessus. Maintenir le bouton enfoncé *charge le bateau*, et relâcher le bouton *permet au bateau de se déplacer*. Les bateaux se déplacent plus rapidement si le bouton a été maintenu plus longtemps, mais le temps passé à maintenir le bouton compte dans le temps total de la course. Vous ne pouvez maintenir le bouton enfoncé qu'au début de la course, et les bateaux ne se déplacent pas tant que le bouton n'est pas relâché.

Par exemple :

```
Time:      7  15   30
Distance:  9  40  200
```

Ce document décrit trois courses :


- La première course dure 7 millisecondes. La distance record dans cette course est de 9 millimètres.
- La deuxième course dure 15 millisecondes. La distance record de cette course est de 40 millimètres.
- La troisième course dure 30 millisecondes. La distance record dans cette course est de 200 millimètres.


Votre petit bateau a une vitesse de départ de *zéro millimètre par milliseconde*. Pour chaque milliseconde entière que vous passez au début de la course à maintenir le bouton enfoncé, la vitesse du bateau augmente de *un millimètre par milliseconde*.


Donc, comme la première course dure 7 millisecondes, vous n'avez que quelques options :

- Ne maintenez pas du tout le bouton enfoncé (c'est-à-dire pendant *`0` millisecondes*) au début de la course. Le bateau ne bougera pas ; il aura parcouru *`0` millimètres* à la fin de la course.
- Maintenez le bouton enfoncé pendant *`1` milliseconde* au début de la course. Ensuite, le bateau se déplacera à une vitesse de `1` millimètre par milliseconde pendant 6 millisecondes, atteignant une distance totale parcourue de *`6` millimètres*.
- Maintenez le bouton enfoncé pendant *`2` millisecondes*, ce qui donne au bateau une vitesse de ``2` millimètres par milliseconde. Il aura alors 5 millisecondes pour se déplacer, atteignant une distance totale de *`10` millimètres*.
- Maintenez le bouton enfoncé pendant *`3` millisecondes*. Après les 4 millisecondes restantes, le bateau aura parcouru *`12` millimètres*.
- Maintenez le bouton enfoncé pendant *`4` millisecondes*. Après les 3 millisecondes restantes, le bateau aura parcouru *`12` millimètres*.
- Maintenez le bouton enfoncé pendant *`5` millisecondes*, ce qui permet au bateau de parcourir *`10` millimètres* au total.
- Maintenez le bouton enfoncé pendant *`6` millisecondes*, pour que le bateau parcoure un total de *`6` millimètres*.
- Maintenez le bouton enfoncé pendant *`7` millisecondes*. C'est la durée totale de la course. Vous ne lâchez jamais le bouton. Le bateau ne peut pas bouger tant que vous ne lâchez pas le bouton. Veillez à lâcher le bouton pour que le bateau puisse se déplacer. *0`` millimètres*.

Puisque le record actuel pour cette course est de `9` millimètres, il y a en fait `*4*` façons différentes de gagner : vous pouvez maintenir le bouton pendant `2`, `3`, `4`, ou `5` millisecondes au début de la course.


Dans la deuxième course, vous pouvez maintenir le bouton pendant au moins `4` millisecondes et au plus `11` millisecondes et battre le record, soit un total de `*8*` façons différentes de gagner.


Dans la troisième course, vous pouviez maintenir le bouton pendant au moins `11` millisecondes et pas plus de `19` millisecondes tout en battant le record, soit un total de `*9*` façons de gagner.


Pour connaître votre marge d'erreur, déterminez le *nombre de façons de battre le record* dans chaque course ; dans cet exemple, si vous multipliez ces valeurs, vous obtenez `*288*` (`4` * `8` * `9`).


Déterminez le nombre de façons dont vous pourriez battre le record dans chaque course. *Qu'obtenez-vous si vous multipliez ces nombres ensemble ?*

To begin, [get your puzzle input](https://adventofcode.com/2023/day/6/input).

## --- Part Two ---

Alors que la course est sur le point de commencer, vous réalisez que le morceau de papier avec les temps de course et les distances enregistrées que vous avez reçu plus tôt a en fait un très mauvais crénage. Il n'y a en réalité *qu'une seule course* - ignorez les espaces entre les chiffres de chaque ligne.

Donc, l'exemple précédent :

```
Time:      7  15   30
Distance:  9  40  200
```

...signifie maintenant ceci :

```
Time:      71530
Distance:  940200
```

Maintenant, vous devez déterminer combien de façons il y a de gagner cette course. Dans cet exemple, la course dure 71530 millisecondes et la distance record à battre est de *`940200` millimètres*. Vous pouvez maintenir le bouton entre 14 et 71516 millisecondes et battre le record, soit un total de `71503` façons !

*Combien de fois pouvez-vous battre le record dans cette course beaucoup plus longue ?*