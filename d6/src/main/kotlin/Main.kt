
fun main(args: Array<String>) {
    //part1()
    part2()
    println("end.")
}

fun part1() {
    //val times: List<Int> = listOf(7,15,30)
    //val dists: List<Int> = listOf(9,40,200)
    val times: List<Int> = listOf(61,67,75,71)
    val dists: List<Int> = listOf(430,1036,1307,1150)

    var res: Long = 1

    for (i in times.indices) {
        var r: Int = 0

        for (j in 1..<times[i]-1) {
            //println("(${times[t]} - $d) * $d > ${dists[d]}")
            if (((times[i] - j) * j) > dists[i]) {
                r += 1
            }
        }
        res *= r
    }

    println(res)
}

fun part2() {
    //val time: Long = 71530
    //val dist: Long = 940200
    val time: Long = 61677571
    val dist: Long = 430103613071150

    var res: Long = 0

    for (j in 1..<time-1) {
        if (((time - j) * j) > dist) {
            res += 1
        }
    }

    println(res)
}
