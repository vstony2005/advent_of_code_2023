# --- Jour 18 : Lagon de Lavaduct ---

## --- Première partie ---

Grâce à vos efforts, l'usine de pièces détachées est l'une des premières usines à fonctionner depuis le retour de la lavafall. Cependant, pour rattraper le grand nombre de demandes de pièces, l'usine aura également besoin d'une *grande réserve de lave* pendant un certain temps ; les Elfes ont déjà commencé à créer une grande lagune à proximité dans ce but.

Cependant, ils ne sont pas sûrs que la lagune sera assez grande ; ils vous ont demandé de jeter un coup d'œil au *plan de fouille* (votre contribution à l'énigme). Par exemple :

```
R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)
```

Le creuseur commence dans un trou de 1 mètre de côté dans le sol. Il creuse ensuite le nombre de mètres spécifié *en haut* (`U`), *en bas* (`D`), *à gauche* (`L`), ou *à droite* (`R`), en dégageant des cubes entiers de 1 mètre de côté au fur et à mesure. Les directions sont données vues d'en haut, donc si "haut" était le nord, alors "droite" serait l'est, et ainsi de suite. Chaque tranchée est également listée avec *la couleur que le bord de la tranchée doit être peint* sous la forme d'un [code couleur hexadécimal RVB](https://en.wikipedia.org/wiki/RGB_color_model#Numeric_representations).

Vu d'en haut, l'exemple de plan de fouille ci-dessus donnerait la boucle de *tranchée* (`#`) suivante, creusée à partir d'un *terrain au niveau du sol* (`.`) :

```
#######
#.....#
###...#
..#...#
..#...#
###.###
#...#..
##..###
.#....#
.######
```

À ce stade, la tranchée pourrait contenir 38 mètres cubes de lave.  Cependant, il ne s'agit que du bord de la lagune ; la prochaine étape consistera à *creuser l'intérieur* de manière à ce qu'il ait également un mètre de profondeur :

```
#######
#######
#######
..#####
..#####
#######
#####..
#######
.######
.######
```

Maintenant, le lagon peut contenir une quantité beaucoup plus respectable de `*62*` mètres cubes de lave. Pendant que l'intérieur est creusé, les bords sont également peints selon les codes de couleur du plan de creusement.

Les elfes craignent que le lagon ne soit pas assez grand ; s'ils suivent leur plan de creusement, *combien de mètres cubes de lave pourrait-il contenir ?*

Pour commencer, [obtenez les données de votre puzzle] (https://adventofcode.com/2023/day/18/input).

## --- Deuxième partie

Les Elfes avaient raison de s'inquiéter : le lagon prévu serait *beaucoup trop petit*.

Après quelques minutes, quelqu'un se rend compte de ce qui s'est passé : quelqu'un a interverti les paramètres de couleur et d'instruction lors de la production du plan de fouille. Ils n'ont pas le temps de corriger le bug ; l'un d'eux demande si vous pouvez *extraire les instructions* correctes des codes hexadécimaux.

Chaque code hexadécimal est composé de six chiffres hexadécimaux. Les cinq premiers chiffres hexadécimaux codent la distance en mètres sous la forme d'un nombre hexadécimal à cinq chiffres. Le dernier chiffre hexadécimal indique la *direction à suivre pour creuser* : `0` signifie `R`, `1` signifie `D`, `2` signifie `L` et `3` signifie `U`.

Ainsi, dans l'exemple ci-dessus, les codes hexadécimaux peuvent être convertis en véritables instructions :

- `#70c710` = `R 461937`
- `#0dc571` = `D 56407`
- `#5713f0` = `R 356671`
- `#d2c081` = `D 863240`
- `#59c680` = `R 367720`
- `#411b91` = `D 266681`
- `#8ceee2` = `L 577262`
- `#caa173` = `U 829975`
- `#1b58a2` = `L 112010`
- `#caa171` = `D 829975`
- `#7807d2` = `L 491645`
- `#a77fa3` = `U 686074`
- `#015232` = `L 5411`
- `#7a21e3` = `U 500254`

En creusant cette boucle et son intérieur, on obtient un lagon qui peut contenir une quantité impressionnante de lave : *952408144115* mètres cubes.


Convertissez les codes de couleur hexadécimaux en instructions correctes ; si les Elfes suivent ce nouveau plan de creusement, *combien de mètres cubes de lave la lagune peut-elle contenir ?*