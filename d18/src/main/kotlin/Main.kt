import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    //readfile2("input")
}


operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, DIRECTIONS>>


enum class DIRECTIONS { UP, RIGHT, DOWN, LEFT }
val positions: HashMap<DIRECTIONS, PairInt> = hashMapOf(
    DIRECTIONS.UP to Pair(-1,0),
    DIRECTIONS.RIGHT to Pair(0,1),
    DIRECTIONS.DOWN to Pair(1,0),
    DIRECTIONS.LEFT to Pair(0,-1)
)

fun divers() {
    val values: List<String> = listOf("70c710","0dc571","5713f0","d2c081","59c680","411b91","8ceee2")
    for (v in values) {
        v.println()
        v.last().println("char: ")
        v.dropLast(1).println()
        v.dropLast(1).toInt(16).println()
        println()
    }
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val infos: MutableList<Triple<Char,Int,String>> = getInfos(lines)
    val plan: MutListChars = getPlan(infos)
    cleanPlan(plan)

    var res: Int = 0
    plan.map { l -> l.map { c -> if (c != ' ') res += 1 } }

    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun cleanPlan(plan: MutListChars) {
    do {
        var isChange: Boolean = false
        for (lin in plan.indices) {
            for (col in plan[lin].indices) {
                if (plan[lin][col] == '.') {
                    if ((lin == 0) || (lin == plan.size - 1)) {
                        // first/last line
                        plan[lin][col] = ' '
                        isChange = true
                    } else if (plan[lin - 1][col] == ' ') {
                        // line up empty
                        plan[lin][col] = ' '
                        isChange = true
                    } else if (plan[lin + 1][col] == ' ') {
                        // line down empty
                        plan[lin][col] = ' '
                        isChange = true
                    } else if ((col == 0) || (col == plan[lin].size - 1)) {
                        // first/last col
                        plan[lin][col] = ' '
                        isChange = true
                    } else if (plan[lin][col - 1] == ' ') {
                        // col left empty
                        plan[lin][col] = ' '
                        isChange = true
                    } else if (plan[lin][col + 1] == ' ') {
                        // col right empty
                        plan[lin][col] = ' '
                        isChange = true
                    }
                }
            }
        }
    } while (isChange)
}

fun getPlan(infos: List<Triple<Char, Int, String>>): MutListChars {
    var nbLines: Int = 0
    var nbCols: Int = 0

    // count lines cols
    //region count values
    val lines: PairInt = Pair(0,0)
    val cols: PairInt = Pair(0,0)
    var pos: PairInt = Pair(0,0)
    var start: PairInt = Pair(0,0)
    for (info in infos) {
        val n: Int = info.second
        when (info.first) {
            'L' -> {
                if (pos.second - n < 0) {
                    val nPos: Int = n - pos.second
                    nbCols += nPos
                    pos = Pair(pos.first, 0)
                    start = Pair(start.first,start.second + nPos)
                } else {
                    pos = Pair(pos.first, pos.second - n)
                }
            }
            'R' -> {
                if (pos.second + n > nbCols)
                    nbCols = pos.second + n
                pos = Pair(pos.first, pos.second + n)
            }
            'U' -> {
                if (pos.first - n < 0) {
                    val nPos: Int = n - pos.first
                    nbLines += nPos
                    pos = Pair(0,pos.second)
                    start = Pair(start.first + nPos,start.second)
                } else {
                    pos = Pair(pos.first - n, pos.second)
                }
            }
            'D' -> {
                if (pos.first + n > nbLines)
                    nbLines = pos.first + n
                pos = Pair(pos.first + n, pos.second)
            }
        }
    }
    //endregion

    //region set plan
    val plan: MutListChars = MutableList(nbLines+2) { MutableList(nbCols+1) { '.' } }

    pos = start
    plan[pos.first][pos.second] = '#'
    for (info in infos) {
        val n: Int = info.second
        when (info.first) {
            'U' -> {
                for (i in 1..n)
                    plan[pos.first - i][pos.second] = '#'
                pos = Pair(pos.first - n, pos.second)
            }

            'R' -> {
                for (i in 1..n)
                    plan[pos.first][pos.second + i] = '#'
                pos = Pair(pos.first, pos.second + n)
            }

            'D' -> {
                for (i in 1..n)
                    plan[pos.first + i][pos.second] = '#'
                pos = Pair(pos.first + n, pos.second)
            }

            'L' -> {
                for (i in 1..n)
                    plan[pos.first][pos.second - i] = '#'
                pos = Pair(pos.first, pos.second - n)
            }

            else -> continue
        }
    }
    //endregion

    return plan
}

fun getInfos(lines: List<String>): MutableList<Triple<Char, Int, String>> {
    val infos: MutableList<Triple<Char,Int,String>> = mutableListOf()
    val reg: Regex = Regex("([A-Z]) ([0-9]+) \\(#([0-9a-z]+)\\)")
    lines.map { line->
        val match: MatchResult? = reg.find(line)
        if (match != null) {
            infos.add(
                Triple(
                    match.groups[1]!!.value[0],
                    match.groups[2]!!.value.toInt(),
                    match.groups[3]!!.value))
        }
    }
    return infos
}

fun getInfosPart2(lines: List<String>): MutableList<Pair<Char,Int>> {
    val infos: MutableList<Pair<Char,Int>> = mutableListOf()
    val reg: Regex = Regex("\\(#([0-9a-z]{6})\\)")
    val dirs: HashMap<Char,Char> = hashMapOf(
        '0' to 'R',
        '1' to 'D',
        '2' to 'L',
        '3' to 'U',
    )
    lines.map { line->
        val match: MatchResult? = reg.find(line)
        if (match != null) {
            val str: String = match.groups[1]!!.value
            infos.add(
                Pair(dirs[str.last()]!!, str.dropLast(1).toInt(16)))
        } else {
            "error: $line".println()
        }
    }
    return infos
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val infos: MutableList<Pair<Char,Int>> = getInfosPart2(lines)
    infos.println()
    //val plan: MutableList<MutableList<Char>> = getPlan(infosPart2)
    //cleanPlan(plan)
    //plan.map { lin -> lin.joinToString("").println() }

    var res: Int = 0
    //plan.map { l -> l.map { c -> if (c != ' ') res += 1 } }

    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}
