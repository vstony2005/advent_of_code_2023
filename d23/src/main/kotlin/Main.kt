import java.util.concurrent.TimeUnit
import kotlin.concurrent.timer
import kotlin.jvm.Throws

fun main(args: Array<String>) {
    //divers()
    part1()
    //part2()
    println("end.")
}

fun part1() {
    "part 1".println()
    readfile1("test")
    //readfile1("input23")
}

fun part2() {
    "part 1".println()
    readfile2("test")
    //readfile2("input23")
}

//region my types
typealias PairInt = Pair<Int,Int>

operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, Direction>>

/**
 * 4-way direction
 *
 * @param pos PairInt
 */
enum class Direction (val pos: PairInt) {
    // Pair (line, col)
    UP (Pair(-1,0)),
    RIGHT (Pair(0,1)),
    DOWN (Pair(1,0)),
    LEFT (Pair(0,-1));

    fun turnLeft(): Direction =
        when (this) {
            UP -> LEFT
            RIGHT -> UP
            DOWN -> RIGHT
            LEFT -> DOWN
        }

    fun turnRight(): Direction =
        when (this) {
            UP -> RIGHT
            RIGHT -> DOWN
            DOWN -> LEFT
            LEFT -> UP
        }

    fun opposite(): Direction =
        when (this) {
            UP -> DOWN
            RIGHT -> LEFT
            DOWN -> UP
            LEFT -> RIGHT
        }
}
//endregion

fun divers() {
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    var res: Int = 0
    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun readfile2(afile: String) {
}
