# --- Jour 23 : Une longue marche ---

## --- Première partie ---

Les Elfes reprennent les opérations de filtrage de l'eau ! De l'eau propre commence à couler par-dessus le bord de l'île.

Ils vous proposent de vous aider à franchir le bord de l'île ! Il vous suffit de vous accrocher à l'une des extrémités de cette corde incroyablement longue pour qu'ils vous fassent descendre à bonne distance de l'énorme chute d'eau que vous venez de créer.

Lorsque vous atteignez enfin l'île de la Neige, vous constatez que l'eau n'atteint pas vraiment le sol : elle est *absorbée par l'air* lui-même. Il semble que vous allez enfin avoir un peu de temps libre, le temps que l'humidité atteigne un niveau suffisant pour produire de la neige. L'île Snow est assez pittoresque, même sans neige ; pourquoi ne pas y faire une promenade ?

Il y a une carte des sentiers de randonnée à proximité (votre entrée dans l'énigme) qui indique les *sentiers* (`.`), les *forêts* (`#`) et les *pentes* abruptes (`^`, `>`, `v`, et `<`).

En voici un exemple :

```
#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#
```

Vous êtes actuellement sur l'unique tuile chemin de la rangée du haut ; votre but est d'atteindre l'unique tuile chemin de la rangée du bas. En raison de la brume causée par la chute d'eau, les pentes sont probablement *glacées* ; si vous posez le pied sur une tuile de pente, votre prochain pas doit être *en descente* (dans la direction indiquée par la flèche). Pour être sûr de faire la randonnée la plus pittoresque possible, *ne marchez jamais deux fois sur la même tuile*. Quelle est la plus longue randonnée possible ?

Dans l'exemple ci-dessus, la randonnée la plus longue que vous puissiez faire est indiquée par "O", et votre position de départ est indiquée par "S" :

```
#S#####################
#OOOOOOO#########...###
#######O#########.#.###
###OOOOO#OOO>.###.#.###
###O#####O#O#.###.#.###
###OOOOO#O#O#.....#...#
###v###O#O#O#########.#
###...#O#O#OOOOOOO#...#
#####.#O#O#######O#.###
#.....#O#O#OOOOOOO#...#
#.#####O#O#O#########v#
#.#...#OOO#OOO###OOOOO#
#.#.#v#######O###O###O#
#...#.>.#...>OOO#O###O#
#####v#.#.###v#O#O###O#
#.....#...#...#O#O#OOO#
#.#########.###O#O#O###
#...###...#...#OOO#O###
###.###.#.###v#####O###
#...#...#.#.>.>.#.>O###
#.###.###.#.###.#.#O###
#.....###...###...#OOO#
#####################O#
```

Cette randonnée contient `*94*` étapes. (Les autres randonnées possibles étaient de 90, 86, 82, 82 et 74 pas).

Trouvez la randonnée la plus longue que vous puissiez faire en empruntant les sentiers de randonnée indiqués sur votre carte. *Combien de pas fait la randonnée la plus longue ?

Pour commencer, [obtenez les données de votre puzzle] (https://adventofcode.com/2023/day/23/input).
