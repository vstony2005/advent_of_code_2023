
*début de la plage de destination*
*début de la plage de source*
*longueur de la plage*

seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4


L'almanach commence par énumérer les graines à planter : les graines `79`, `14`, `55` et `13`.

Le reste de l'almanach contient une liste de *cartes* qui décrivent comment convertir les nombres d'une *catégorie source* en nombres d'une *catégorie destination*. En d'autres termes, la section qui commence par "carte graines-sol" décrit comment convertir un *nombre de graines* (la source) en un *nombre de sol* (la destination). Cela permet au jardinier et à son équipe de savoir quel sol utiliser avec quelles graines, quelle eau utiliser avec quel engrais, etc.

Plutôt que d'énumérer chaque numéro source et son numéro de destination correspondant un par un, les cartes décrivent des *plages* entières de numéros qui peuvent être converties. Chaque ligne d'une carte contient trois nombres : le *début de la plage de destination*, le *début de la plage de source* et la *longueur de la plage*.

Reprenons l'exemple de la carte "semences-sol" :

50 98 2
52 50 48

La première ligne a un *début de plage de destination* de `50`, un *début de plage de source* de `98`, et une *longueur de plage* de `2`. Cette ligne signifie que l'intervalle source commence à `98` et contient deux valeurs : `98` et `99`. L'intervalle de destination est de la même longueur, mais il commence à `50`, donc ses deux valeurs sont `50` et `51`. Avec cette information, vous savez que la graine numéro `98` correspond au sol numéro `50` et que la graine numéro `99` correspond au sol numéro `51`.

La deuxième ligne signifie que l'intervalle source commence à `50` et contient `48` valeurs : `50`, `51`, ..., `96`, `97`. Ceci correspond à une plage de destination commençant à `52` et contenant également des valeurs `48` : `52`, `53`, ..., `98`, `99`. Ainsi, le numéro de graine `53` correspond au numéro de sol `55`.

Tous les numéros de source qui *ne sont pas mappés* correspondent au *même* numéro de destination. Ainsi, le numéro de graine `10` correspond au numéro de sol `10`.

Ainsi, la liste complète des numéros de graine et des numéros de sol correspondants ressemble à ceci :


seed  soil
0     0
1     1
...   ...
48    48
49    49
50    52
51    53
...   ...
96    98
97    99
98    50
99    51

Grâce à cette carte, vous pouvez déterminer le numéro de sol nécessaire pour chaque numéro de semence initial :

- Le numéro de semence `79` correspond au numéro de sol `81`.
- Le numéro de semence `14` correspond au numéro de sol `14`.
- Le numéro de graine `55` correspond au numéro de sol `57`.
- La graine numéro `13` correspond au sol numéro `13`.

Le jardinier et son équipe veulent commencer le plus tôt possible, ils aimeraient donc connaître l'endroit le plus proche qui a besoin d'une graine. À l'aide de ces cartes, trouvez *le numéro de l'emplacement le plus bas qui correspond à l'une des graines initiales*. Pour ce faire, vous devrez convertir chaque numéro de graine dans d'autres catégories jusqu'à ce que vous trouviez le *numéro d'emplacement* correspondant. Dans cet exemple, les types correspondants sont :

- Graine `79`, sol `81`, engrais `81`, eau `81`, lumière `74`, température `78`, humidité `78`, *lieu `82`*.
- Semence `14`, sol `14`, engrais `53`, eau `49`, lumière `42`, température `42`, humidité `43`, *lieu `43`*.
- Semence `55`, sol `57`, engrais `57`, eau `53`, lumière `46`, température `82`, humidité `82`, *lieu `86`*.
- Semence `13`, sol `13`, engrais `52`, eau `41`, lumière `34`, température `34`, humidité `35`, *lieu `35`*.

Dans cet exemple, le numéro d'emplacement le plus bas est donc `*35*`.

*Quel est le plus petit numéro d'emplacement correspondant à l'un des numéros de semences initiaux ?
