import java.io.File

fun main(args: Array<String>) {
    //part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1(".\\test1.txt")
    //readfile1(".\\input1.txt")
}

fun part2() {
    readfile2(".\\test1.txt")
    readfile2(".\\input1.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Long = -1

    var seeds: MutableList<Long> = mutableListOf()
    var srcs: MutableList<LongRange> = mutableListOf()
    var dests: MutableList<LongRange> = mutableListOf()

    f.forEachLine { line ->
        if (line.startsWith("seeds:")) {
            val reg = Regex("[0-9]+")
            seeds = reg.findAll(line).map { it.value.toLong() }.toMutableList()
        }

        if (line.isEmpty()) {
            if (srcs.isNotEmpty()) {
                for (i in seeds.indices) {
                    var pos: Long = -1
                    for (j in srcs.indices) {
                        if (seeds[i] in srcs[j]) {
                            pos = seeds[i] - srcs[j].first + dests[j].first
                        }
                    }
                    if (pos > 0) seeds[i] = pos
                }
                srcs.clear()
                dests.clear()
            }
        } else if (line.isNotEmpty() && line[0] in '0'..'9') {
            val reg = Regex("[0-9]+")
            val l1: List<Long> = reg.findAll(line).map { it.value.toLong() }.toList()

            var d: Long = l1[0]
            var s: Long = l1[1]
            var l: Long = l1[2]

            srcs.add(s..<s + l)
            dests.add(d..<d + l)
        }
    }

    for (i in seeds) {
        if (res < 0) res = i
        else if (i < res) res = i
    }
    println(res)
}

fun readfile1_v1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Long = -1

    var seeds: MutableList<Long> = mutableListOf()
    var srcs: MutableList<Long> = mutableListOf()
    var dests: MutableList<Long> = mutableListOf()

    f.forEachLine { line ->
        if (line.startsWith("seeds:")) {
            val reg = Regex("[0-9]+")
            seeds = reg.findAll(line).map { it.value.toLong() }.toMutableList()
        }

        if (line.isEmpty()) {
            if (srcs.isNotEmpty()) {
                for (i in seeds.indices) {
                    val pos: Int = srcs.indexOf(seeds[i])
                    var r: Long
                    if (pos < 0) r = seeds[i]
                    else r = dests[pos]

                    seeds[i] = r
                }
                srcs.clear()
                dests.clear()
            }
        } else if (line.isNotEmpty() && line[0] in '0'..'9') {
            val reg = Regex("[0-9]+")
            val l1: List<Long> = reg.findAll(line).map { it.value.toLong() }.toList()

            var d: Long = l1[0]
            var s: Long = l1[1]
            var l: Long = l1[2]

            for (i in s..<s + l)
                srcs.add(i)
            for (i in d..<d + l)
                dests.add(i)
        }
    }

    for (i in seeds) {
        if (res < 0) res = i
        else if (i < res) res = i
    }
    println(res)
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Long = -1

    var lines: MutableList<String> = mutableListOf()
    var seeds: MutableList<LongRange> = mutableListOf()

    var srcs: MutableList<LongRange> = mutableListOf()
    var dests: MutableList<LongRange> = mutableListOf()

    f.forEachLine { lines.add(it) }

    val reg = Regex("[0-9]+")
    var head: MutableList<Long> = reg.findAll(lines[0]).map { it.value.toLong() }.toMutableList()
    var i1: Int = 0
    while (i1 < head.size) {
        seeds.add(head[i1]..<head[i1]+head[i1+1])
        i1 += 2
    }
    lines.removeAt(0)
    lines.removeAt(0)

    val dicSrc: MutableMap<Int, List<LongRange>> = mutableMapOf()
    val dicDst: MutableMap<Int, List<LongRange>> = mutableMapOf()
    var level: Int = 0
    for (line in lines) {
        if (line.isEmpty()) {
            if (srcs.isNotEmpty()) {
                dicSrc.put(level, srcs.toMutableList())
                dicDst.put(level, dests.toMutableList())
                level += 1
                srcs.clear()
                dests.clear()
            }
        } else if (line[0] in '0'..'9') {
            val l1: List<Long> = reg.findAll(line).map { it.value.toLong() }.toList()

            var d: Long = l1[0]
            var s: Long = l1[1]
            var l: Long = l1[2]

            srcs.add(s..<s + l)
            dests.add(d..<d + l)
        }
    }

    println(dicSrc)
    println(dicDst)

    for (rang in seeds) {
        println(rang)
        for (seed in rang) {
            var v = seed

            for (lvl in dicSrc.keys) {
                val dic: List<LongRange> = dicSrc[lvl]!!
                for (r in dic.indices) {
                    if (v in dic[r]) {
                        v = v - (dic[r].first ?: 0) + (dicDst[lvl]?.get(r)?.first ?: 0)
                        break
                    }
                }
            }

            if (res < 0) res = v
            else if (v < res) res = v
        }
    }

    println(res)
}

fun readfile2_v1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Long = -1

    var lines: MutableList<String> = mutableListOf()
    var seeds: MutableList<LongRange> = mutableListOf()

    var srcs: MutableList<LongRange> = mutableListOf()
    var dests: MutableList<LongRange> = mutableListOf()

    f.forEachLine { lines.add(it) }

    val reg = Regex("[0-9]+")
    var head: MutableList<Long> = reg.findAll(lines[0]).map { it.value.toLong() }.toMutableList()
    var i1: Int = 0
    while (i1 < head.size) {
        seeds.add(head[i1]..<head[i1]+head[i1+1])
        i1 += 2
    }
    lines.removeAt(0)
    lines.removeAt(0)

    for (rang in seeds) {
        println(rang)
        for (seed in rang) {
            var v = seed

            for (line in lines) {
                if (line.isEmpty()) {
                    if (srcs.isNotEmpty()) {
                        var v2: Long = -1
                        for (j in srcs.indices) {
                            if (v in srcs[j]) {
                                v2 = v - srcs[j].first + dests[j].first
                                break
                            }
                        }
                        if (v2 >= 0) v = v2

                        srcs.clear()
                        dests.clear()
                    }
                } else if (line[0] in '0'..'9') {
                    val l1: List<Long> = reg.findAll(line).map { it.value.toLong() }.toList()

                    var d: Long = l1[0]
                    var s: Long = l1[1]
                    var l: Long = l1[2]

                    srcs.add(s..<s + l)
                    dests.add(d..<d + l)
                }
            }

            if (res < 0) res = v
            else if (v < res) res = v
        }
    }

    println(res)
}

