import java.io.File

fun main(args: Array<String>) {
    part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1(".\\test1-1.txt")
    readfile1(".\\test1-2.txt")
    readfile1(".\\input.txt")
}

fun part2() {
    readfile2(".\\test2-1.txt")
    readfile2(".\\test2-2.txt")
    readfile2(".\\test2-3.txt")
    readfile2(".\\test2-4.txt")
    readfile2(".\\input.txt")
}

class Position (aLin: Int, aCol: Int) {
    var lin: Int = aLin
    var col: Int = aCol

    fun clone(): Position {
        return Position(lin, col)
    }

    fun move(p: Position) {
        lin = p.lin
        col = p.col
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        return if (other !is Position) false
        else (other.lin == this.lin) and (other.col == this.col)
    }

    override fun toString(): String {
        return "line: $lin, column: $col"
    }

    override fun hashCode(): Int {
        return 28 * lin * col
    }
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }

    val grid = Array(lines.size) { CharArray(lines[0].length) }
    var current: Position = Position(0, 0)
    for (i in lines.indices) {
        for (j in 0..<lines[i].length) {
            grid[i][j] = lines[i][j]
            if (lines[i][j] == 'S') {
                current.lin = i
                current.col = j
            }
        }
    }

    var res: Int = 0

    val distances = Array(grid.size) { IntArray(grid[0].size) { 0 } }
    startWalkV1(current, grid, distances)

    for (l in distances) {
        for (c in l)
            if (c > res) res = c
    }

    println(res)
}

fun startWalkV1(from: Position, grid: Array<CharArray>, distances: Array<IntArray>) {
    val start: Position = from.clone()
    // up
    if (from.lin > 0) {
        val new: Position = Position(from.lin-1, from.col)
        if (grid[new.lin][new.col] in listOf('|', '7', 'F')) {
            distances[new.lin][new.col] = 1
            walkV1(start, new, grid, distances, 1)
        }
    }
    // right
    if (from.col < grid[from.lin].size-1) {
        val new: Position = Position(from.lin, from.col+1)
        if (grid[new.lin][new.col] in listOf('-', '7', 'J')) {
            distances[new.lin][new.col] = 1
            walkV1(start, new, grid, distances, 1)
        }
    }
    // down
    if (from.lin < grid.size-1) {
        val new: Position = Position(from.lin+1, from.col)
        if (grid[new.lin][new.col] in listOf('|', 'L', 'J')) {
            distances[new.lin][new.col] = 1
            walkV1(start, new, grid, distances, 1)
        }
    }
    // left
    if (from.col > 0) {
        val new: Position = Position(from.lin, from.col-1)
        if (grid[new.lin][new.col] in listOf('-', 'L', 'F')) {
            distances[new.lin][new.col] = 1
            walkV1(start, new, grid, distances, 1)
        }
    }
}

fun walkV1(start: Position, from: Position, grid: Array<CharArray>, distances: Array<IntArray>, distance: Int) {
    val pos: Position = from.clone()
    val last: Position = start.clone()
    var currDist: Int = distance
    while (pos != start) {
        var isMove: Boolean = false
        if ((pos.lin > 0)
            and (last.lin != pos.lin-1)
            and (grid[pos.lin][pos.col] in listOf('S','|','L','J'))) {
            // up
            val new: Position = Position(pos.lin-1, pos.col)
            if (grid[new.lin][new.col] in listOf('|', '7', 'F')) {
                val dist: Int = distances[new.lin][new.col]
                if ((dist == 0) or (currDist+1 < dist)) {
                    last.move(pos)
                    pos.lin -= 1
                    isMove = true
                    currDist += 1
                    distances[pos.lin][pos.col] = currDist
                }
            }
        } else if ((pos.col < grid[pos.lin].size-1)
            and (last.col != pos.col+1)
            and (grid[pos.lin][pos.col] in listOf('S','-','F','L'))) {
            // right
            val new: Position = Position(pos.lin, pos.col+1)
            if (grid[new.lin][new.col] in listOf('-', '7', 'J')) {
                val dist: Int = distances[new.lin][new.col]
                if ((dist == 0) or (currDist+1 < dist)) {
                    last.move(pos)
                    pos.col += 1
                    isMove = true
                    currDist += 1
                    distances[pos.lin][pos.col] = currDist
                }
            }
        } else if ((pos.lin < grid.size-1)
            and (last.lin != pos.lin+1)
            and (grid[pos.lin][pos.col] in listOf('S','|','F','7'))) {
            // down
            val new: Position = Position(pos.lin+1, pos.col)
            if (grid[new.lin][new.col] in listOf('|', 'L', 'J')) {
                val dist: Int = distances[new.lin][new.col]
                if ((dist == 0) or (currDist+1 < dist)) {
                    last.move(pos)
                    pos.lin += 1
                    isMove = true
                    currDist += 1
                    distances[pos.lin][pos.col] = currDist
                }
            }
        } else  if ((pos.col > 0)
            and (last.col != pos.col-1)
            and (grid[pos.lin][pos.col] in listOf('S','-','7','J'))) {
            // left
            val new: Position = Position(pos.lin, pos.col-1)
            if (grid[new.lin][new.col] in listOf('-', 'L', 'F')) {
                val dist: Int = distances[new.lin][new.col]
                if ((dist == 0) or (currDist+1 < dist)) {
                    last.move(pos)
                    pos.col -= 1
                    isMove = true
                    currDist += 1
                    distances[pos.lin][pos.col] = currDist
                }
            }
        }
        if (!isMove)
            break
    }
}

fun walkV0(start: Position, from: Position, grid: Array<CharArray>, distances: Array<IntArray>, distance: Int) {
    // up
    if (from.lin > 0) {
        val new: Position = Position(from.lin-1, from.col)
        if ((grid[from.lin][from.col] in listOf('S','|','L','J'))
            and (grid[new.lin][new.col] in listOf('|', '7', 'F'))) {
            val dist: Int = distances[new.lin][new.col]
            if ((dist == 0) or (distance+1 < dist)) {
                distances[new.lin][new.col] = distance+1
                walkV0(start, new, grid, distances, distance+1)
            }
        }
    }
    // right
    if (from.col < grid[from.lin].size-1) {
        val new: Position = Position(from.lin, from.col+1)
        if ((grid[from.lin][from.col] in listOf('S','-','F','L')) and
            (grid[new.lin][new.col] in listOf('-', '7', 'J'))) {
            val dist: Int = distances[new.lin][new.col]
            if ((dist == 0) or (distance+1 < dist)) {
                distances[new.lin][new.col] = distance+1
                walkV0(start, new, grid, distances, distance+1)
            }
        }
    }
    // down
    if (from.lin < grid.size-1) {
        val new: Position = Position(from.lin+1, from.col)
        if ((grid[from.lin][from.col] in listOf('S','|','F','7')) and
            (grid[new.lin][new.col] in listOf('|', 'L', 'J'))) {
            val dist: Int = distances[new.lin][new.col]
            if ((dist == 0) or (distance+1 < dist)) {
                distances[new.lin][new.col] = distance+1
                walkV0(start, new, grid, distances, distance+1)
            }
        }
    }
    // left
    if (from.col > 0) {
        val new: Position = Position(from.lin, from.col-1)
        if ((grid[from.lin][from.col] in listOf('S','-','7','J')) and
            (grid[new.lin][new.col] in listOf('-', 'L', 'F'))) {
            val dist: Int = distances[new.lin][new.col]
            if ((dist == 0) or (distance+1 < dist)) {
                distances[new.lin][new.col] = distance+1
                walkV0(start, new, grid, distances, distance+1)
            }
        }
    }
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }
    println(afile)

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line ->
        if (line.isNotEmpty())
            lines.add(line)
    }
    if (lines.isEmpty()) {
        println("error")
        return
    }

    val grid = Array(lines.size) { CharArray(lines[0].length) }
    var current: Position = Position(0, 0)
    for (i in lines.indices) {
        for (j in 0..<lines[i].length) {
            grid[i][j] = lines[i][j]
            if (lines[i][j] == 'S') {
                current.lin = i
                current.col = j
            }
        }
    }

    var res: Int = 0

    val distances = Array(grid.size) { IntArray(grid[0].size) { 0 } }
    startWalkV2(current, grid, distances)

    //for (l in distances) {
    //    for (c in l)
    //        print(c)
    //    println()
    //}

    val double: Array<CharArray> = doubleArray(distances, grid)

    noteExteriorV2(double)
    val solution: Array<CharArray> = reduceArray(double)

    //for (l in double) {
    //    for (c in l)
    //        print(c)
    //    println()
    //}

    for (l in solution) {
        for (c in l)
            if (c == '.') res += 1
    }

    println(res/4)
    println(res)
}

fun startWalkV2(from: Position, grid: Array<CharArray>, distances: Array<IntArray>) {
    val start: Position = from.clone()
    // up
    if (from.lin > 0) {
        val new: Position = Position(from.lin-1, from.col)
        if (grid[new.lin][new.col] in listOf('|', '7', 'F')) {
            distances[new.lin][new.col] = 1
            walkV2(start, new, grid, distances)
        }
    }
    // right
    if (from.col < grid[from.lin].size-1) {
        val new: Position = Position(from.lin, from.col+1)
        if (grid[new.lin][new.col] in listOf('-', '7', 'J')) {
            distances[new.lin][new.col] = 1
            walkV2(start, new, grid, distances)
        }
    }
    // down
    if (from.lin < grid.size-1) {
        val new: Position = Position(from.lin+1, from.col)
        if (grid[new.lin][new.col] in listOf('|', 'L', 'J')) {
            distances[new.lin][new.col] = 1
            walkV2(start, new, grid, distances)
        }
    }
    // left
    if (from.col > 0) {
        val new: Position = Position(from.lin, from.col-1)
        if (grid[new.lin][new.col] in listOf('-', 'L', 'F')) {
            distances[new.lin][new.col] = 1
            walkV2(start, new, grid, distances)
        }
    }
}

fun walkV2(start: Position, from: Position, grid: Array<CharArray>, distances: Array<IntArray>) {
    val pos: Position = from.clone()
    val last: Position = start.clone()
    while (pos != start) {
        var isMove: Boolean = false
        if ((pos.lin > 0)
            and (last.lin != pos.lin-1)
            and (grid[pos.lin][pos.col] in listOf('S','|','L','J'))) {
            // up
            val new: Position = Position(pos.lin-1, pos.col)
            if (grid[new.lin][new.col] in listOf('|', '7', 'F')) {
                val dist: Int = distances[new.lin][new.col]
                if (dist == 0) {
                    last.move(pos)
                    pos.lin -= 1
                    isMove = true
                    distances[pos.lin][pos.col] = 1
                }
            }
        } else if ((pos.col < grid[pos.lin].size-1)
            and (last.col != pos.col+1)
            and (grid[pos.lin][pos.col] in listOf('S','-','F','L'))) {
            // right
            val new: Position = Position(pos.lin, pos.col+1)
            if (grid[new.lin][new.col] in listOf('-', '7', 'J')) {
                val dist: Int = distances[new.lin][new.col]
                if (dist == 0) {
                    last.move(pos)
                    pos.col += 1
                    isMove = true
                    distances[pos.lin][pos.col] = 1
                }
            }
        } else if ((pos.lin < grid.size-1)
            and (last.lin != pos.lin+1)
            and (grid[pos.lin][pos.col] in listOf('S','|','F','7'))) {
            // down
            val new: Position = Position(pos.lin+1, pos.col)
            if (grid[new.lin][new.col] in listOf('|', 'L', 'J')) {
                val dist: Int = distances[new.lin][new.col]
                if (dist == 0) {
                    last.move(pos)
                    pos.lin += 1
                    isMove = true
                    distances[pos.lin][pos.col] = 1
                }
            }
        } else  if ((pos.col > 0)
            and (last.col != pos.col-1)
            and (grid[pos.lin][pos.col] in listOf('S','-','7','J'))) {
            // left
            val new: Position = Position(pos.lin, pos.col-1)
            if (grid[new.lin][new.col] in listOf('-', 'L', 'F')) {
                val dist: Int = distances[new.lin][new.col]
                if (dist == 0) {
                    last.move(pos)
                    pos.col -= 1
                    isMove = true
                    distances[pos.lin][pos.col] = 1
                }
            }
        }
        if (!isMove)
            break
    }
}

fun reduceArray(grid: Array<CharArray>): Array<CharArray> {
    val new = Array((grid.size + 1) / 2) { CharArray((grid[0].size + 1) / 2) { ' ' } }
    for (l in grid.indices step 2) {
        for (c in grid[l].indices step 2) {
            new[l / 2][c / 2] = grid[l][c]
        }
    }
    return new
}

fun doubleArray(distances: Array<IntArray>, grid: Array<CharArray>): Array<CharArray> {
    val new = Array(grid.size * 2 - 1) { CharArray(grid[0].size * 2 - 1) { '.' } }
    for (l in grid.indices) {
        for (c in grid[l].indices) {
            if ((distances[l][c] == 1) || (grid[l][c] == 'S')) {
                new[l*2][c*2] = grid[l][c]
                if (c < grid[l].size-1) {
                    if (grid[l][c] in listOf('-', 'L', 'F'))
                        new[l * 2][c * 2 + 1] = '-'
                    else if (grid[l][c] == 'S') {
                        if (grid[l][c+1]in listOf('7','J','-'))
                            new[l * 2][c * 2 + 1] = '-'
                    }
                }

                if (l < grid.size-1)
                    if (grid[l][c] in listOf('|','7','F','S'))
                        new[l*2+1][c*2] = '|'
            }
        }
    }
    return new
}

fun noteExteriorV2(distances: Array<CharArray>) {
    do {
        var isChange: Boolean = false
        for (lin in distances.indices) {
            for (col in 0..<distances[lin].size) {
                if (distances[lin][col] == '.') {
                    if ((lin == 0) || (lin == distances.size - 1)) {
                        // first/last line
                        distances[lin][col] = ' '
                        isChange = true
                    } else if (distances[lin - 1][col] == ' ') {
                        // line up empty
                        distances[lin][col] = ' '
                        isChange = true
                    } else if (distances[lin + 1][col] == ' ') {
                        // line down empty
                        distances[lin][col] = ' '
                        isChange = true
                    } else if ((col == 0) || (col == distances[lin].size - 1)) {
                        // first/last col
                        distances[lin][col] = ' '
                        isChange = true
                    } else if (distances[lin][col - 1] == ' ') {
                        // col left empty
                        distances[lin][col] = ' '
                        isChange = true
                    } else if (distances[lin][col + 1] == ' ') {
                        // col right empty
                        distances[lin][col] = ' '
                        isChange = true
                    }
                }
            }
        }
    } while(isChange)
}

fun noteExterior(distances: Array<CharArray>) {
    // recursive doesn't work :/
    // lines
    for (i in distances.indices) {
        if (distances[i][0] == '.')
            noteCase(i, 0, distances)
        if (distances[i][distances[i].size-1] == '.')
            noteCase(i, distances[i].size-1, distances)
    }
    //columns
    for (i in distances[0].indices) {
        if (distances[0][i] == '.')
            noteCase( 0, i, distances)
        if (distances[distances.size-1][i] == '.')
            noteCase(distances.size-1, i, distances)
    }
}

fun noteCase(line: Int, col: Int, distances: Array<CharArray>) {
    if (distances[line][col] == ' ') return
    println("l: $line - c: $col")

    if (distances[line][col] == '.') {
        distances[line][col] = ' '

        if ((line > 0) && (distances[line - 1][col] == '.'))
            noteCase(line - 1, col, distances)

        if ((line < distances.size - 1) && (distances[line + 1][col] == '.'))
            noteCase(line + 1, col, distances)

        if ((col > 0) && (distances[line][col - 1] == '.'))
            noteCase(line, col - 1, distances)

        if ((col < distances[0].size - 1) && (distances[line][col + 1] == '.'))
            noteCase(line, col + 1, distances)
    }
}
