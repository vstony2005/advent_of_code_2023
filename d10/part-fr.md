# --- Jour 10 : Labyrinthe de tuyaux ---

## --- Première partie ---

Vous utilisez le deltaplane pour voler dans l'air chaud de l'île Déserte jusqu'à l'île métallique flottante. Cette île est étonnamment froide et il n'y a pas de thermiques sur lesquels planer, alors vous laissez votre deltaplane derrière vous.

Vous vous promenez un peu sur l'île, mais vous ne trouvez ni personne ni animal. Cependant, vous trouvez de temps en temps des panneaux indiquant "[Sources chaudes](https://en.wikipedia.org/wiki/Hot_spring)" qui pointent dans une direction apparemment cohérente ; peut-être pourrez-vous trouver quelqu'un aux sources d'eau chaude et lui demander où sont fabriquées les pièces des machines du désert.

Le paysage ici est étranger ; même les fleurs et les arbres sont en métal. Alors que vous vous arrêtez pour admirer de l'herbe métallique, vous remarquez que quelque chose de métallique s'éloigne dans votre vision périphérique et saute dans un gros tuyau ! Cet animal ne ressemble à aucun de ceux que vous avez déjà vus ; si vous voulez le voir de plus près, vous devez le devancer.

En balayant la zone du regard, vous découvrez que tout le champ sur lequel vous vous trouvez est densément peuplé de tuyaux ; c'était difficile à dire au début parce qu'ils sont de la même couleur argent métallique que le " sol ". Vous faites un rapide croquis de tous les tuyaux de surface que vous pouvez voir (votre entrée d'énigme).

Les tuyaux sont disposés dans une grille bidimensionnelle de *tuiles* :

- `|` est un *tuyau vertical* reliant le nord et le sud.
- `-` est un *tuyau horizontal* reliant l'est et l'ouest.
- `L` est un *courbe à 90 degrés* reliant le nord et l'est.
- `J` est un *courbe à 90 degrés* reliant le nord et l'ouest.
- `7` est un *courbe à 90 degrés* reliant le sud et l'ouest.
- `F` est un *courbe à 90 degrés* reliant le sud et l'est.
- `.` est un *sol* ; il n'y a pas de tuyau dans cette tuile.
- `S` est la *position de départ* de l'animal ; il y a un tuyau sur cette tuile, mais votre croquis ne montre pas la forme du tuyau.

D'après l'acoustique de la course de l'animal, vous êtes sûr que le tuyau qui contient l'animal est *une grande boucle continue*.

Par exemple, voici une boucle carrée de tuyau :

```
.....
.F-7.
.|.|.
.L-J.
.....
```

Si l'animal était entré dans cette boucle par le coin nord-ouest, le croquis ressemblerait plutôt à ceci :

```
.....
.S-7.
.|.|.
.L-J.
.....
```

Dans le diagramme ci-dessus, vous pouvez toujours déterminer quels tuyaux forment la boucle principale : ce sont ceux qui sont connectés à `S`, les tuyaux auxquels ces tuyaux sont connectés, les tuyaux auxquels *ces* tuyaux sont connectés, et ainsi de suite. Chaque tuyau de la boucle principale se connecte à ses deux voisins (y compris `S`, qui aura exactement deux tuyaux qui se connecteront à lui, et qui est supposé se connecter à ces deux tuyaux).

Voici un croquis qui contient une boucle principale légèrement plus complexe :

```
-L|F7
7S-7|
L|7||
-L-J|
L|-JF
```

Dans le diagramme ci-dessus, vous pouvez toujours déterminer quels tuyaux forment la boucle principale : ce sont ceux qui sont connectés à `S`, les tuyaux auxquels ces tuyaux sont connectés, les tuyaux auxquels *ces* tuyaux sont connectés, et ainsi de suite. Chaque tuyau de la boucle principale se connecte à ses deux voisins (y compris `S`, qui aura exactement deux tuyaux qui se connecteront à lui, et qui est supposé se connecter à ces deux tuyaux).

Voici un croquis qui contient une boucle principale légèrement plus complexe :

```
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
```

Voici le même croquis d'exemple avec les tuiles de tuyaux supplémentaires, qui ne font pas partie de la boucle principale, également montrées :

```
7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ
```


Si vous voulez *prendre de l'avance sur l'animal*, vous devez trouver le carreau de la boucle qui est *le plus éloigné* de la position de départ. Comme l'animal est dans le tuyau, cela n'a pas de sens de mesurer la distance directe. Au lieu de cela, vous devez trouver la tuile qui prendrait le plus grand nombre de pas *le long de la boucle* pour atteindre le point de départ - quel que soit le sens de la boucle que l'animal a emprunté.

Dans le premier exemple avec la boucle carrée :

```
.....
.S-7.
.|.|.
.L-J.
.....
```

Vous pouvez compter la distance entre chaque tuile de la boucle et le point de départ comme suit :

```
.....
.012.
.1.3.
.234.
.....
```

Dans cet exemple, le point le plus éloigné du départ est à `*4*` pas.

Voici à nouveau la boucle la plus complexe :

```
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
```

Voici les distances pour chaque tuile de cette boucle :

```
..45.
.236.
01.78
14567
23...
```

Trouvez la boucle géante unique commençant à `S`. *Combien de pas le long de la boucle faut-il pour aller de la position de départ au point le plus éloigné de la position de départ ?

Pour commencer, [obtenez l'entrée de votre puzzle] (https://adventofcode.com/2023/day/10/input).

## --- Deuxième partie ---

Vous atteignez rapidement le point le plus éloigné de la boucle, mais l'animal n'en sort pas. Peut-être que son nid se trouve *dans la zone délimitée par la boucle* ?

Pour déterminer si cela vaut la peine de prendre le temps de chercher un tel nid, vous devez calculer combien de tuiles sont contenues dans la boucle. Par exemple :

```
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........
```

La boucle ci-dessus n'entoure que *quatre tuiles* - les deux paires de `.` au sud-ouest et au sud-est (marquées `I` ci-dessous). Les tuiles `.` du milieu (marquées `O` ci-dessous) ne sont *pas* dans la boucle. Voici à nouveau la même boucle avec les régions marquées :

```
...........
.S-------7.
.|F-----7|.
.||OOOOO||.
.||OOOOO||.
.|L-7OF-J|.
.|II|O|II|.
.L--JOL--J.
.....O.....
```

En fait, il n'est même pas nécessaire d'avoir un chemin complet vers l'extérieur pour que les tuiles soient considérées comme étant à l'extérieur de la boucle - se faufiler entre les tuyaux est également autorisé ! Ici, `I` est toujours à l'intérieur de la boucle et `O` est toujours à l'extérieur de la boucle :

```
..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
..........
```

Dans les deux exemples ci-dessus, les tuiles `*4*` sont entourées par la boucle.

Voici un exemple plus grand :

```
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
```

Le croquis ci-dessus comporte de nombreux éléments aléatoires, dont certains sont dans la boucle (`I`) et d'autres en dehors (`O`) :

```
OF----7F7F7F7F-7OOOO
O|F--7||||||||FJOOOO
O||OFJ||||||||L7OOOO 
FJL7L7LJLJ||LJIL-7OO
L--JOL7IIILJS7F-7L7O
OOOOF-JIIF7FJ|L7L7L7
OOOOL7IF7||L7|IL7L7|
OOOOO|FJLJ|FJ|F7|OLJ
OOOOFJL-7O||O||||OOO
OOOOL---JOLJOLJLJOOO
```

Dans cet exemple plus large, les tuiles `*8*` sont entourées par la boucle.

Toute tuile qui ne fait pas partie de la boucle principale peut être considérée comme incluse dans la boucle. Voici un autre exemple avec de nombreux bouts de tuyaux qui traînent et qui ne sont pas du tout connectés à la boucle principale :

```
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
```

Voici seulement les tuiles qui sont *enveloppées par la boucle* marquée par `I` :

```
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJIF7FJ-
L---JF-JLJIIIIFJLJJ7
|F|F-JF---7IIIL7L|7|
|FFJF7L7F-JF7IIL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
```

Dans ce dernier exemple, les tuiles `*10*` sont incluses dans la boucle.

Pour savoir si vous avez le temps de chercher le nid, calculez la surface à l'intérieur de la boucle. *Combien de tuiles sont incluses dans la boucle ?

