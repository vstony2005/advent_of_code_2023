# --- Jour 17: Creuset maladroit ---

## --- Première partie ---

La lave commence à couler rapidement une fois que l'usine de production de lave est opérationnelle. En partant, le renne vous offre un parachute, ce qui vous permet d'atteindre rapidement l'île de Gear.

En descendant, votre vue plongeante sur Gear Island vous permet de comprendre pourquoi vous avez eu du mal à trouver quelqu'un en montant : la moitié de Gear Island est vide, mais la moitié en dessous de vous est une gigantesque ville-usine !

Vous atterrissez près du bassin de lave qui se remplit progressivement au pied de votre nouvelle *chute de lave*. Des lavaducs finiront par transporter la lave dans toute la ville, mais pour l'utiliser immédiatement, les Elfes la chargent dans de grands [Creusets](https://en.wikipedia.org/wiki/Crucible) sur roues.

Les creusets sont lourds et poussés à la main. Malheureusement, les creusets deviennent très difficiles à diriger à grande vitesse, et il peut donc être difficile de rester en ligne droite très longtemps.

Pour que l'Île déserte obtienne les pièces de machine dont elle a besoin le plus rapidement possible, vous devez trouver le meilleur moyen d'acheminer le creuset *de la piscine de lave à l'usine de pièces de machine*. Pour ce faire, vous devez minimiser les *pertes de chaleur* tout en choisissant un itinéraire qui n'oblige pas le creuset à suivre une *ligne droite* pendant trop longtemps.

Heureusement, les Elfes disposent d'une carte qui utilise les schémas de circulation, la température ambiante et des centaines d'autres paramètres pour calculer exactement la perte de chaleur à laquelle on peut s'attendre pour un creuset entrant dans un pâté de maisons donné.

Par exemple :

```
2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533
```

Chaque bloc urbain est marqué d'un seul chiffre qui représente la *quantité de chaleur perdue si le creuset pénètre dans ce bloc*. Le point de départ, la piscine de lave, est l'îlot en haut à gauche ; la destination, l'usine de pièces de machine, est l'îlot en bas à droite.  (Étant donné que vous commencez déjà dans le bloc en haut à gauche, vous ne subissez pas la perte de chaleur de ce bloc à moins que vous ne quittiez ce bloc pour y revenir).

Comme il est difficile de maintenir le creuset lourd en ligne droite pendant très longtemps, il peut se déplacer de trois blocs au maximum dans une même direction avant de devoir tourner de 90 degrés vers la gauche ou vers la droite. Le creuset ne peut pas non plus faire marche arrière ; après être entré dans chaque bloc de la ville, il ne peut que tourner à gauche, continuer tout droit ou tourner à droite.

Une façon de *minimiser la perte de chaleur* est de suivre ce chemin :

```
2>>34^>>>1323
32v>>>35v5623
32552456v>>54
3446585845v52
4546657867v>6
14385987984v4
44578769877v6
36378779796v>
465496798688v
456467998645v
12246868655<v
25465488877v5
43226746555v>
```

Ce chemin ne déplace jamais plus de trois blocs consécutifs dans la même direction et entraîne une perte de chaleur de seulement `*102*`.

Si vous dirigez le creuset de la piscine de lave vers l'usine de pièces détachées, mais que vous ne déplacez pas plus de trois blocs consécutifs dans la même direction, *quelle est la perte de chaleur la plus faible qu'il puisse subir ?

Pour commencer, [obtenez les données de votre puzzle](https://adventofcode.com/2023/day/17/input).

## --- Deuxième partie ---

Les creusets de lave ne sont tout simplement pas assez grands pour fournir une quantité suffisante de lave à l'usine de pièces détachées. Les Elfes vont donc passer à des *crucibles ultra*.

Les ultra-creusets sont encore plus difficiles à diriger que les creusets normaux. Non seulement ils ont du mal à aller en ligne droite, mais ils ont aussi du mal à tourner !

Une fois qu'un creuset ultra commence à se déplacer dans une direction, il doit se déplacer d'au moins quatre blocs dans cette direction avant de pouvoir tourner (ou même avant de pouvoir s'arrêter à la fin). Cependant, il finira par devenir bancal : un ultra-creuset peut se déplacer au maximum de *dix blocs consécutifs* sans tourner.

Dans l'exemple ci-dessus, un ultra-creuset pourrait suivre ce chemin pour minimiser la perte de chaleur :

```
2>>>>>>>>1323
32154535v5623
32552456v4254
34465858v5452
45466578v>>>>
143859879845v
445787698776v
363787797965v
465496798688v
456467998645v
122468686556v
254654888773v
432267465553v
```

Dans l'exemple ci-dessus, un creuset ultra subira une perte de chaleur minimale de `*94*`.

Voici un autre exemple :

```
111111111111
999999999991
999999999991
999999999991
999999999991
```

Malheureusement, un ultra-creuset devrait prendre un chemin malheureux comme celui-ci :

```
1>>>>>>>1111
9999999v9991
9999999v9991
9999999v9991
9999999v>>>>
```

Cette route fait que l'ultra creuset subit la perte de chaleur minimale possible de `*71*`.

En dirigeant l'*ultra-creuset* de la piscine de lave vers l'usine de pièces détachées, *quelle est la perte de chaleur la plus faible qu'il puisse subir ?*