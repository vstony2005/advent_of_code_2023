import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    readfile2("input")
}

//region my types
operator fun PairInt.plus(other: PairInt): PairInt =
    Pair(this.first + other.first, this.second + other.second)
operator fun PairInt.minus(other: Pair<Int,Int>): PairInt =
    Pair(this.first - other.first, this.second - other.second)
operator fun PairInt.times(n: Int): PairInt =
    Pair(this.first * n, this.second * n)

operator fun <T> List<List<T>>.get(index: Pair<Int, Int>): T =
    this[index.first][index.second]
operator fun <T> List<List<T>>.set(index: Pair<Int, Int>, value: T) {
    this[index] = value
}

typealias PairInt = Pair<Int,Int>

typealias ListChars = List<List<Char>>
typealias ListInts = List<List<Int>>
typealias ListStrings = List<List<String>>

typealias MutListChars = MutableList<MutableList<Char>>
typealias MutListInts = MutableList<MutableList<Int>>
typealias MutListStrings = MutableList<MutableList<String>>

typealias ListMoves = List<Pair<PairInt, Directions>>
//endregion

enum class Directions {
    UP,
    RIGHT,
    DOWN,
    LEFT;

    fun pair(): PairInt {
        return when (this) {
            UP -> Pair(-1,0)
            RIGHT -> Pair(0,1)
            DOWN -> Pair(1,0)
            LEFT -> Pair(0,-1)
        }
    }

    fun turnLeft(): Directions {
        return when (this) {
            UP -> LEFT
            RIGHT -> UP
            DOWN -> RIGHT
            LEFT -> DOWN
        }
    }

    fun turnRight(): Directions {
        return when (this) {
            UP -> RIGHT
            RIGHT -> DOWN
            DOWN -> LEFT
            LEFT -> UP
        }
    }
}

fun divers() {
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }

    val grid: ListInts = lines.map { l -> l.map { it.digitToInt() } }
    //grid.map { line -> line.joinToString("").println() }

    var res: Int = runDijkstraFor(grid)

    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

/**
 * Node for Dijkstra path
 * @property position position of node
 * @property currentWeight Weight of node
 * @property direction direction of node
 * @property straightCounter number of moves in one direction
 * */
data class NodePath (
    val position: PairInt,
    val currentWeight: Int,
    val direction: Directions,
    val straightCounter: Int
)

/* solve from NineBerry (c#) https://dotnetfiddle.net/9DN6g5 */
fun runDijkstraFor(grid: ListInts, minStraightMoves: Int = 1, maxStraightMoves: Int = 3): Int {
    /**
     * line indexes
     */
    val validLins: IntRange = grid.indices

    /**
     * column indexes
     */
    val validCols: IntRange = grid[0].indices

    /**
     * finish position
     */
    val goal: PairInt = Pair(grid.size-1, grid[0].size-1)

    /**
     * start position
     */
    val start: PairInt = Pair(0,0)

    /**
     * nodes to be processed
     */
    val todoPaths: MutableList<NodePath> = mutableListOf()

    /**
     * nodes visited
     */
    val visited: HashMap<Triple<PairInt,Directions, Int>, Int> = hashMapOf()

    val allDirections: List<Directions> = listOf(
        Directions.UP,
        Directions.RIGHT,
        Directions.DOWN,
        Directions.LEFT
    )

    allDirections.map { todoPaths.add(NodePath(start, 0, it, 0)) }

    while (todoPaths.isNotEmpty()) {
        todoPaths.sortBy { it.currentWeight }
        val node = todoPaths.removeFirst()

        // solve
        if (node.position == goal && node.straightCounter >= minStraightMoves && node.straightCounter <= maxStraightMoves) {
            return node.currentWeight
        }

        if (visited.contains(Triple(node.position, node.direction, node.straightCounter)))
            continue
        visited[Triple(node.position, node.direction, node.straightCounter)] = 0

        fun performStep(nextDirection: Directions, nextStraightMoves: Int) {
            val nextPos: PairInt = node.position + nextDirection.pair()

            if (nextPos.first !in validLins || nextPos.second !in validCols)
                return
            val weight: Int = grid[nextPos]
            todoPaths.add(NodePath(nextPos, node.currentWeight + weight, nextDirection, nextStraightMoves))
        }

        // change direction
        if (node.straightCounter >= minStraightMoves) {
            performStep(node.direction.turnLeft(), 1)
            performStep(node.direction.turnRight(), 1)
        }

        // continue direction
        if (node.straightCounter < maxStraightMoves) {
            performStep(node.direction, node.straightCounter + 1)
        }
    }

    return 0
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }

    val grid: ListInts = lines.map { l -> l.map { it.digitToInt() } }
    //grid.map { line -> line.joinToString("").println() }

    var res: Int = runDijkstraFor(grid, 4, 10)

    "res: ${res}".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}
