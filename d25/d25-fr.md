# --- Jour 25 : Chargement de neige ---

## --- Première partie ---

Toujours* sans neige, vous vous rendez au dernier endroit que vous n'avez pas vérifié : le centre de Snow Island, juste en dessous de la cascade.

Ici, quelqu'un a manifestement essayé de résoudre le problème. Des centaines de machines météorologiques, d'almanachs, de modules de communication, d'empreintes de sabots, de pièces de machines, de miroirs, de lentilles, etc. sont éparpillés un peu partout.

D'une manière ou d'une autre, tout a été *câblé* pour former un gigantesque appareil de production de neige, mais rien ne semble fonctionner. Vous vérifiez un petit écran sur l'un des modules de communication : Erreur 2023. Il ne dit pas ce que signifie "Erreur 2023", mais il y a le numéro de téléphone d'une ligne d'assistance imprimé dessus.

"Bonjour, vous avez joint Weather Machines And So On, Inc. How can I help you ?" Vous expliquez la situation.

"Erreur 2023, dites-vous ? Il s'agit d'une erreur de surcharge, bien sûr !  Cela signifie que vous avez trop de composants branchés. Essayez de débrancher certains composants et..." Vous expliquez qu'il y a des centaines de composants ici et que vous êtes un peu pressé.

"Voyons à quel point c'est grave ; voyez-vous un *gros bouton rouge de réinitialisation* quelque part ? Il devrait se trouver sur son propre module. Si tu appuies dessus, ça ne changera probablement rien, mais ça indiquera à quel point les choses sont surchargées." Au bout d'une minute ou deux, vous trouvez le bouton de réinitialisation ; il est si gros qu'il faut deux mains pour faire levier et l'enfoncer. L'écran s'affiche alors :

```
SURCHARGE DU SYSTÈME !

Les composants connectés nécessiteraient
une puissance égale à au moins 100 étoiles !
```

"Attendez, *combien* de composants avez-vous dit sont branchés ? Avec autant d'équipement, vous pourriez produire de la neige pour une *entière*..." Vous déconnectez l'appel.

Vous êtes loin d'avoir autant d'étoiles - vous devez trouver un moyen de déconnecter au moins la moitié de l'équipement ici, mais c'est déjà Noël ! Vous n'avez le temps de débrancher que *trois fils*.

Heureusement, quelqu'un a laissé un schéma de câblage (votre contribution au puzzle) qui montre *comment les composants sont connectés*. Voici un exemple :

```
jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr
```

Chaque ligne contient le *nom d'un composant*, deux points, puis *une liste d'autres composants* auxquels ce composant est connecté. Les connexions ne sont pas directionnelles ; `abc : xyz` et `xyz : abc` représentent la même configuration. Chaque connexion entre deux composants n'est représentée qu'une seule fois, de sorte que certains composants peuvent n'apparaître qu'à gauche ou à droite d'un deux-points.

Dans cet exemple, si vous déconnectez le fil entre `hfx`/`pzl`, le fil entre `bvb`/`cmg`, et le fil entre `nvd`/`jqt`, vous *diviserez les composants en deux groupes séparés et déconnectés* :

- `*9*` components: `cmg`, `frs`, `lhk`, `lsr`, `nvd`, `pzl`, `qnr`, `rsh`, and `rzs`.
- `*6*` components: `bvb`, `hfx`, `jqt`, `ntq`, `rhn`, and `xhk`.

En multipliant les tailles de ces groupes, on obtient `*54*`.

Trouvez les trois fils que vous devez déconnecter pour diviser les composants en deux groupes distincts. *Qu'obtenez-vous si vous multipliez les tailles de ces deux groupes ?*

Pour commencer, [obtenez les données de votre puzzle](https://adventofcode.com/2023/day/25/input).