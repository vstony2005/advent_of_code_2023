import java.io.File

fun main(args: Array<String>) {
    //part1()
    part2()
}

fun part1() {
    readfile1(".\\test1.txt")
    readfile1(".\\input1.txt")
}

fun part2() {
    //readfile2(".\\test1.txt")
    readfile2(".\\input1.txt")
}

fun readfile1(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    var res: Int = 0

    f.forEachLine {
        val s1: String = it.split(":")[1]
        val s2: List<String> = s1.split("|")
        val reg = Regex("[0-9]+")
        val l1: List<Int> = reg.findAll(s2[0]).map { it.value.toInt() }.toList()
        val l2: List<Int> = reg.findAll(s2[1]).map { it.value.toInt() }.toList()

        var r: Int = 0

        for (i in l2) {
            if (i in l1) {
                if (r == 0) r = 1
                else r *= 2
            }
        }
        res += r
    }
    println(res)
}

fun readfile2(afile: String) {
    val f = File(afile)
    if (!f.exists()) {
        println("no file")
        return
    }

    val lines = mutableListOf<String>()
    var res: Int = 0

    f.forEachLine {
        lines.add(it)
    }

    for (i in 0..<lines.size) {
        res += readLine(0, i, lines)
    }

    println(res)
}

fun readLine(aRes: Int, aIndex: Int, aLines: List<String>): Int {
    var res = aRes + 1
    val s1: String = aLines[aIndex].split(":")[1]
    val s2: List<String> = s1.split("|")
    val reg = Regex("[0-9]+")
    val l1: List<Int> = reg.findAll(s2[0]).map { it.value.toInt() }.toList()
    val l2: List<Int> = reg.findAll(s2[1]).map { it.value.toInt() }.toList()

    var n = 0
    for (i in l1) {
        if (i in l2) n += 1
    }

    for (i in 1..n) res += readLine(0, aIndex + i, aLines)

    return res
}

