import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun divers() {
    val pattern: String = """OOOO.#.O..
OO..#....#
OO..O##..O
O..#.OO...
........#.
..#....#.#
..O..#.O.O
..O.......
#....###..
#....#...."""
    val lines = pattern.split("\n") // 10 lines
    lines.println()
    println()
    // transform cols to lines
    val transf = lines[0].indices.map { col ->
        lines.map { line -> line[col] }.joinToString("")
    }
    transf.joinToString("\n").println()
    transf.size.println()

    // count values
    val res: Int = totalLoad(lines)
    res.println()
}

fun part1() {
    readfile1("test")
    readfile1("input")
}

fun part2() {
    readfile2("test")
    readfile2("input")
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //"(1) org".println()
    //lines.joinToString("\n").println()

    val moves = moveRocks(lines)

    //"\n(5) count".println()
    var res: Int = totalLoad(moves)

    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

private fun totalLoad(moves: List<String>) = moves
    .reversed()
    .mapIndexed { index, line -> line to index + 1 }
    .sumOf { (lin, value) -> lin.count { it == 'O' } * value }

private fun moveRocks(lines: List<String>): List<String> {
    // delete empty
    val linesfilt = lines.filter { it.isNotEmpty() }
    // transform cols to lines (transpose)
    val colsLines = linesfilt[0].indices.map { col ->
        linesfilt.map { line -> line[col] }.joinToString("")
    }

    // move
    val reg = Regex("(\\.+)(O)")
    val moves = colsLines.map { l ->
        var l1 = l
        var l2 = l
        do {
            l1 = l2
            l2 = l1.replace(reg, "$2$1")
        } while (l1 != l2)
        l2
    }

    // turn in correct position (transpose bis)
    val linesCols = moves[0].indices.map { col ->
        moves.map { line -> line[col] }.joinToString("")
    }

    return linesCols
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid("(1) org")

    var res: Int = do1MCycles(lines)

    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

private fun do1MCycles(lines: List<String>): Int {
    var cycle = lines
    val saves : MutableList<List<String>> = mutableListOf()
    do {
        saves.add(cycle)
        cycle = doOneCycle(cycle)
    } while (!saves.contains(cycle))
    val posIndex: Int = saves.indexOf(cycle)
    val repeatLines = saves.drop(posIndex)
    val posAfter1M: Int = (1_000_000_000 - posIndex) % repeatLines.size

    return totalLoad(repeatLines[posAfter1M])
}

private fun doNCycles(lines: List<String>, n: Int): List<String> {
    var cycle = lines
    cycle.printGrid("origin")
    repeat(n) {
        cycle = doOneCycle(cycle)
        cycle.printGrid("(cycle)")
    }
    return cycle
}

private fun doOneCycle(lines: List<String>): List<String> {
    var moves: List<String> = lines
    repeat(4) {
        moves = moveRocks(moves)
        moves = turnMap(moves)
    }
    return moves
}

fun turnMap(rocksMap: List<String>): List<String> {
    //rocksMap.printGrid("turn (begin)")
    val lines = rocksMap[0].indices.map { col ->
        rocksMap.map { line -> line[col] }
            .reversed()
            .joinToString("")
    }
    //lines.printGrid("turn (end)")
    return lines
}
