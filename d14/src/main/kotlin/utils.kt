import java.io.File

fun readTextFile(fileName: String): List<String> {
    val name = ".\\$fileName.txt"
    val f = File(name)
    if (!f.exists()) {
        println("no file")
        return listOf()
    }
    println("filename: $name")

    val lines: MutableList<String> = mutableListOf<String>()
    f.forEachLine { line -> lines.add(line) }
    return lines
}

fun Any?.println() = println(this)
fun Any?.println(txt: String) = "$txt$this".println()

fun List<String>.printGrid(txt: String) {
    if (txt.isNotEmpty())
        kotlin.io.println(txt)
    this.joinToString("\n").println()
    kotlin.io.println()
}
