
# --- Day 14: Parabole réflectrice ---

## --- Première partie ---

Vous arrivez à l'endroit où tous les miroirs pointaient : une énorme [Antenne parabolique](https://en.wikipedia.org/wiki/Parabolic_reflector) attachée au flanc d'une autre grande montagne.

La parabole est composée de nombreux petits miroirs, mais si les miroirs eux-mêmes ont à peu près la forme d'une parabole, chacun d'entre eux semble pointer dans la mauvaise direction.  Si la parabole est censée concentrer la lumière, tout ce qu'elle fait pour l'instant, c'est de l'envoyer dans une direction vague.

C'est ce système qui doit fournir l'énergie à la lave ! Si vous concentrez la parabole, vous pourrez peut-être aller là où elle pointe et utiliser la lumière pour réparer la production de lave.

En y regardant de plus près, on s'aperçoit que les différents miroirs sont reliés par un système élaboré de cordes et de poulies à une grande plate-forme métallique située sous la parabole. Cette plate-forme est recouverte de gros rochers de formes diverses. En fonction de leur position, le poids des rochers déforme la plate-forme, et la forme de la plate-forme contrôle le mouvement des cordes et, en fin de compte, la mise au point de la parabole.

En bref : si vous déplacez les pierres, vous pouvez orienter le plat. La plate-forme dispose même d'un panneau de commande sur le côté qui vous permet de l'"incliner" dans l'une des quatre directions ! Les pierres arrondies (`O`) roulent lorsque la plate-forme est inclinée, tandis que les pierres en forme de cube (`#`) restent en place. Vous notez la position de tous les espaces vides (`.`) et des rochers (votre entrée dans le puzzle). Par exemple :

```
O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
```

Commencez par incliner le levier de façon à ce que tous les rochers glissent vers le nord jusqu'à la limite de leur capacité :

```
OOOO.#.O..
OO..#....#
OO..O##..O
O..#.OO...
........#.
..#....#.#
..O..#.O.O
..O.......
#....###..
#....#....
```

Vous remarquez que les poutres de soutien du côté nord de la plate-forme sont *endommagées* ; pour vous assurer que la plate-forme ne s'effondre pas, vous devez calculer la *charge totale* sur les poutres de soutien nord.

La charge causée par un seul rocher arrondi (`O`) est égale au nombre de rangées entre le rocher et le bord sud de la plate-forme, y compris la rangée sur laquelle se trouve le rocher. (Les pierres en forme de cube (`#`) ne contribuent pas à la charge). Ainsi, la quantité de charge causée par chaque rocher dans chaque rangée est la suivante :

```
OOOO.#.O.. 10
OO..#....#  9
OO..O##..O  8
O..#.OO...  7
........#.  6
..#....#.#  5
..O..#.O.O  4
..O.......  3
#....###..  2
#....#....  1
```

La charge totale est la somme de la charge causée par toutes les *roches arrondies*. Dans cet exemple, la charge totale est de `*136*`.

Inclinez la plate-forme de façon à ce que les pierres arrondies roulent toutes vers le nord. Ensuite, *quelle est la charge totale sur les poutres de soutien nord ?*

Pour commencer, [obtenez les données de votre puzzle] (https://adventofcode.com/2023/day/14/input).

## --- Deuxième partie ---

La parabole se déforme, mais pas de manière à focaliser le faisceau. Pour cela, vous devez déplacer les rochers vers les bords de la plate-forme. Heureusement, un bouton situé sur le côté du panneau de commande et intitulé "*cycle d'essorage*" permet de faire cela !

Chaque *cycle* fait basculer la plateforme quatre fois de façon à ce que les rochers arrondis roulent vers le *nord*, puis vers l'*ouest*, puis vers le *sud*, puis vers l'*est*. Après chaque basculement, les pierres arrondies roulent aussi loin qu'elles le peuvent avant que la plate-forme ne bascule dans la direction suivante. Après un cycle, la plate-forme aura fini de faire rouler les pierres arrondies dans ces quatre directions et dans cet ordre.

Voici ce qui se passe dans l'exemple ci-dessus après chacun des premiers cycles :

```
Après 1 cycle:
.....#....
....#...O#
...OO##...
.OO#......
.....OOO#.
.O#...O#.#
....O#....
......OOOO
#...O###..
#..OO#....

Après 2 cycles:
.....#....
....#...O#
.....##...
..O#......
.....OOO#.
.O#...O#.#
....O#...O
.......OOO
#..OO###..
#.OOO#...O

Après 3 cycles:
.....#....
....#...O#
.....##...
..O#......
.....OOO#.
.O#...O#.#
....O#...O
.......OOO
#...O###.O
#.OOO#...O
```

Ce processus devrait fonctionner si vous le laissez en marche suffisamment longtemps, mais vous vous inquiétez toujours pour les poutres de soutien nord. Pour vous assurer qu'elles survivront pendant un certain temps, vous devez calculer la *charge totale* sur les poutres de soutien nord après `1000000000` cycles.


Dans l'exemple ci-dessus, après `1000000000` cycles, la charge totale sur les poutres de soutien nord est de `*64*`.


Lancez le cycle d'essorage pendant `1000000000` cycles. Après cela, *quelle est la charge totale sur les poutres de soutien nord ?*
