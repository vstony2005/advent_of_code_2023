import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    //divers()
    //part1()
    part2()
    println("end.")
}

fun divers() {
    //'A'.code.println("A: ") // A = 65
    //65.toChar().println("65: ") // 65 = A
    //'1'.code.println("1: ") // 1 = 49
    //49.toChar().println("49: ") // 49 = 1

    //'1'.digitToInt().println("digitToInt: ") // 1
    //'1'.code.println("code: ") // 49

    algoHash("HASH").println("HASH: ")
}

fun part1() {
    "part 1".println()
    readfile1("test")
    readfile1("input")
}

fun part2() {
    "part 2".println()
    readfile2("test")
    readfile2("input")
}

fun readfile1(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    //lines.printGrid()

    val codes: List<String> = lines.first().split(",")

    var res: Int = hashList(codes)
    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

fun hashList(words: List<String>): Int {
    var res: Int = 0
    words.map { res += algoHash(it) }
    return res
}

fun algoHash(word: String): Int {
    val chars = word.toList()
    var res = 0
    chars.map { c ->
        res = ((res + c.code) * 17) % 256
    }
    return res
}

fun readfile2(afile: String) {
    val startFun = System.currentTimeMillis()
    val lines = readTextFile(afile)
    if (lines.isEmpty()) {
        println("nothing")
        return
    }
    lines.printGrid()

    val codes: List<String> = lines.first().split(",")
    val boxes: HashMap<Int, Box> = hashMapOf()
    for (i in 0..255)
        boxes[i] = Box()

    codes.map { code ->
        val operation: String = Regex("^[a-z]+").find(code)!!.value
        val codeBox = algoHash(operation)
        if (code.contains("=")){
            val split = code.split("=")
            boxes[codeBox]!!.updateLens(split[0], split[1].toInt())
        } else if (code.endsWith("-")) {
            boxes[codeBox]!!.removeLens(code.dropLast(1))
        }
    }

    var res: Int = 0
    boxes.keys.map { res += boxes[it]!!.power(it + 1) }
    "res: $res".println()

    val tim = System.currentTimeMillis() - startFun
    println("done in ${TimeUnit.MILLISECONDS.toMinutes(tim)} "
            + "min ${TimeUnit.MILLISECONDS.toSeconds(tim)} secs")
}

class Lens (lbl: String, length: Int) {
    val label: String = lbl
    var focalLength: Int = length
}

class Box() {
    val items: MutableList<Lens> = mutableListOf()

    fun updateLens(lbl: String, length: Int) {
        val lens: Lens? = items.find { it.label == lbl }
        if (lens != null)
            lens.focalLength = length
        else
            items.add(Lens(lbl, length))
    }

    fun removeLens(lbl: String) {
        val lens: Lens? = items.find { it.label == lbl }
        if (lens != null)
            items.remove(lens)
    }

    fun power(num: Int): Int {
        var n: Int = 0
        items.mapIndexed() { i, l -> n += num * (i + 1) * items[i].focalLength }
        return n
    }
}
