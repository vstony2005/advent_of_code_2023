# --- Jour 15 : Bibliothèque d'objectifs ---

## --- Première partie ---

La parabole à réflecteur parabolique nouvellement focalisée envoie toute la lumière collectée vers un point situé sur le flanc d'une autre montagne - la plus grande montagne de l'île de Lava. En vous approchant de la montagne, vous constatez que la lumière est captée par le mur d'une grande installation encastrée dans le flanc de la montagne.

Vous trouvez une porte sous un grand panneau indiquant "Lava Production Facility" et à côté d'un plus petit panneau indiquant "Danger - Personal Protective Equipment required beyond this point".

En entrant, vous êtes immédiatement accueilli par un renne un peu paniqué, portant des lunettes et un [casque de protection](https://en.wikipedia.org/wiki/Hard_hat). Le renne vous conduit vers une étagère de lunettes et de casques (vous en trouvez rapidement qui vous conviennent), puis plus loin dans l'installation. À un moment donné, vous passez devant un bouton portant une légère marque de museau et l'inscription "PUSH FOR HELP". Pas étonnant que vous ayez été chargé dans ce [trébuchet](https://adventofcode.com/2023/day/1) si rapidement !

Vous passez une dernière série de portes entourées d'encore plus de panneaux d'avertissement et entrez dans ce qui doit être la pièce qui recueille toute la lumière de l'extérieur. Alors que vous admirez le large assortiment de lentilles disponibles pour concentrer davantage la lumière, le renne vous apporte un livre intitulé "Manuel d'initialisation".

"Bonjour !", commence joyeusement le livre, ignorant apparemment que le renne inquiet lit par-dessus votre épaule. "Cette procédure vous permettra de mettre en service l'installation de production de lave, sans brûler ni faire fondre quoi que ce soit d'involontaire !

"Avant de commencer, préparez-vous à utiliser l'algorithme Holiday ASCII String Helper (annexe 1A). Vous passez à l'annexe 1A. Le renne se rapproche avec intérêt.

L'algorithme HASH permet de transformer n'importe quelle [chaîne](https://en.wikipedia.org/wiki/String_(computer_science)) de caractères en un seul *nombre* compris entre 0 et 255. Pour exécuter l'algorithme HASH sur une chaîne de caractères, commencez par une *valeur courante* de `0`. Ensuite, pour chaque caractère de la chaîne, en commençant par le début :

- Déterminez le [code ASCII](https://en.wikipedia.org/wiki/ASCII#Printable_characters) pour le caractère actuel de la chaîne.
- Augmentez la *valeur courante* du code ASCII que vous venez de déterminer.
- Fixez la *valeur courante* à elle-même multipliée par `17`.
- Fixez la *valeur courante* au [reste](https://en.wikipedia.org/wiki/Modulo) de la division par `256`.

Après avoir suivi ces étapes pour chaque caractère de la chaîne dans l'ordre, la *valeur courante* est le résultat de l'algorithme HASH.

Ainsi, pour trouver le résultat de l'exécution de l'algorithme HASH sur la chaîne `HASH` :

- La *valeur courante* commence à `0`.
- Le premier caractère est `H` ; son code ASCII est `72`.
- La *valeur courante* augmente jusqu'à `72`.
- La *valeur courante* est multipliée par `17` pour devenir `1224`.
- La *valeur courante* devient `*200*` (le reste de `1224` divisé par `256`).
- Le caractère suivant est `A` ; son code ASCII est `65`.
- La *valeur courante* passe à `265`.
- La *valeur courante* est multipliée par `17` pour devenir `4505`.
- La *valeur courante* devient `*153*` (le reste de `4505` divisé par `256`).
- Le caractère suivant est `S` ; son code ASCII est `83`.
- La *valeur courante* passe à `236`.
- La *valeur courante* est multipliée par `17` pour devenir `4012`.
- La *valeur courante* devient `*172*` (le reste de `4012` divisé par `256`).
- Le caractère suivant est `H` ; son code ASCII est `72`.
- La *valeur courante* passe à `244`.
- La *valeur courante* est multipliée par `17` pour devenir `4148`.
- La *valeur courante* devient `*52*` (le reste de `4148` divisé par `256`).

Ainsi, le résultat de l'exécution de l'algorithme HASH sur la chaîne `HASH` est `*52*`.

La *séquence d'initialisation* (votre entrée puzzle) est une liste d'étapes séparées par des virgules pour démarrer l'installation de production de lave. *Ignorez les caractères de nouvelle ligne lors de l'analyse de la séquence d'initialisation. Pour vérifier que votre algorithme HASH fonctionne, le livre propose la somme des résultats de l'exécution de l'algorithme HASH sur chaque étape de la séquence d'initialisation.

Par exemple :

```
rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
```

Cette séquence d'initialisation spécifie 11 étapes individuelles ; le résultat de l'exécution de l'algorithme HASH sur chacune des étapes est le suivant :

- `rn=1` devient  `*30*`.
- `cm-` devient  `*253*`.
- `qp=3` devient  `*97*`.
- `cm=2` devient  `*47*`.
- `qp-` devient  `*14*`.
- `pc=4` devient  `*180*`.
- `ot=9` devient  `*9*`.
- `ab=5` devient  `*197*`.
- `pc-` devient  `*48*`.
- `pc=6` devient  `*214*`.
- `ot=7` devient  `*231*`.

Dans cet exemple, la somme de ces résultats est `*1320*`. Malheureusement, le renne a volé la page contenant le numéro de vérification attendu et court actuellement autour de l'installation avec elle, tout excité.

Exécutez l'algorithme HASH à chaque étape de la séquence d'initialisation. *Quelle est la somme des résultats ? (La séquence d'initialisation est une longue ligne ; soyez prudent lorsque vous la copiez-collez).

Pour commencer, [obtenez l'entrée de votre puzzle] (https://adventofcode.com/2023/day/15/input).

## --- Deuxième partie ---

Vous convainquez le renne de vous apporter la page ; la page confirme que votre algorithme HASH fonctionne.

Le livre décrit ensuite une série de 256 *boîtes* numérotées de 0 à 255. Les boîtes sont disposées en ligne à partir du point où la lumière pénètre dans l'installation. Les boîtes sont percées de trous qui permettent à la lumière de passer d'une boîte à l'autre tout au long de la ligne.

```
      +-----+  +-----+         +-----+
Light | Box |  | Box |   ...   | Box |
----------------------------------------->
      |  0  |  |  1  |   ...   | 255 |
      +-----+  +-----+         +-----+
```

À l'intérieur de chaque boîte, il y a plusieurs *fentes pour lentilles* qui maintiennent une lentille correctement positionnée pour focaliser la lumière qui passe à travers la boîte. Sur le côté de chaque boîte, un panneau s'ouvre pour permettre d'insérer ou de retirer des lentilles si nécessaire.

Le long du mur parallèle aux boîtes se trouve une grande bibliothèque contenant des lentilles classées par *longueur focale* allant de `1` à `9`. Le renne vous apporte également une petite [imprimante d'étiquettes portative](https://en.wikipedia.org/wiki/Label_printer).

Le livre explique ensuite comment effectuer chaque étape de la séquence d'initialisation, un processus appelé Holiday ASCII String Helper Manual Arrangement Procedure, ou *HASHMAP* en abrégé.

Chaque étape commence par une séquence de lettres indiquant l'"étiquette" de la lentille sur laquelle l'étape opère. Le résultat de l'exécution de l'algorithme HASH sur l'étiquette indique la boîte correcte pour cette étape.

L'étiquette est immédiatement suivie d'un caractère indiquant l'"opération" à effectuer : soit un signe égal (`=`), soit un tiret (`-`).

Si le caractère de l'opération est un *tiret* (`-`), allez dans la boîte concernée et retirez la lentille avec l'étiquette donnée si elle est présente dans la boîte. Ensuite, déplacez toutes les lentilles restantes aussi loin que possible dans la boîte sans changer leur ordre, en remplissant l'espace créé par le retrait de la lentille indiquée. (Si aucune lentille de cette boîte n'a l'étiquette donnée, rien ne se passe).

Si le caractère de l'opération est un *signe d'égalité* (`=`), il sera suivi d'un nombre indiquant la *longueur focale* de l'objectif qui doit être placé dans la case correspondante ; assurez-vous d'utiliser le fabricant d'étiquettes pour marquer l'objectif avec l'étiquette donnée au début de l'étape afin de pouvoir le retrouver plus tard. Deux situations sont possibles :

- S'il y a déjà un objectif dans la boîte avec la même étiquette, *remplacer l'ancien objectif* par le nouveau : retirer l'ancien objectif et mettre le nouveau à sa place, sans déplacer d'autres objectifs dans la boîte.
- S'il n'y a *pas* déjà une lentille dans la boîte avec la même étiquette, ajoutez la lentille à la boîte juste derrière les lentilles déjà présentes dans la boîte. Ne déplacez pas les autres lentilles lors de cette opération. S'il n'y a pas de lentilles dans la boîte, la nouvelle lentille est placée à l'avant de la boîte.

Voici le contenu de chaque boîte après chaque étape de la séquence d'initialisation ci-dessus :

```
After "rn=1":
Box 0: [rn 1]

After "cm-":
Box 0: [rn 1]

After "qp=3":
Box 0: [rn 1]
Box 1: [qp 3]

After "cm=2":
Box 0: [rn 1] [cm 2]
Box 1: [qp 3]

After "qp-":
Box 0: [rn 1] [cm 2]

After "pc=4":
Box 0: [rn 1] [cm 2]
Box 3: [pc 4]

After "ot=9":
Box 0: [rn 1] [cm 2]
Box 3: [pc 4] [ot 9]

After "ab=5":
Box 0: [rn 1] [cm 2]
Box 3: [pc 4] [ot 9] [ab 5]

After "pc-":
Box 0: [rn 1] [cm 2]
Box 3: [ot 9] [ab 5]

After "pc=6":
Box 0: [rn 1] [cm 2]
Box 3: [ot 9] [ab 5] [pc 6]

After "ot=7":
Box 0: [rn 1] [cm 2]
Box 3: [ot 7] [ab 5] [pc 6]
```

Les 256 boîtes sont toujours présentes ; seules les boîtes contenant des objectifs sont présentées ici. Dans chaque boîte, les objectifs sont répertoriés de l'avant vers l'arrière ; chaque objectif est indiqué par son étiquette et sa longueur focale entre crochets.

Pour confirmer que tous les objectifs sont correctement installés, additionnez le *pouvoir de mise au point* de tous les objectifs. La puissance de mise au point d'un seul objectif est le résultat d'une multiplication :

- Un plus le numéro de boîte de l'objectif en question.
- Le numéro d'emplacement de l'objectif dans la boîte : `1` pour le premier objectif, `2` pour le second, et ainsi de suite.
- La longueur focale de l'objectif.

À la fin de l'exemple ci-dessus, la puissance de mise au point de chaque objectif est la suivante :

- `rn` : `1` (boîte 0) * `1` (première fente) * `1` (longueur focale) = `*1*`
- `cm` : `1` (boîte 0) * `2` (deuxième fente) * `2` (distance focale) = `*4*`
- `ot` : `4` (boîte 3) * `1` (première fente) * `7` (longueur focale) = `*28*`
- `ab` : `4` (boîte 3) * `2` (deuxième fente) * `5` (longueur focale) = `*40*`
- `pc` : `4` (boîte 3) * `3` (troisième slot) * `6` (longueur focale) = `*72*`

Ainsi, l'exemple ci-dessus aboutit à une puissance de mise au point totale de `*145*`.

Avec l'aide d'un renne trop enthousiaste portant un casque, suivez la séquence d'initialisation. *Quelle est la puissance de mise au point de la configuration de lentille résultante ?*